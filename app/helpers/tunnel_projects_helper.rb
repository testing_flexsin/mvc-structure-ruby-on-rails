module TunnelProjectsHelper
  def tunnel_calculate_unit_price(project, product_price_list, part_quantity_information)
    price = product_price_list.our_sell_price.round(2)
    if (project.number_of_services >= 3) && (project.number_of_services <= 20)
      if part_quantity_information.tunnel_report_material_list.present? && (part_quantity_information.tunnel_report_material_list.tunnel_material_category_id == 2)
        unit_multiplier = UnitMultiplier.last
        if unit_multiplier.present?
          price = price * unit_multiplier.multiplied_factor
        end
      end
    end
    price
  end

  def tunnel_floor_plan_message(floor_plan)
    TunnelHaveFloorPlan.where(operator: floor_plan).first.try(:operand)
  end

	def tunnel_name_project(project)
		if project.project_details?
      "#{link_to project.project_name, tunnel_entry_path(project, facility: project.facility_option), remote: true}"
    elsif project.tunnel_data?
      "#{link_to project.project_name, project_tunnel_services_path(project, facility: project.facility_option), remote: true}"
    elsif project.tunnel_services?
      "#{link_to project.project_name, project_tunnel_system_path(project, facility: project.facility_option), remote: true}"
    elsif project.tunnel_system?
      "#{link_to project.project_name, project_tunnel_type_path(project, facility: project.facility_option), remote: true}"
    else
      "#{link_to project.project_name, edit_tunnel_project_path(project, facility: project.facility_option), remote: true}"
    end
	end

	def tunnel_edit_project(project)
		if project.project_details?
      "#{link_to '', tunnel_entry_path(project, facility: project.facility_option), remote: true, class: 'glyphicon glyphicon-edit'}"
    elsif project.tunnel_data?
      "#{link_to '', project_tunnel_services_path(project, facility: project.facility_option), remote: true, class: 'glyphicon glyphicon-edit'}"
    elsif project.tunnel_services?
      "#{link_to '', project_tunnel_system_path(project, facility: project.facility_option), remote: true, class: 'glyphicon glyphicon-edit'}"
    elsif project.tunnel_system?
      "#{link_to '', project_tunnel_type_path(project, facility: project.facility_option), remote: true, class: 'glyphicon glyphicon-edit'}"
    else
      "#{link_to '', edit_tunnel_project_path(project, facility: project.facility_option), remote: true, class: 'glyphicon glyphicon-edit'}"
    end
	end
end
