module ApplicationHelper
  def remote_request(type, path, params={}, target_tag_id)
	  "$.#{type}('#{path}',
      {#{params.collect { |p| "#{p[0]}: #{p[1]}" }.join(", ")}},
      function(data) {$('##{target_tag_id}').html(data);}
	  );"
	end

  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", alert: "alert-danger", notice: "alert-info" }[flash_type.to_sym]
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} fade in margin-bottom-10") do 
        concat content_tag(:a, 'x', class: "close position-inherit", data: { dismiss: 'alert' })
        concat message 
      end)
    end
    nil
  end

  def model_error_messages(model_resource)
    return '' if model_resource.errors.empty?

    messages = model_resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t('errors.messages.not_saved',
                      count: model_resource.errors.count,
                      resource: model_resource.class.model_name.human.downcase)

    html = <<-HTML
    <div class="alert alert-danger fade in margin-bottom-10">
      <a class="close position-inherit" data-dismiss="alert">x</a>
      <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end

  def project_quote_ref(value = 0)
    if value.to_s.length < 6
      "%06d" % value
    else
      value.to_s
    end
  end

  def value_with_precision(value = 0.0)
    number_with_precision(value, precision: 2, delimiter: ',')
  end

  def value_with_delimiter(value = 0.0)
    number_with_precision(value, precision: 0, delimiter: ',')
  end

  def user_plan(user)
    user_message = ""
    building_plan = user.building_plan
    tunnel_plan = user.tunnel_plan
    if building_plan.present? && tunnel_plan.present?
      building_plan_name = building_plan.name.split(" ").first
      tunnel_plan_name = tunnel_plan.name.split(" ").first
      if building_plan_name == tunnel_plan_name
        user_message = "You are member of the #{building_plan_name} plan<br/><span class='m-plan'>(Building &amp; Tunnel)</span>"
      else
        user_message = "You are member of the <br/>#{building_plan_name} plan (Building) &amp; #{tunnel_plan_name} plan (Tunnel)"
      end
    elsif building_plan.present? || tunnel_plan.present?
      if building_plan.present?
        building_plan_name = building_plan.name.split(" ").first
        user_message = "You are member of the #{building_plan_name} plan<br/><span class='m-plan'>(Building)</span>"
      end
      if tunnel_plan.present?
        tunnel_plan_name = tunnel_plan.name.split(" ").first
        user_message = "You are member of the #{tunnel_plan_name} plan<br/><span class='m-plan'>(Tunnel)</span>"
      end
    else
      user_message = "You are not a member of any plan<br/><span class='m-plan'>&nbsp;</span>"
    end
    user_message
  end

  def trail_message
    message = ""
    if (remaining_days > 0) && !(current_user.building_plan.present? || current_user.tunnel_plan.present?)
      message = "<div class='creditsBox'>
        <div class='container'>
          <div class='creaditsItem mw-370'>Your trial period will expire in #{remaining_days} days.</div>
        </div>
      </div>"
    elsif remaining_days <= 0 && !(current_user.building_plan.present? || current_user.tunnel_plan.present?)
      message = "<div class='creditsBox'>
        <div class='container'>
          <div class='creaditsItem mw-550'>Your trial period has expired. Please subscribe to continue.</div>
        </div>
      </div>"
    end
    message
  end
end
