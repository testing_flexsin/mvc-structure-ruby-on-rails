module HomeHelper
	def building_plans(user, static_page)
		message = "<td> </td>"
		building_plan_name = ""
		building_plan = user.building_plan
		if building_plan.present?
			building_plan_name = building_plan.name.split(" ").first
		end
		if building_plan_name == "Basic"
			message += "<td> </td>
			<td><a href='javascript:;' data-href='#{static_page.building_standard_plan_link}' class='signUp'>Upgrade <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='javascript:;' data-href='#{static_page.building_professional_plan_link}' class='signUp'>Upgrade <img src='/assets/circleArror.png' alt=''></a></td>"
		elsif building_plan_name == "Standard"
			message += "<td><a href='javascript:;' data-href='#{static_page.building_basic_plan_link}' class='signUp w180'>Downgrade <img src='/assets/circleArror.png' alt=''></a></td>
			<td> </td>
			<td><a href='javascript:;' data-href='#{static_page.building_professional_plan_link}' class='signUp'>Upgrade <img src='/assets/circleArror.png' alt=''></a></td>"
		elsif building_plan_name == "Professional"
			message += "<td><a href='javascript:;' data-href='#{static_page.building_basic_plan_link}' class='signUp w180'>Downgrade <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='javascript:;' data-href='#{static_page.building_standard_plan_link}' class='signUp w180'>Downgrade <img src='/assets/circleArror.png' alt=''></a></td>
			<td> </td>"
		else
			message += "<td><a href='javascript:;' data-href='#{static_page.building_basic_plan_link}' class='signUp w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='javascript:;' data-href='#{static_page.building_standard_plan_link}' class='signUp w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='javascript:;' data-href='#{static_page.building_professional_plan_link}' class='signUp w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>"
		end
		message
	end

	def tunnel_plans(user, static_page)
		message = "<td> </td>"
		building_plan_name = ""
		tunnel_plan = user.tunnel_plan
		if tunnel_plan.present?
			tunnel_plan_name = tunnel_plan.name.split(" ").first
		end
		if tunnel_plan_name == "Basic"
			message += "<td> </td>
			<td><a href='javascript:;' data-href='#{static_page.tunnel_standard_plan_link}' class='signUp'>Upgrade <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='javascript:;' data-href='#{static_page.tunnel_professional_plan_link}' class='signUp'>Upgrade <img src='/assets/circleArror.png' alt=''></a></td>"
		elsif tunnel_plan_name == "Standard"
			message += "<td><a href='javascript:;' data-href='#{static_page.tunnel_basic_plan_link}' class='signUp w180'>Downgrade <img src='/assets/circleArror.png' alt=''></a></td>
			<td> </td>
			<td><a href='javascript:;' data-href='#{static_page.tunnel_professional_plan_link}' class='signUp'>Upgrade <img src='/assets/circleArror.png' alt=''></a></td>"
		elsif tunnel_plan_name == "Professional"
			message += "<td><a href='javascript:;' data-href='#{static_page.tunnel_basic_plan_link}' class='signUp w180'>Downgrade <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='javascript:;' data-href='#{static_page.tunnel_standard_plan_link}' class='signUp w180'>Downgrade <img src='/assets/circleArror.png' alt=''></a></td>
			<td> </td>"
		else
			message += "<td><a href='javascript:;' data-href='#{static_page.tunnel_basic_plan_link}' class='signUp w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='javascript:;' data-href='#{static_page.tunnel_standard_plan_link}' class='signUp w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='javascript:;' data-href='#{static_page.tunnel_professional_plan_link}' class='signUp w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>"
		end
		message
	end
end
