module ProjectsHelper
  def calculate_unit_price(project, product_price_list, part_quantity_information)
    price = product_price_list.our_sell_price.round(2)
    if (project.number_of_services >= 3) && (project.number_of_services <= 20)
      if part_quantity_information.report_material_list.present? && (part_quantity_information.report_material_list.material_category_id == 2)
        unit_multiplier = UnitMultiplier.last
        if unit_multiplier.present?
          price = price * unit_multiplier.multiplied_factor
        end
      end
    end
    price
  end

  def floor_plan_message(floor_plan)
    HaveFloorPlan.where(operator: floor_plan).first.try(:operand)
  end
  
	def name_project(project)
		if project.project_details?
      "#{link_to project.project_name, building_entry_path(project, facility: project.facility_option), remote: true}"
    elsif project.building_data?
      "#{link_to project.project_name, project_building_services_path(project, facility: project.facility_option), remote: true}"
    elsif project.building_services?
      "#{link_to project.project_name, project_building_system_path(project, facility: project.facility_option), remote: true}"
    elsif project.building_system?
      "#{link_to project.project_name, project_building_type_path(project, facility: project.facility_option), remote: true}"
    else
      "#{link_to project.project_name, edit_project_path(project, facility: project.facility_option), remote: true}"
    end
	end

	def edit_project(project)
		if project.project_details?
      "#{link_to '', building_entry_path(project, facility: project.facility_option), remote: true, class: 'glyphicon glyphicon-edit'}"
    elsif project.building_data?
      "#{link_to '', project_building_services_path(project, facility: project.facility_option), remote: true, class: 'glyphicon glyphicon-edit'}"
    elsif project.building_services?
      "#{link_to '', project_building_system_path(project, facility: project.facility_option), remote: true, class: 'glyphicon glyphicon-edit'}"
    elsif project.building_system?
      "#{link_to '', project_building_type_path(project, facility: project.facility_option), remote: true, class: 'glyphicon glyphicon-edit'}"
    else
      "#{link_to '', edit_project_path(project, facility: project.facility_option), remote: true, class: 'glyphicon glyphicon-edit'}"
    end
	end
end
