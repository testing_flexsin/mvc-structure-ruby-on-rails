ActiveAdmin.register CommunicationType do
	menu false
  menu label: 'Communication Type - DB22c'

  permit_params :communication, :communication_assigned, :default_selected

	index do
	  selectable_column
	  id_column
	  column "Communication Type", :communication
	  column :communication_assigned
	  column :default_selected
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
	    row :id
	  	row "Communication Type" do |communication_type|
	  		communication_type.communication
	  	end
	    row :communication_assigned
		  row :default_selected
	    row :created_at
	    row :updated_at
  	end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :communication, label: "Communication Type"
	    f.input :communication_assigned, as: :select, collection: [['Yes', true], ['No', false]]
	    f.input :default_selected, as: :select, collection: [['Yes', true], ['No', false]]
		end
    f.actions
  end
end
