ActiveAdmin.register DesignInformation do
	menu false
	menu label: 'Design Information - DB23'

	preserve_default_filters!
	filter :building_data_entry, as: :select, collection: ->{BuildingDataEntry.order("id DESC").collect {|building_data_entry| [ building_data_entry.id] }}

	index do
	  selectable_column
	  id_column
	  column "Project" do |design_information|
	  	link_to design_information.building_data_entry.building.project_id, admin_project_path(design_information.building_data_entry.building.project_id)
	  end
	  column "Building" do |design_information|
	  	link_to design_information.building_data_entry.building_id, admin_building_path(design_information.building_data_entry.building_id)
	  end
	  column "Building Data Entry" do |design_information|
	    link_to design_information.building_data_entry_id, admin_building_data_entry_path(design_information.building_data_entry_id)
	  end
	  column "Building Number" do |design_information|
	  	design_information.building_data_entry.try(:building_number)
	  end
	  column "Floor Number" do |design_information|
	  	design_information.building_data_entry.try(:floor_number)
	  end
	  column "Design Frequency", :design_frequency
	  column "Coverage Radius", :coverage_radius
	  column "Coverage Area per each Antenna", :coverage_area_per_antenna
	  column "Building Total Coverage Area per floor", :building_total_coverage_area
	  column "Number of Antennas (Total)", :number_of_antennas
	  column "Horizontal Cable", :horizontal_cable
	  column "Number of BDA Required (Total)", :number_of_bda
	  column "Total Number of Antennas Required Each Floor", :total_number_of_antennas
	  column "Max Number of Floors", :number_of_floors
	  column "Distance Between each Floor", :distance_each_floor
	  column "Vertical Cable", :vertical_cable
	  column "Coverage Area per BDA", :coverage_area_per_bda
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
      row "Project" do |design_information|
	  		link_to "Project ##{design_information.building_data_entry.building.project_id}", admin_project_path(design_information.building_data_entry.building.project_id)
	  	end
	  	row "Building" do |design_information|
	  		link_to "Building ##{design_information.building_data_entry.building_id}", admin_building_path(design_information.building_data_entry.building_id)
	  	end
      row :building_data_entry_id
		  row "Building Number" do |design_information|
		  	design_information.building_data_entry.try(:building_number)
		  end
		  row "Floor Number" do |design_information|
		  	design_information.building_data_entry.try(:floor_number)
		  end
		  row "Design Frequency" do |design_information|
		  	design_information.design_frequency
		  end
		  row "Coverage Radius" do |design_information|
		  	design_information.coverage_radius
		  end
		  row "Coverage Area per each Antenna" do |design_information|
		  	design_information.coverage_area_per_antenna
		  end
		  row "Building Total Coverage Area per floor" do |design_information|
		  	design_information.building_total_coverage_area
		  end
		  row "Number of Antennas (Total)" do |design_information|
		  	design_information.number_of_antennas
		  end
		  row "Horizontal Cable" do |design_information|
		  	design_information.horizontal_cable
		  end
		  row "Number of BDA Required (Total)" do |design_information|
		  	design_information.number_of_bda
		  end
		  row "Total Number of Antennas Required Each Floor" do |design_information|
		  	design_information.total_number_of_antennas
		  end
		  row "Max Number of Floors" do |design_information|
		  	design_information.number_of_floors
		  end
		  row "Distance Between each Floor" do |design_information|
		  	design_information.distance_each_floor
		  end
		  row "Vertical Cable" do |design_information|
		  	design_information.vertical_cable
		  end
		  row "Coverage Area per BDA" do |design_information|
		  	design_information.coverage_area_per_bda
		  end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	actions :index, :show
end
