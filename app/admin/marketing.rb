ActiveAdmin.register Marketing do
	menu false
	menu label: 'Marketing - DB14'
	
	permit_params :region_id, :vendor_name

	preserve_default_filters!
	remove_filter :region_id
	remove_filter :projects
	remove_filter :tunnel_projects
	filter :region, as: :select, collection: ->{Region.order("id ASC").collect {|region| [region.name, region.id] }}, label: "Region"

	index do
	  selectable_column
	  id_column
	  column "Region" do |marketing|
	  	marketing.region.try(:name)
	  end
	  column :vendor_name
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
		  row "Region" do |marketing|
		  	marketing.region.try(:name)
		  end
      row :vendor_name
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :region_id, as: :select, collection: ->{Region.order("id ASC").collect {|region| [region.name, region.id] }}, label: "Region"
	    f.input :vendor_name
		end
    f.actions
  end
end
