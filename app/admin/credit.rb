ActiveAdmin.register Credit do
	menu false
	menu label: 'Credits'

	permit_params :credits, :amount
end
