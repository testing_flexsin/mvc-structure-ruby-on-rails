ActiveAdmin.register MaterialCategory do
	menu false
	
	permit_params :category

	preserve_default_filters!
	remove_filter :report_material_lists
end
