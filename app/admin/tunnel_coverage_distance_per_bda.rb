ActiveAdmin.register TunnelCoverageDistancePerBda do
	menu false
	menu label: 'Coverage Distance per BDA - DB24'
	
	permit_params :bda_product_category_id, :coverage_distance

	preserve_default_filters!
	remove_filter :bda_product_category_id
	filter	:bda_product_category, as: :select, collection: ->{BdaProductCategory.order("id ASC").collect {|bda_product_category| [bda_product_category.category, bda_product_category.id] }}, label: "BDA Product Category"
	
	index do
	  selectable_column
	  id_column
	  column "BDA Product Category" do |tunnel_coverage_distance_per_bda|
	  	tunnel_coverage_distance_per_bda.bda_product_category.try(:category)
	  end
	  column "Coverage Distance per BDA (Ft)", :coverage_distance
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
      row "BDA Product Category" do |tunnel_coverage_distance_per_bda|
      	tunnel_coverage_distance_per_bda.bda_product_category.try(:category)
      end
      row "Coverage Distance per BDA (Ft)" do |tunnel_coverage_distance_per_bda|
      	tunnel_coverage_distance_per_bda.coverage_distance
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :bda_product_category_id, as: :select, collection: ->{BdaProductCategory.order("id ASC").collect {|bda_product_category| [bda_product_category.category, bda_product_category.id] }}, label: "BDA Product Category"
	    f.input :coverage_distance, label: "Coverage Distance per BDA (Ft)"
		end
    f.actions
  end
end
