ActiveAdmin.register CableLoss do
	menu false
	menu label: 'Cable Loss - DB22'
	
	permit_params :highest_frequency_band_id, :cable_loss_1_by_2, :cable_loss_7_by_8, :cable_loss_11_by_4, :cable_loss_15_by_8

	preserve_default_filters!
	remove_filter :highest_frequency_band_id
	filter :highest_frequency_band, as: :select, collection: ->{HighestFrequencyBand.order("frequency ASC").collect {|frequency_band| [frequency_band.frequency, frequency_band.id] }}, label: "Freq (MHz)"

	index do
	  selectable_column
	  id_column
	  column "Freq (MHz)" do |cable_loss|
	  	cable_loss.highest_frequency_band.try(:frequency)
	  end
	  column "Coax Cable Loss per ft (1/2 in)", :cable_loss_1_by_2
	  column "Coax Cable Loss per ft (7/8 in)", :cable_loss_7_by_8
	  column "Coax Cable Loss per ft (11/4)", :cable_loss_11_by_4
	  column "Coax Cable Loss per ft (15/8)", :cable_loss_15_by_8
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
		  row "Freq (MHz)" do |cable_loss|
		  	cable_loss.highest_frequency_band.try(:frequency)
		  end
      row "Coax Cable Loss per ft (1/2 in)" do |cable_loss|
      	cable_loss.cable_loss_1_by_2
      end
      row "Coax Cable Loss per ft (7/8 in)" do |cable_loss|
      	cable_loss.cable_loss_7_by_8
      end
      row "Coax Cable Loss per ft (11/4)" do |cable_loss|
      	cable_loss.cable_loss_11_by_4
      end
      row "Coax Cable Loss per ft (15/8)" do |cable_loss|
      	cable_loss.cable_loss_15_by_8
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :highest_frequency_band_id, as: :select, collection: ->{HighestFrequencyBand.order("frequency ASC").collect {|frequency_band| [frequency_band.frequency, frequency_band.id] }}, label: "Freq (MHz)"
	    f.input :cable_loss_1_by_2, label: "Coax Cable Loss per ft (1/2 in)"
	    f.input :cable_loss_7_by_8, label: "Coax Cable Loss per ft (7/8 in)"
	    f.input :cable_loss_11_by_4, label: "Coax Cable Loss per ft (11/4)"
	    f.input :cable_loss_15_by_8, label: "Coax Cable Loss per ft (15/8)"
		end
    f.actions
  end
end
