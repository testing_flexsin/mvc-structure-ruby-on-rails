ActiveAdmin.register DesignInformationAdmin do
	menu false
	menu label: 'Design Information Admin - DB23'
	
	permit_params :distance_between_each_floor, :maximum_antennas_per_bda

	index do
	  selectable_column
	  id_column
	  column :distance_between_each_floor
	  column "Maximum Number of Antennas per BDA", :maximum_antennas_per_bda
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
		  row :distance_between_each_floor
		  row "Maximum Number of Antennas per BDA" do |design_information_admin|
		  	design_information_admin.maximum_antennas_per_bda
		  end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :distance_between_each_floor
	    f.input :maximum_antennas_per_bda, label: "Maximum Number of Antennas per BDA"
		end
    f.actions
  end
end
