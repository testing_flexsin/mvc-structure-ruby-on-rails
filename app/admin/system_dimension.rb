ActiveAdmin.register SystemDimension do
	menu false
	menu label: 'System Dimensioning - DB20'

	preserve_default_filters!
	filter	:building_data_entry, as: :select, collection: ->{BuildingDataEntry.order("id DESC").collect {|building_data_entry| [building_data_entry.building_name, building_data_entry.id] }}

	index do
	  selectable_column
	  id_column
	  column "Project" do |system_dimension|
	  	link_to system_dimension.building_data_entry.building.project_id, admin_project_path(system_dimension.building_data_entry.building.project_id)
	  end
	  column "building" do |system_dimension|
	  	link_to system_dimension.building_data_entry.building_id, admin_building_path(system_dimension.building_data_entry.building_id)
	  end
	  column "Building Data Entry" do |system_dimension|
	   	link_to system_dimension.building_data_entry_id, admin_building_data_entry_path(system_dimension.building_data_entry_id)
	  end
	  column "Building Number" do |system_dimension|
	  	system_dimension.building_data_entry.try(:building_number)
	  end
	  column "Floor Number" do |system_dimension|
	  	system_dimension.building_data_entry.try(:floor_number)
	  end
	  column "Area of Floor" do |system_dimension|
	  	system_dimension.building_data_entry.area_this_floor
	  end
	  column "Estimated Path Distance (km)", :estimated_path_distance
	  column "(F) Frequency (MHz)", :frequency
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
	  attributes_table do
	    row :id
	    row "Project" do |system_dimension|
	  		link_to "Project ##{system_dimension.building_data_entry.building.project_id}", admin_project_path(system_dimension.building_data_entry.building.project_id)
	    end
	  	row "building" do |system_dimension|
	  		link_to "Building ##{system_dimension.building_data_entry.building_id}", admin_building_path(system_dimension.building_data_entry.building_id)
	    end
	    row :building_data_entry_id
		  row "Building Number" do |system_dimension|
		  	system_dimension.building_data_entry.try(:building_number)
		  end
		  row "Floor Number" do |system_dimension|
		  	system_dimension.building_data_entry.try(:floor_number)
		  end
		  row "Area of Floor" do |system_dimension|
		  	system_dimension.building_data_entry.area_this_floor
		  end
		  row "Estimated Path Distance (km)" do |system_dimension|
		  	system_dimension.estimated_path_distance
		  end
		  row "(F) Frequency (MHZ)" do |system_dimension|
		  	system_dimension.frequency
		  end
	    row :created_at
	    row :updated_at
	  end
	  active_admin_comments
	end

	actions :index, :show
end
