ActiveAdmin.register StaticPage do
	menu false

	permit_params :header_logged_in, :header_not_logged_in, :footer, :support_link, :welcome_page, :home_page, :home_slider, :quick_start, :quick_start_step1, :quick_start_step2, :quick_start_step3, :quick_start_download,
								:about_us, :terms_and_conditions, :privacy_policy, :faq, :contact_us, :disclaimer_acknowledgement, :download_warning, :show_report_heading, :report_disclaimer, :report_header_pdf,
								:buy_credits, :credits, :transaction_history, :building_projects, :building_pending_projects, :tunnel_projects, :tunnel_pending_projects, :buttons_report_page,
								:help_user_name, :help_project_name, :help_company, :help_name, :help_number_of_buildings, :help_number_of_tunnels, :help_have_floorplan, :help_number_of_services, :help_highest_frequency_band, :help_frequency_channels, :help_excepted_rssi_at_mobile, :help_materials_sourcing,
								:help_building_name, :help_building_number, :help_number_of_floors, :help_quick_area_data_entry, :help_building_environment,
								:help_tunnel_name, :help_tunnel_number, :help_number_of_bores, :help_number_of_segments, :help_this_tunnel_bore, :help_this_tunnel_segment, :help_tunnel_environment, :help_number_of_stations,
								:left_image, :right_image, :edit_your_account, :personal_information, :change_password, :my_plan,
								:building_project_details, :building_facility_option, :building_data, :building_area_data_entry, :building_data_entry, :building_search_option, :building_band_and_services, :building_system_feed_method, :building_system_architecture, :building_preferred_materials_sourcing,
								:tunnel_project_details, :tunnel_facility_option, :tunnel_data, :tunnel_area_data_entry, :tunnel_data_entry, :tunnel_search_option, :tunnel_band_and_services,  :tunnel_system_feed_method, :tunnel_system_architecture, :expected_rssi_at_mobile, :tunnel_preferred_materials_sourcing,:technology_type, :communication_type,
								:admin_email, :internal_server_error, :not_found, :heading_change_your_password, :heading_forgot_your_password, :heading_register_account, :heading_login_details, :heading_retrieve_forgotten_password, :heading_resend_confirmation_instructions,:building_pricing, :tunnel_pricing, :pricing,
								:building_basic_plan_link, :building_standard_plan_link, :building_professional_plan_link, :tunnel_basic_plan_link, :tunnel_standard_plan_link, :tunnel_professional_plan_link, :verification_email, :confirmation_email, :reset_password_email, :unlock_email
	
	index do
	  selectable_column
	  id_column
	  column :header_logged_in
	  column :header_not_logged_in
	  column :footer
	  column :support_link
	  column :welcome_page
	  column :home_page
	  column :home_slider
	  column :quick_start
	  column :quick_start_step1
	  column :quick_start_step2
	  column :quick_start_step3
	  column :quick_start_download
	  column :about_us
	  column :terms_and_conditions
	  column :privacy_policy
	  column :faq
	  column :contact_us
		column :pricing
		column :building_pricing
		column :tunnel_pricing
	  column :disclaimer_acknowledgement
	  column :download_warning
	  column :show_report_heading
	  column :report_disclaimer
	  column :report_header_pdf
	  column :buy_credits
	  column :credits
	  column :transaction_history
    column :building_projects
	  column :building_pending_projects
		column :building_project_details
		column :building_facility_option
		column :building_data
		column :building_area_data_entry
		column :building_data_entry
		column :building_search_option
		column :building_band_and_services 
		column :building_system_feed_method
		column :building_system_architecture
		column :building_preferred_materials_sourcing
	  column :tunnel_projects
	  column :tunnel_pending_projects
		column :tunnel_project_details
		column :tunnel_facility_option
		column :tunnel_data
		column :tunnel_area_data_entry
		column :tunnel_data_entry
		column :tunnel_search_option
		column :tunnel_band_and_services 
		column :tunnel_system_feed_method
		column :tunnel_system_architecture
		column :expected_rssi_at_mobile
		column :tunnel_preferred_materials_sourcing
		column :technology_type
		column :communication_type
	  column :buttons_report_page
	  column :help_user_name
	  column :help_project_name
	  column :help_company
	  column :help_name
	  column :help_number_of_buildings
	  column :help_number_of_tunnels
	  column :help_have_floorplan
	  column :help_number_of_services
	  column :help_highest_frequency_band
	  column :help_frequency_channels
	  column :help_excepted_rssi_at_mobile
	  column :help_materials_sourcing
	  column :help_building_name
	  column :help_building_number
	  column :help_number_of_floors
	  column :help_quick_area_data_entry
	  column :help_building_environment
		column :help_tunnel_name
		column :help_tunnel_number
		column :help_number_of_bores
		column :help_number_of_segments
		column :help_this_tunnel_bore
		column :help_this_tunnel_segment
		column :help_tunnel_environment
		column :help_number_of_stations
		column :edit_your_account
		column :personal_information
		column :change_password
		column :my_plan
		column :internal_server_error
		column :not_found
		column :verification_email
		column :confirmation_email
		column :reset_password_email
		column :unlock_email
		column :heading_change_your_password
		column :heading_forgot_your_password
		column :heading_register_account
		column :heading_login_details
		column :heading_retrieve_forgotten_password
		column :heading_resend_confirmation_instructions
		column :building_basic_plan_link
		column :building_standard_plan_link
		column :building_professional_plan_link
		column :tunnel_basic_plan_link
		column :tunnel_standard_plan_link
		column :tunnel_professional_plan_link
	  column "Project Left Image (260 x 402)" do |static_page|
	  	image_tag static_page.left_image, class: "image-preview-small"
	  end
	  column "Project Right Image (264 x 266)" do |static_page|
	  	image_tag static_page.right_image, class: "image-preview-small"
	  end
		column :admin_email
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
      row :header_logged_in
		  row :header_not_logged_in
		  row :footer
		  row :support_link
		  row :welcome_page
		  row :home_page
	  	row :home_slider
		  row :quick_start
		  row :quick_start_step1
		  row :quick_start_step2
		  row :quick_start_step3
		  row :quick_start_download
		  row :about_us
		  row :terms_and_conditions
		  row :privacy_policy
		  row :faq
		  row :contact_us
			row :pricing
			row :building_pricing
			row :tunnel_pricing
		  row :disclaimer_acknowledgement
		  row :download_warning
	  	row :show_report_heading
		  row :report_disclaimer
		  row :report_header_pdf
		  row :buy_credits
		  row :credits
		  row :transaction_history
		  row :building_projects
		  row :building_pending_projects
		  row :building_project_details
			row :building_facility_option
			row :building_data
			row :building_area_data_entry
			row :building_data_entry
			row :building_search_option
			row :building_band_and_services 
			row :building_system_feed_method
			row :building_system_architecture
			row :building_preferred_materials_sourcing
		  row :tunnel_projects
		  row :tunnel_pending_projects
		  row :tunnel_project_details
			row :tunnel_facility_option
			row :tunnel_data
			row :tunnel_area_data_entry
			row :tunnel_data_entry
			row :tunnel_search_option
			row :tunnel_band_and_services 
			row :tunnel_system_feed_method
			row :tunnel_system_architecture
			row :expected_rssi_at_mobile
			row :tunnel_preferred_materials_sourcing
			row :technology_type
			row :communication_type
		  row :buttons_report_page
		  row :help_user_name
		  row :help_project_name
		  row :help_company
		  row :help_name
		  row :help_number_of_buildings
		  row :help_number_of_tunnels
		  row :help_have_floorplan
		  row :help_number_of_services
		  row :help_highest_frequency_band
		  row :help_frequency_channels
		  row :help_excepted_rssi_at_mobile
		  row :help_materials_sourcing
		  row :help_building_name
		  row :help_building_number
		  row :help_number_of_floors
		  row :help_quick_area_data_entry
		  row :help_building_environment
			row :help_tunnel_name
			row :help_tunnel_number
			row :help_number_of_bores
			row :help_number_of_segments
			row :help_this_tunnel_bore
			row :help_this_tunnel_segment
			row :help_tunnel_environment
			row :help_number_of_stations
			row :edit_your_account
			row :personal_information
			row :change_password
			row :my_plan
			row :internal_server_error
			row :not_found
			row :verification_email
			row :confirmation_email
			row :reset_password_email
			row :unlock_email
			row :heading_change_your_password
			row :heading_forgot_your_password
			row :heading_register_account
			row :heading_login_details
			row :heading_retrieve_forgotten_password
			row :heading_resend_confirmation_instructions
			row :building_basic_plan_link
			row :building_standard_plan_link
			row :building_professional_plan_link
			row :tunnel_basic_plan_link
			row :tunnel_standard_plan_link
			row :tunnel_professional_plan_link
		  row "Project Left Image (260 x 402)" do |static_page|
		  	image_tag static_page.left_image, class: "image-preview"
		  end
		  row "Project Right Image (264 x 266)" do |static_page|
		  	image_tag static_page.right_image, class: "image-preview"
		  end
			row :admin_email
      row :created_at
      row :updated_at
    end

    active_admin_comments
  end

  form do |f|
		f.inputs do
	    f.input :header_logged_in
	    f.input :header_not_logged_in
		  f.input :footer
		  f.input :support_link
		  f.input :welcome_page
		  f.input :home_page
		  f.input :home_slider
		  f.input :quick_start
		  f.input :quick_start_step1
		  f.input :quick_start_step2
		  f.input :quick_start_step3
		  f.input :quick_start_download
		  f.input :about_us
		  f.input :terms_and_conditions
		  f.input :privacy_policy
		  f.input :faq
		  f.input :contact_us
		  f.input :pricing
			f.input :building_pricing
		  f.input :tunnel_pricing
		  f.input :disclaimer_acknowledgement
		  f.input :download_warning
	  	f.input :show_report_heading
		  f.input :report_disclaimer
		  f.input :report_header_pdf
		  f.input :buy_credits
		  f.input :credits
		  f.input :transaction_history
		  f.input :building_projects
		  f.input :building_pending_projects
		  f.input :building_project_details
			f.input :building_facility_option
			f.input :building_data
			f.input :building_area_data_entry
			f.input :building_data_entry
			f.input :building_search_option
			f.input :building_band_and_services 
			f.input :building_system_feed_method
			f.input :building_system_architecture
			f.input :building_preferred_materials_sourcing
		  f.input :tunnel_projects
		  f.input :tunnel_pending_projects
		  f.input :tunnel_project_details
			f.input :tunnel_facility_option
			f.input :tunnel_data
			f.input :tunnel_area_data_entry
			f.input :tunnel_data_entry
			f.input :tunnel_search_option
			f.input :tunnel_band_and_services 
			f.input :tunnel_system_feed_method
			f.input :tunnel_system_architecture
			f.input :expected_rssi_at_mobile
			f.input :tunnel_preferred_materials_sourcing
			f.input :technology_type
			f.input :communication_type
		  f.input :buttons_report_page
		  f.input :help_user_name
		  f.input :help_project_name
		  f.input :help_company
		  f.input :help_name
		  f.input :help_number_of_buildings
		  f.input :help_number_of_tunnels
		  f.input :help_have_floorplan
		  f.input :help_number_of_services
		  f.input :help_highest_frequency_band
		  f.input :help_frequency_channels
		  f.input :help_excepted_rssi_at_mobile
		  f.input :help_materials_sourcing
		  f.input :help_building_name
		  f.input :help_building_number
		  f.input :help_number_of_floors
		  f.input :help_quick_area_data_entry
		  f.input :help_building_environment
			f.input :help_tunnel_name
			f.input :help_tunnel_number
			f.input :help_number_of_bores
			f.input :help_number_of_segments
			f.input :help_this_tunnel_bore
			f.input :help_this_tunnel_segment
			f.input :help_tunnel_environment
			f.input :help_number_of_stations
		  f.input :edit_your_account
			f.input :personal_information
			f.input :change_password, as: :text
			f.input :my_plan
			f.input :internal_server_error
			f.input :not_found
			f.input :verification_email
			f.input :confirmation_email
			f.input :reset_password_email
			f.input :unlock_email
			f.input :heading_change_your_password, as: :string
			f.input :heading_forgot_your_password, as: :string
			f.input :heading_register_account
			f.input :heading_login_details
			f.input :heading_retrieve_forgotten_password, as: :string
			f.input :heading_resend_confirmation_instructions
			f.input :building_basic_plan_link
			f.input :building_standard_plan_link
			f.input :building_professional_plan_link
			f.input :tunnel_basic_plan_link
			f.input :tunnel_standard_plan_link
			f.input :tunnel_professional_plan_link
		  f.input :left_image, as: :file, image_preview: true, accept: 'image/png,image/gif,image/jpeg,image/jpg', label: "Project Left Image (260 x 402)"
		  f.input :right_image, as: :file, image_preview: true, accept: 'image/png,image/gif,image/jpeg,image/jpg', label: "Project Right Image (264 x 266)"
			f.input :admin_email
		end
    f.actions
  end
end
