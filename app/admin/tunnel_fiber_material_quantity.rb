ActiveAdmin.register TunnelFiberMaterialQuantity do
  menu false
  menu label: 'Fiber Material Quantity - DB13a'

  permit_params :tunnel_report_material_list_id, :quantity

  preserve_default_filters!
  remove_filter :tunnel_report_material_list_id
  filter :tunnel_report_material_list, as: :select, collection: ->{TunnelReportMaterialList.order("id ASC").collect {|tunnel_report_material_list| ["#{tunnel_report_material_list.tunnel_material_category.try(:category)} - #{tunnel_report_material_list.description}", tunnel_report_material_list.id] }}, label: "Category - Description"

  index do
    selectable_column
    id_column
    column "Category" do |tunnel_fiber_material_quantity|
      tunnel_fiber_material_quantity.tunnel_report_material_list.tunnel_material_category.try(:category)
    end
    column "Description" do |tunnel_fiber_material_quantity|
      tunnel_fiber_material_quantity.tunnel_report_material_list.try(:description)
    end
    column :quantity
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row "Category" do |tunnel_fiber_material_quantity|
        tunnel_fiber_material_quantity.tunnel_report_material_list.tunnel_material_category.try(:category)
      end
      row "Description" do |tunnel_fiber_material_quantity|
        tunnel_fiber_material_quantity.tunnel_report_material_list.try(:description)
      end
      row :quantity
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    f.inputs do
      f.input :description, as: :select, collection: ->{TunnelMaterialCategory.order("id ASC").collect {|tunnel_material_category| [tunnel_material_category.category, tunnel_material_category.id] }}, label: "Category", input_html: { onchange: remote_request(:post, :change_report_material_list, {tunnel_material_category_id: "$('#tunnel_fiber_material_quantity_description').val()"}, :tunnel_fiber_material_quantity_tunnel_report_material_list_id) }
      f.input :tunnel_report_material_list_id, as: :select, collection: [], label: "Description"
      f.input :quantity
    end
    f.actions
  end

  collection_action :change_report_material_list, method: :post do
    @report_material_lists = TunnelReportMaterialList.where(tunnel_material_category_id: params[:tunnel_material_category_id]).order("id ASC")
    render text: view_context.options_from_collection_for_select(@report_material_lists, :id, :description)
  end

  member_action :change_report_material_list, method: :post do
    @report_material_lists = TunnelReportMaterialList.where(tunnel_material_category_id: params[:tunnel_material_category_id]).order("id ASC")
    render text: view_context.options_from_collection_for_select(@report_material_lists, :id, :description)
  end
end
