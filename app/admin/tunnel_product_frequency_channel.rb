ActiveAdmin.register TunnelProductFrequencyChannel do
	menu false
	menu label: 'Product Category ‐ Frequency Channel - DB11'

	permit_params :bda_product_category_id, :number_of_channels, :composite_power, :channel_power, :papr, :default_selected
	
	preserve_default_filters!
	remove_filter :bda_product_category_id
	remove_filter :projects
	remove_filter :tunnel_projects
	filter :bda_product_category, as: :select, collection: ->{BdaProductCategory.order("id ASC").collect {|product_category| [product_category.category, product_category.id] }}, label: "Product Category"
	
	index do
	  selectable_column
	  id_column
	  column :number_of_channels
	  column "Composite Power (dBm)", :composite_power
	  column "Channel Power (dBm)", :channel_power
	  column "PAPR (dB)", :papr
	  column "Product Category" do |product_frequency_channel|
		  product_frequency_channel.bda_product_category.try(:category)
		end
	  column :default_selected
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
	    row :id
	  	row :number_of_channels
	    row "Composite Power (DBM)" do |product_frequency_channel|
	    	product_frequency_channel.composite_power
	    end
	    row "Channel Power (DBM)" do |product_frequency_channel|
	    	product_frequency_channel.channel_power
	    end
	    row "PAPR (DB)" do |product_frequency_channel|
	    	product_frequency_channel.papr
	    end
		  row "Product Category" do |product_frequency_channel|
		  	product_frequency_channel.bda_product_category.try(:category)
		  end
		  row :default_selected
	    row :created_at
	    row :updated_at
  	end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :number_of_channels
	    f.input :composite_power, label: "Composite Power (dBm)"
	    f.input :channel_power, input_html: { disabled: true }, label: "Channel Power (dBm)"
	    f.input :papr, input_html: { disabled: true }, label: "PAPR (dB)"
	    f.input :bda_product_category_id, as: :select, collection: ->{BdaProductCategory.order("id ASC").collect {|bda_product| [bda_product.category, bda_product.id] }}, label: "Product Category"
	    f.input :default_selected, as: :select, collection: [['Yes', true], ['No', false]]
		end
    f.actions
  end
end
