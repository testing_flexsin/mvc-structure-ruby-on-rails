ActiveAdmin.register_page "Dashboard" do
  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel "Building DBs" do
          ul(class: "list-none") do
            li link_to("Project Details - DB1", admin_projects_path)
            li link_to("Building Data", admin_buildings_path)
            li link_to("Number of Buildings - DB3", admin_number_of_buildings_path)
            li link_to("Number of Floors", admin_number_of_floors_path)
            li link_to("Have Floor Plan - DB4", admin_have_floor_plans_path)
            li link_to("Building Data Entry - DB6", admin_building_data_entries_path)
            li link_to("Building Environment - DB8", admin_building_environments_path)
            li link_to("Product Category ‐ Frequency Channel - DB11", admin_product_frequency_channels_path)
            li link_to("Donor and Direct Feed Quantity - DB12a", admin_donor_direct_feed_quantities_path)
            li link_to("Fiber Material Quantity - DB13a", admin_fiber_material_quantities_path)
            li link_to("Square Footage Size", admin_square_footage_sizes_path)              
            li link_to("Material Categories", admin_material_categories_path)
            li link_to("Report Material List - DB15", admin_report_material_lists_path)
            li link_to("Splitter Matrix - DB16", admin_splitter_matrices_path)
            li link_to("Product Price List - DB17", admin_product_price_lists_path)
            li link_to("Link Budget‐DL - DB18", admin_link_budgets_path)
            li link_to("Link Budget Admin‐DL - DB18", admin_link_budget_admins_path)
            li link_to("Passive Component Loss - DB19", admin_passive_component_losses_path)
            li link_to("System Dimensioning - DB20", admin_system_dimensions_path)
            li link_to("Cable Loss - DB22", admin_cable_losses_path)
            li link_to("Design Information - DB23", admin_design_informations_path) 
            li link_to("Design Information Admin - DB23", admin_design_information_admins_path)
            li link_to("Coverage Area per BDA - DB24", admin_coverage_area_per_bdas_path)
            li link_to("Equalization Multiplier", admin_equalization_multipliers_path)
            li link_to("Unit Multiplier", admin_unit_multipliers_path)
            li link_to("Part Quantity Information - DB25", admin_part_quantity_informations_path)
          end
        end
        panel "Tunnel DBs" do
          ul(class: "list-none") do
            li link_to("Project Details - DB1", admin_tunnel_projects_path)
            li link_to("Tunnel Data", admin_tunnels_path)
            li link_to("Number of Tunnels - DB3a", admin_number_of_tunnels_path)
            li link_to("Number of Bores - DB3b", admin_number_of_bores_path)
            li link_to("Number of Segments - DB3c", admin_number_of_segments_path)
            li link_to("Have Floor Plan - DB4", admin_tunnel_have_floor_plans_path)
            li link_to("Tunnel Data Entry - DB6", admin_tunnel_data_entries_path)
            li link_to("Number of Radio Rooms - DB6a", admin_number_of_radio_rooms_path)
            li link_to("Cable Type Matrix - DB7", admin_cable_type_matrices_path)
            li link_to("Tunnel Environment - DB8", admin_tunnel_environments_path)
            li link_to("Product Category ‐ Frequency Channel - DB11", admin_tunnel_product_frequency_channels_path)
            li link_to("Donor and Direct Feed Quantity - DB12a", admin_tunnel_donor_direct_feed_quantities_path)
            li link_to("Fiber Material Quantity - DB13a", admin_tunnel_fiber_material_quantities_path)
            li link_to("Total Tunnel Bore", admin_tunnel_lengths_path)
            li link_to("Material Categories", admin_tunnel_material_categories_path)
            li link_to("Report Material List - DB15", admin_tunnel_report_material_lists_path)
            li link_to("Splitter Matrix - DB16", admin_tunnel_splitter_matrices_path)
            li link_to("Product Price List - DB17", admin_tunnel_product_price_lists_path)
            li link_to("Tunnel Link Budget‐DL - DB18", admin_tunnel_link_budgets_path)
            li link_to("Link Budget Admin‐DL - DB18", admin_tunnel_link_budget_admins_path)
            li link_to("Passive Component Loss - DB19", admin_tunnel_passive_component_losses_path)
            li link_to("Tunnel Signal Level Benchmark - DB20", admin_tunnel_signal_level_benchmarks_path)
            li link_to("Tunnel Cable I‐Loss - DB22a", admin_tunnel_cable_i_losses_path)
            li link_to("Tunnel Cable C‐Loss - DB22b", admin_tunnel_cable_c_losses_path)
            li link_to("Design Information ‐ Tunnel - DB23", admin_tunnel_design_informations_path)
            li link_to("Coverage Distance per BDA - DB24", admin_tunnel_coverage_distance_per_bdas_path)
            li link_to("Part Quantity Information ‐ Tunnel - DB25", admin_tunnel_part_quantity_informations_path)
          end
        end
        panel "Shared DBs" do
          ul(class: "list-none") do
            li link_to("Number of Services - DB9", admin_number_of_services_path)
            li link_to("Highest Frequency Band - DB10", admin_highest_frequency_bands_path)
            li link_to("BDA Product Categories", admin_bda_product_categories_path)
            li link_to("System Feed Method - DB12", admin_system_feed_methods_path)
            li link_to("System Architecture - DB13", admin_system_architectures_path)
            li link_to("Regions", admin_regions_path)
            li link_to("Marketing - DB14", admin_marketings_path)
            li link_to("RSSI Threshold Level Benchmark - DB21", admin_rssi_threshold_level_benchmarks_path)
            li link_to("Technology Type - DB22c", admin_technology_types_path)
            li link_to("Communication Type - DB22d", admin_communication_types_path)
            li link_to("Services Breakout - DB30", admin_services_breakouts_path)
          end
        end
        panel "DBs" do
          ul(class: "list-none") do
            li link_to("Admin Users", admin_admin_users_path)
            li link_to("Users", admin_users_path)
            # li link_to("Contacts", admin_contacts_path)
            li link_to("Static Pages", admin_static_pages_path)
            li link_to("Timer Settings", admin_timer_settings_path)
            li link_to("Trail Period", admin_trail_periods_path)
            li link_to("Manage Credits", admin_manage_credits_path)
            li link_to("Plans", admin_plans_path)
            li link_to("Credits", admin_credits_path)
            li link_to("Subscriptions", admin_subscriptions_path)
            li link_to("Comments", admin_comments_path)
          end
        end
      end
    end
  end # content
end
