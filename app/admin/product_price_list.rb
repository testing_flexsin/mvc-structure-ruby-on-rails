ActiveAdmin.register ProductPriceList do
	menu false
	menu label: 'Product Price List - DB17'
	
	permit_params :report_material_list_id, :vendor, :manufacturer, :mfg_pn, :our_pn, :mfg_list_price, :our_cost, :discount_off_list, :our_sell_price

	preserve_default_filters!
	remove_filter :report_material_list_id
	filter :report_material_list, as: :select, collection: ->{ReportMaterialList.order("id ASC").collect {|report_material_list| ["#{report_material_list.material_category.try(:category)} - #{report_material_list.description}", report_material_list.id] }}, label: "Category - Description"

	index do
	  selectable_column
	  id_column
	  column "Category" do |product_price_list|
	  	product_price_list.report_material_list.material_category.try(:category)
	  end
	  column "Vendor/Supplier", :vendor
	  column "Manufacturer", :manufacturer
	  column "MFG PN", :mfg_pn
	  column "OUR PN", :our_pn
	  column "Description" do |product_price_list|
	  	product_price_list.report_material_list.try(:description)
	  end
	  column "MFG List Price", :mfg_list_price
	  column "OUR Cost", :our_cost
	  column "Discount off List (%)", :discount_off_list
	  column "OUR SELL Price (USD)", :our_sell_price
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
		  row :id
		  row "Category" do |product_price_list|
		  	product_price_list.report_material_list.material_category.try(:category)
		  end
		  row "Vendor/Supplier" do |product_price_list|
		  	product_price_list.vendor
		  end
		  row "Manufacturer" do |product_price_list|
		  	product_price_list.manufacturer
		  end
		  row "MFG PN" do |product_price_list|
		  	product_price_list.mfg_pn
		  end
		  row "OUR PN" do |product_price_list|
		  	product_price_list.our_pn
		  end
		  row "Description" do |product_price_list|
		  	product_price_list.report_material_list.try(:description)
		  end
		  row "MFG List Price" do |product_price_list|
		  	product_price_list.mfg_list_price
		  end
		  row "OUR Cost" do |product_price_list|
		  	product_price_list.our_cost
		  end
		  row "Discount off List (%)" do |product_price_list|
		  	product_price_list.discount_off_list
		  end
		  row "OUR SELL Price (USD)" do |product_price_list|
		  	product_price_list.our_sell_price
		  end
		  row :created_at
		  row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :description, as: :select, collection: ->{MaterialCategory.order("id ASC").collect {|material_category| [material_category.category, material_category.id] }}, label: "Category", :input_html => { :onchange => remote_request(:post, :change_report_material_list, {:material_category_id=>"$('#product_price_list_description').val()"}, :product_price_list_report_material_list_id) }
	    f.input :vendor, label: "Vendor/Supplier"
	    f.input :manufacturer, label: "Manufacturer"
	    f.input :mfg_pn, label: "MFG PN"
	    f.input :our_pn, label: "OUR PN"
	    f.input :report_material_list_id, as: :select, collection: [], label: "Description"
	    f.input :mfg_list_price, label: "MFG List Price"
	    f.input :our_cost, label: "OUR Cost"
	    f.input :discount_off_list, label: "Discount off List (%)"
	    f.input :our_sell_price, label: "OUR SELL Price (USD)"
		end
    f.actions
  end

	collection_action :change_report_material_list, method: :post do
    @report_material_lists = ReportMaterialList.where(material_category_id: params[:material_category_id]).order("id ASC")
    render text: view_context.options_from_collection_for_select(@report_material_lists, :id, :description)
	end

	member_action :change_report_material_list, method: :post do
    @report_material_lists = ReportMaterialList.where(material_category_id: params[:material_category_id]).order("id ASC")
    render text: view_context.options_from_collection_for_select(@report_material_lists, :id, :description)
	end
end
