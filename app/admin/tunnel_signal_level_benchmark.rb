ActiveAdmin.register TunnelSignalLevelBenchmark do
	menu false
  menu label: 'Tunnel Signal Level Benchmark - DB20'

	preserve_default_filters!
	remove_filter :tunnel_data_entry_id
  filter :tunnel_data_entry, as: :select, collection: ->{TunnelDataEntry.order("id DESC").collect {|tunnel_data_entry| [tunnel_data_entry.tunnel_name, tunnel_data_entry.id] }}
	
	index do
    selectable_column
    id_column
    column "Project" do |tunnel_signal_level_benchmark|
      link_to tunnel_signal_level_benchmark.tunnel_data_entry.tunnel.tunnel_project_id, admin_tunnel_project_path(tunnel_signal_level_benchmark.tunnel_data_entry.tunnel.tunnel_project_id)
    end
    column "Tunnel" do |tunnel_signal_level_benchmark|
      link_to tunnel_signal_level_benchmark.tunnel_data_entry.tunnel_id, admin_tunnel_path(tunnel_signal_level_benchmark.tunnel_data_entry.tunnel_id)
    end
    column "Tunnel Data Entry" do |tunnel_signal_level_benchmark|
      link_to tunnel_signal_level_benchmark.tunnel_data_entry_id, admin_tunnel_data_entry_path(tunnel_signal_level_benchmark.tunnel_data_entry_id)
    end
    column "RSSI at Portable In Tunnel(dBm)", :rssi_at_portable_in_tunnel
    column "Threshold Level Benchmark", :threshold_level_benchmark
    column "Threshold Overlimit Factor(dB)", :threshold_overlimit_factor
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row "Project" do |tunnel_signal_level_benchmark|
        link_to "Project ##{tunnel_signal_level_benchmark.tunnel_data_entry.tunnel.tunnel_project_id}", admin_project_path(tunnel_signal_level_benchmark.tunnel_data_entry.tunnel.tunnel_project_id)
      end
      row "Tunnel" do |tunnel_signal_level_benchmark|
       link_to "Tunnel ##{tunnel_signal_level_benchmark.tunnel_data_entry.tunnel_id}", admin_tunnel_path(tunnel_signal_level_benchmark.tunnel_data_entry.tunnel_id)
      end
      row :tunnel_data_entry_id
      row "RSSI at Portable In Tunnel(dbm)" do |tunnel_signal_level_benchmark|
      	tunnel_signal_level_benchmark.rssi_at_portable_in_tunnel
      end
      row "Threshold Level Benchmark" do |tunnel_signal_level_benchmark|
      	tunnel_signal_level_benchmark.threshold_level_benchmark
      end
      row "Threshold Overlimit Factor(db)" do |tunnel_signal_level_benchmark|
      	tunnel_signal_level_benchmark.threshold_overlimit_factor
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  actions :index, :show
end
