ActiveAdmin.register User do
  menu false

  permit_params :email, :user_name, :phone_number, :first_name, :last_name, :company_name, :password, :credits

	preserve_default_filters!
  remove_filter :subscriptions
  remove_filter :full_name
	filter :projects, as: :select, collection: ->{Project.order("id DESC").collect {|project| [project.project_name, project.id] }}
	filter :email
  filter :user_name
  filter :first_name
  filter :last_name
  filter :company_name
  filter :phone_number
  filter :credits

  index do
    selectable_column
    id_column
    column :email
    column :first_name
    column :last_name
    column :user_name
    column :company_name
    column :phone_number
    column :credits
    actions
  end

  show do
    attributes_table do
      row :id
      row :email
      row :first_name
      row :last_name
      row :user_name
      row :company_name
      row :phone_number
      row :credits
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end


	form do |f|
		f.inputs do
			f.input :email
	    f.input :password
      f.input :first_name
	    f.input :last_name
	    f.input :user_name
	    f.input :company_name
	    f.input :phone_number
      f.input :credits
		end
    f.actions
  end
end
