ActiveAdmin.register BuildingEnvironment do
	menu false
	menu label: 'Building Environment - DB8'
	
	permit_params :environment, :building_indoor_factor
end
