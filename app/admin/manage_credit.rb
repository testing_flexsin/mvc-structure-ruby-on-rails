ActiveAdmin.register ManageCredit do
	menu false
	menu label: 'Manage Credits'

	permit_params :default_credits, :credits_required

	index do
	  selectable_column
	  id_column
	  column "Default Credits Given to Trail Users", :default_credits
	  column "Credits Required for Project", :credits_required
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
	    row :id
	  	row "Default Credits Given to Trail Users" do |plan|
	  		plan.default_credits
	  	end
	  	row "Credits Required for Project" do |plan|
	  		plan.credits_required
	  	end
	    row :created_at
	    row :updated_at
  	end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :default_credits, label: "Default Credits Given to Trail Users"
	    f.input :credits_required, label: "Credits Required for Project"
		end
    f.actions
  end
end
