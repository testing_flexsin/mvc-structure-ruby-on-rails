ActiveAdmin.register CoverageAreaPerBda do
	menu false
	menu label: 'Coverage Area per BDA - DB24'
	
	permit_params :bda_product_category_id, :coverage_area

	preserve_default_filters!
	remove_filter :bda_product_category_id
	filter	:bda_product_category, as: :select, collection: ->{BdaProductCategory.order("id ASC").collect {|product_category| [product_category.category, product_category.id] }}, label: "BDA Product Category"
	
	index do
	  selectable_column
	  id_column
	  column "BDA Product Category" do |coverage_area_per_bda|
	  	coverage_area_per_bda.bda_product_category.try(:category)
	  end
	  column "Coverage Area per BDA (Sq.Ft)", :coverage_area
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
      row "BDA Product Category" do |coverage_area_per_bda|
      	coverage_area_per_bda.bda_product_category.try(:category)
      end
      row "Coverage Area per BDA (Sq.Ft)" do |coverage_area_per_bda|
      	coverage_area_per_bda.coverage_area
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :bda_product_category_id, as: :select, collection: ->{BdaProductCategory.order("id ASC").collect {|product_category| [product_category.category, product_category.id] }}, label: "BDA Product Category"
	    f.input :coverage_area, label: "Coverage Area per BDA (Sq.Ft)"
		end
    f.actions
  end
end
