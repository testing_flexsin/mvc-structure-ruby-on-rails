ActiveAdmin.register TunnelEnvironment do
	menu false
	menu label: 'Tunnel Environment - DB8'

	permit_params :environment, :tunnel_indoor_factor
end
