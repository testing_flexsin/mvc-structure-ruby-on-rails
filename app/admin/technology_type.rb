ActiveAdmin.register TechnologyType do
	menu false
  menu label: 'Technology Type - DB22c'

	permit_params :technology, :papr, :default_selected

	index do
    selectable_column
    id_column
    column "Technology Type", :technology
    column "PAPR", :papr
    column :default_selected
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row "Technology Type" do |technology_type|
        technology_type.technology
      end
      row "PAPR" do |technology_type|
        technology_type.papr
      end
      row :default_selected
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    f.inputs do
      f.input :technology, label: "Technology Type"
      f.input :papr, label: "PAPR"
      f.input :default_selected, as: :select, collection: [['Yes', true], ['No', false]]
    end
    f.actions
  end
end
