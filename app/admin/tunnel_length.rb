ActiveAdmin.register TunnelLength do
	menu false
  menu label: 'Total Tunnel Bore'

	permit_params :length
end

