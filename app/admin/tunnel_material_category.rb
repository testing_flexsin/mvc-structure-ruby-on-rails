ActiveAdmin.register TunnelMaterialCategory do
	menu false

	permit_params :category
	
  preserve_default_filters!
	remove_filter :tunnel_report_material_lists
end
