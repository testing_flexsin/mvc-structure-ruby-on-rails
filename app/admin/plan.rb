ActiveAdmin.register Plan do
	menu false
	menu label: 'Plans'

	permit_params :name, :amount, :credits, :usage_credits_one_time, :product_number

	index do
	  selectable_column
	  id_column
	  column :name
	  column :amount
	  column :credits
	  column "Usage Credits", :usage_credits_one_time do |plan|
	  	case plan.usage_credits_one_time
	  	when true
	  		"One Time"
	  	when false
	  		"Per Month"
	  	end
	  end
	  column "Product Number / SKU", :product_number
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
	    row :id
	    row :name
		  row :amount
		  row :credits
	  	row "Usage Credits", :usage_credits_one_time do |plan|
		  	case plan.usage_credits_one_time
		  	when true
		  		"One Time"
		  	when false
		  		"Per Month"
		  	end
		  end
		  row "Product Number / SKU" do |plan|
		  	plan.product_number
		  end
	    row :created_at
	    row :updated_at
  	end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :name
	    f.input :amount
	    f.input :credits
	    f.input :usage_credits_one_time, as: :select, collection: [["One Time", true], ["Per Month", false]], label: "Usage Credits"
	    f.input :product_number, label: "Product Number / SKU"
		end
    f.actions
  end
end
