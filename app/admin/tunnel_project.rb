ActiveAdmin.register TunnelProject do
	menu false
	menu label: 'Project Details - DB1'

	preserve_default_filters!
	remove_filter :tunnel_product_frequency_channel_id, :marketing_id, :tunnel_id
	filter :tunnel_product_frequency_channel, as: :select, collection: ->{TunnelProductFrequencyChannel.order("id ASC").collect {|tpfc| [ tpfc.number_of_channels, tpfc.id ] }}
	filter :marketing, as: :select, collection: ->{Marketing.order("id ASC").collect {|marketing| [ marketing.vendor_name, marketing.id ] }}
	filter :tunnel , as: :select, collection: ->{Tunnel.order("id DESC").collect {|tunnel| [ tunnel.id, tunnel.id ] }}
	 
	index do
	  selectable_column
	  id_column
	  column :user_name
	  column :project_name
	  column :company
	  column "Tunnel Name", :name
	  column :facility_option
	  column :number_of_services
	  column :highest_frequency_band
	  column "Product Frequency Channel" do |project|
	  	project.tunnel_product_frequency_channel.try(:number_of_channels)
	  end
	  column :system_feed_method
	  column :system_architecture
	  column "Marketing" do |project|
	  	if project.marketing.present?
	  		link_to	project.marketing.try(:vendor_name), admin_marketing_path(project.marketing)
	  	elsif project.tunnel_type?
	  		"Customer has no vendor preference"
	  	else
	  		""
	  	end
	  end
	 	column "Expected RSSI at Mobile", :expected_rssi_at_mobile
	 	column :technology_type
	 	column :communication_type
	  column :created_at
	  column :updated_at
	  actions
	  column "" do |project|
	  	link_to "Download", report_export_tunnel_project_path(project) if project.report?
	  end
	end

	show title: proc{|project| "Tunnel project ##{project.id}"} do
    attributes_table do
      row :id
	  	row :user_name
		  row :project_name
		  row :company
		  row "Tunnel Name" do |project|
		  	project.name
		  end
		  row :facility_option
		  row :number_of_services
		  row :highest_frequency_band
		  row :tunnel_product_frequency_channel_id
		  row :system_feed_method
		  row :system_architecture
		  row "Marketing" do |project|
		  	if project.marketing.present?
		  		link_to	project.marketing.try(:vendor_name), admin_marketing_path(project.marketing)
		  	elsif project.tunnel_type?
		  		"Customer has no vendor preference"
		  	else
		  		""
		  	end
		  end
		  row "Expected RSSI at Mobile" do |project|
		  	project.expected_rssi_at_mobile
		  end
		 	row :technology_type
		 	row :communication_type
		  row "Download" do |project|
		  	link_to "Download ##{project.id}", report_export_tunnel_project_path(project) if project.report?
		  end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	actions :index, :show
end
