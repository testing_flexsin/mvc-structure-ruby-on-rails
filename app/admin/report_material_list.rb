ActiveAdmin.register ReportMaterialList do
	menu false
	menu label: 'Report Material List - DB15'
	
	permit_params :material_category_id, :description

	preserve_default_filters!
	remove_filter :material_category_id
	remove_filter :donor_direct_feed_quantities
	remove_filter :fiber_material_quantities
	remove_filter :product_price_lists
	remove_filter :part_quantity_informations
	filter :material_category, as: :select, collection: ->{MaterialCategory.order("id ASC").collect {|material_category| [material_category.category, material_category.id] }}, label: "Category"

	index do
	  selectable_column
	  id_column
	  column "Category" do |report_material_list|
	  	report_material_list.material_category.try(:category)
	  end
	  column :description
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
		  row "Category" do |report_material_list|
		  	report_material_list.material_category.try(:category)
		  end
      row :description
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :material_category_id, as: :select, collection: ->{MaterialCategory.order("id ASC").collect {|material_category| [material_category.category, material_category.id] }}, label: "Category"
	    f.input :description
		end
    f.actions
  end
end
