ActiveAdmin.register PartQuantityInformation do
	menu false
	menu label: 'Part Quantity Information - DB25'

	preserve_default_filters!
	remove_filter :building_data_entry_id, :report_material_list_id
	filter :building_data_entry, as: :select, collection: ->{BuildingDataEntry.order("id DESC").collect {|building_data_entry| [building_data_entry.building_name, building_data_entry.id] }}
	filter :report_material_list, as: :select, collection: ->{ReportMaterialList.order("id ASC").collect {|report_material_list| ["#{report_material_list.material_category.try(:category)} - #{report_material_list.description}", report_material_list.id] }}, label: "Category - Description"

  index do
	  selectable_column
	  id_column
	  column "Project" do |part_quantity_information|
	  	link_to part_quantity_information.building_data_entry.building.project_id, admin_project_path(part_quantity_information.building_data_entry.building.project_id)
	  end	 
	  column "Building" do |part_quantity_information|
	  	link_to part_quantity_information.building_data_entry.building_id, admin_building_path(part_quantity_information.building_data_entry.building_id)
	  end
	  column "Building Data Entry" do |part_quantity_information|
	    link_to part_quantity_information.building_data_entry_id, admin_building_data_entry_path(part_quantity_information.building_data_entry_id)
	  end
	  column "Building Number" do |part_quantity_information|
	  	part_quantity_information.building_data_entry.building_number
	  end
    column "Floor Number" do |part_quantity_information|
    	part_quantity_information.building_data_entry.floor_number
    end
	  column "Category" do |part_quantity_information|
      part_quantity_information.report_material_list.try(:material_category).try(:category)
    end
    column "Description" do |part_quantity_information|
      part_quantity_information.report_material_list.try(:description)
    end
	  column :quantity
	  column :created_at
	  column :updated_at
    actions 
	end

	show do
    attributes_table do
      row :id
		  row "Project" do |part_quantity_information|
		  	link_to "Project ##{part_quantity_information.building_data_entry.building.project_id}", admin_project_path(part_quantity_information.building_data_entry.building.project_id)
		  end
		  row "Building" do |part_quantity_information|
		  	link_to "Building ##{part_quantity_information.building_data_entry.building_id}", admin_building_path(part_quantity_information.building_data_entry.building_id)
		  end
      row :building_data_entry_id
		  row "Building Number" do |part_quantity_information|
		  	part_quantity_information.building_data_entry.building_number
		  end
	    row "Floor Number" do |part_quantity_information|
	    	part_quantity_information.building_data_entry.floor_number
	   end
		  row "Category" do |part_quantity_information|
	      part_quantity_information.report_material_list.try(:material_category).try(:category)
	    end
	    row "Description" do |part_quantity_information|
	      part_quantity_information.report_material_list.try(:description)
	    end
	  	row :quantity
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  actions :index, :show	
end
