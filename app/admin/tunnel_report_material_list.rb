ActiveAdmin.register TunnelReportMaterialList do
	menu false
	menu label: 'Report Material List - DB15'
	
	permit_params :tunnel_material_category_id, :description

	preserve_default_filters!
	remove_filter :tunnel_material_category_id
	remove_filter :tunnel_donor_direct_feed_quantities
	remove_filter :tunnel_fiber_material_quantities
	remove_filter :tunnel_product_price_lists
	remove_filter :tunnel_part_quantity_informations
	filter :tunnel_material_category, as: :select, collection: ->{TunnelMaterialCategory.order("id ASC").collect {|tunnel_material_category| [tunnel_material_category.category, tunnel_material_category.id] }}, label: "Category"

	index do
	  selectable_column
	  id_column
	  column "Category" do |tunnel_report_material_list|
	  	tunnel_report_material_list.tunnel_material_category.try(:category)
	  end
	  column :description
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
		  row "Category" do |tunnel_report_material_list|
		  	tunnel_report_material_list.tunnel_material_category.try(:category)
		  end
      row :description
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :tunnel_material_category_id, as: :select, collection: ->{TunnelMaterialCategory.order("id ASC").collect {|tunnel_material_category| [tunnel_material_category.category, tunnel_material_category.id] }}, label: "Category"
	    f.input :description
		end
    f.actions
  end
end

