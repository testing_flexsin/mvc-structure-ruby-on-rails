ActiveAdmin.register SquareFootageSize do
	menu false
	menu label: 'Square Footage Size'
  
  permit_params :square_foot
end
