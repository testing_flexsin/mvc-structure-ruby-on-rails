ActiveAdmin.register TrailPeriod do
	menu false
	menu label: 'Trail Period'

	permit_params :period

	index do
	  selectable_column
	  id_column
	  column "Trail Period (In Days)", :period
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
	    row :id
		  row "Trail Period (In Days)" do |trail_period|
		  	trail_period.period
		  end
	    row :created_at
	    row :updated_at
  	end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :period, label: "Trail Period (In Days)"
		end
    f.actions
  end
end
