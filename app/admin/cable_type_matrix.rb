ActiveAdmin.register CableTypeMatrix do
	menu false
	menu label: 'Cable Type Matrix - DB7'

	permit_params :radiating_cable_type, :cable_type_assigned

	index do
	  selectable_column
	  id_column
	  column :radiating_cable_type do |cable_type_matrix|
	  	case cable_type_matrix.radiating_cable_type
	  	when "cable_loss_1_by_2"
	  		"1/2 in"
	  	when "cable_loss_7_by_8"
	  		"7/8 in"
	  	when "cable_loss_11_by_4"
	  		"11/4 in"
	  	when "cable_loss_15_by_8"
	  		"15/8 in"
	  	else
	  		cable_type_matrix.radiating_cable_type
	  	end
	  end
	  column :cable_type_assigned
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
		  row :radiating_cable_type do |cable_type_matrix|
		  	case cable_type_matrix.radiating_cable_type
		  	when "cable_loss_1_by_2"
		  		"1/2 in"
		  	when "cable_loss_7_by_8"
		  		"7/8 in"
		  	when "cable_loss_11_by_4"
		  		"11/4 in"
		  	when "cable_loss_15_by_8"
		  		"15/8 in"
		  	else
		  		cable_type_matrix.radiating_cable_type
		  	end
		  end
		  row :cable_type_assigned
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :radiating_cable_type, as: :select, collection: [["1/2 in", "cable_loss_1_by_2"], ["7/8 in", "cable_loss_7_by_8"], ["11/4 in", "cable_loss_11_by_4"], ["15/8 in", "cable_loss_15_by_8"]]
	    f.input :cable_type_assigned, as: :select, collection: [['Yes', true], ['No', false]]
		end
    f.actions
  end
end
