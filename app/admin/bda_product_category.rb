ActiveAdmin.register BdaProductCategory do
	menu false
	
	permit_params :category

	preserve_default_filters!
	remove_filter :product_frequency_channels
	remove_filter :tunnel_product_frequency_channels
	remove_filter :coverage_area_per_bdas
	remove_filter :tunnel_coverage_distance_per_bdas
end
