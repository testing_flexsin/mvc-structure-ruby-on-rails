class ProjectMailer < ApplicationMailer
	def email_admin(project)
		@project = project
		static_page = StaticPage.last
		if static_page.present? && static_page.admin_email.present?
			mail(to: static_page.admin_email, subject: "New Project")
		else
			mail(to: ENV["ADMIN_MAIL"], subject: "New Project")
		end
	end
end
