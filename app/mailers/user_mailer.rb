class UserMailer < ApplicationMailer
	def verification(email)
		@email = email
		mail(to: email, subject: "Verification")
	end

	def retrieve_password(user, password)
		@user = user
		@password = password
		mail(to: user.email, subject: "Password")
	end

	def contact_us(contact)
		@contact = contact
		static_page = StaticPage.last
		if static_page.present? && static_page.admin_email.present?
			mail(to: static_page.admin_email, subject: "Contact Us")
		else
			mail(to: ENV["ADMIN_MAIL"], subject: "Contact Us")
		end
	end
end
