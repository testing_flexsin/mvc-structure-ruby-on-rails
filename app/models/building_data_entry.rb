class BuildingDataEntry < ActiveRecord::Base
	## Relations
	belongs_to :building
	has_one :link_budget, dependent: :destroy
	has_one :system_dimension, dependent: :destroy
	has_one :design_information, dependent: :destroy
	has_many :part_quantity_informations, dependent: :destroy

	## Validations
	validates :building_name, presence: true
	validates :building_id, :building_number, :number_of_floors, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :area_this_floor, :total_area_building, :frequency, :building_slope, numericality: true, allow_nil: true

	## Callbacks
	before_save :calculate_area
	after_save :save_system_dimension

	def calculate_area
		if !self.area_this_floor.present? && self.total_area_building.present?
			self.area_this_floor = self.total_area_building / self.number_of_floors
		end
		if self.area_this_floor.present? && !self.total_area_building.present?
			building_data_entries = self.building.building_data_entries.where(building_number: self.building_number)
			total_area = building_data_entries.sum(:area_this_floor) + self.area_this_floor
			building_data_entries.each do |building_data_entry|
				building_data_entry.update_column(:total_area_building, total_area)
			end
			self.total_area_building = total_area
		end
	end

	def save_system_dimension
		if self.frequency.present?
			area = 0.0
			if self.area_this_floor.present?
				area = self.area_this_floor
			elsif self.total_area_building.present?
				area = self.total_area_building / self.number_of_floors
			end
			estimated_path_distance = (1 / 3280.83) * (Math.sqrt(area) / 2.0)
			frequency = self.frequency * 1000.0

			self.build_system_dimension(estimated_path_distance: estimated_path_distance, frequency: frequency).save
		end
	end
end
