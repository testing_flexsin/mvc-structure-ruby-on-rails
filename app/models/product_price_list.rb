class ProductPriceList < ActiveRecord::Base
	## Relations
	belongs_to :report_material_list

	## Validations
	validates :vendor, :manufacturer, :mfg_pn, :our_pn, presence: true
	validates :report_material_list_id, :mfg_list_price, :our_cost, :discount_off_list, :our_sell_price, presence: true, numericality: { greater_than_or_equal_to: 0 }
end
