class StaticPage < ActiveRecord::Base
	## Soft Delete
	acts_as_paranoid

	## Uploaders
	mount_uploader :left_image, StaticUploader
	mount_uploader :right_image, StaticUploader
end
