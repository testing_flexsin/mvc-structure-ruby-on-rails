class Plan < ActiveRecord::Base
	## Relations
	has_many :building_users, class_name: 'User', foreign_key: 'building_plan_id'
	has_many :tunnel_users, class_name: 'User', foreign_key: 'tunnel_plan_id'

	## Validations
	validates :name, :product_number, presence: true
	validates :amount, presence: true, numericality: { greater_than_or_equal_to: 0 }
	validates :credits, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
