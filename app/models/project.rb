class Project < ActiveRecord::Base
	## Soft Delete
	acts_as_paranoid

	## Friendly ID
	extend FriendlyId
	friendly_id :project_name

	## Relations
	belongs_to :user
	belongs_to :product_frequency_channel
	belongs_to :marketing
	has_one :building, dependent: :destroy

	## Validations
	validates :user_id, presence: true, numericality: { only_integer: true }
	validates :user_name, :project_name, :company, :facility_option, presence: true
	validates :number_of_services, :highest_frequency_band, :product_frequency_channel_id, :marketing_id, numericality: { only_integer: true }, allow_nil: true

	## Callbacks
	after_update :save_building_data_entries_info, :send_admin_email

	## Show Per Page Records
	self.per_page = 10

	## Record every version of create, update, and destroy
	has_paper_trail

	## Override Friendly ID Method for generating slug
	def normalize_friendly_id(string)
		project_count = Project.where("project_name LIKE ?", "#{project_name}%").count
		if project_count > 0
	  	super.gsub("-", "_") + project_count.to_s
	  else
	  	super.gsub("-", "_")
	  end
	end

	## Class Methods
	def self.get_project(project_slug, user_id)
		self.where(slug: project_slug, user_id: user_id).eager_load(:building).order("projects.id ASC").first
	end

	def self.completed_projects(user_id, page)
		self.where(user_id: user_id, report: true).eager_load(:building).paginate(page: page).order("projects.id DESC")
	end

	def self.pending_projects(user_id, page)
		self.where(user_id: user_id, report: false).eager_load(:building).paginate(page: page).order("projects.id DESC")
	end

	## Instance Methods
	def project_details?
		self.step_completed == 1 ? true : false
	end

	def building_data?
		self.step_completed == 2 ? true : false
	end

	def building_services?
		self.step_completed == 3 ? true : false
	end

	def building_system?
		self.step_completed == 4 ? true : false
	end

	def building_type?
		self.step_completed == 5 ? true : false
	end

	def save_building_data_entries_info
		if building_type?
			link_budget_admin = LinkBudgetAdmin.last
			design_information_admin = DesignInformationAdmin.last
			if design_information_admin.present? && design_information_admin.maximum_antennas_per_bda.present?
				maximum_antennas_per_bda = design_information_admin.maximum_antennas_per_bda.to_f
			else
				maximum_antennas_per_bda = 14.0
			end
			equalization_multiplier = EqualizationMultiplier.last
			if equalization_multiplier.present? && equalization_multiplier.multiplied_factor.present?
				multiplied_factor = equalization_multiplier.multiplied_factor
			else
				multiplied_factor = 1
			end
			services_breakout = ServicesBreakout.where(number_of_services: self.number_of_services).last
			passive_component_loss = PassiveComponentLoss.last
			highest_frequency_band_id = HighestFrequencyBand.where(frequency: self.highest_frequency_band).last.try(:id)
			cable_loss_old = 0.0
			if highest_frequency_band_id.present?
				cable_loss_old = CableLoss.where(highest_frequency_band_id: highest_frequency_band_id).last.try(:cable_loss_1_by_2)
			end
			product_frequency_channel = self.product_frequency_channel
			coverage_area_per_bda = 0.0
			if product_frequency_channel.present?
				category_id = product_frequency_channel.bda_product_category.try(:id)
				coverage_area_per_bda = CoverageAreaPerBda.where(bda_product_category_id: category_id).last.try(:coverage_area)
			end
			report_material_lists = ReportMaterialList.eager_load(:donor_direct_feed_quantities, :fiber_material_quantities).order("report_material_lists.id ASC")

			building_data_entries = self.building.building_data_entries.eager_load(:system_dimension, :link_budget, :design_information, :part_quantity_informations).order("building_data_entries.id ASC")
			building_data_entries.each do |building_data_entry|
				system_dimension = building_data_entry.system_dimension
				## Save Link Budget Start
				channel_power = 0.0
				if product_frequency_channel.present?
					channel_power = ((product_frequency_channel.composite_power - 10 * (Math.log10(product_frequency_channel.number_of_channels))) - self.papr).round(1)
				end

				cable_length = 0.0
				antenna_gain = 0.0
				dl_margin = 0.0
				if link_budget_admin.present?
					cable_length = link_budget_admin.try(:cable_length)
					antenna_gain = link_budget_admin.try(:antenna_gain)
					dl_margin = link_budget_admin.try(:dl_margin)
				end

				cable_loss = cable_loss_old * cable_length

				splitter_loss = 0.0
				jumper_loss = 0.0
				connector_loss = 0.0
				if passive_component_loss.present?
					splitter_loss = passive_component_loss.quantity_of_splitters * passive_component_loss.try(passive_component_loss.type_of_splitter.to_sym)
					jumper_loss = passive_component_loss.try(:jumper_loss)
					connector_loss = passive_component_loss.try(:connector_loss)
				end

				antenna_erp = channel_power - cable_loss - splitter_loss - jumper_loss - connector_loss + antenna_gain

				allowed_pl = (20 * Math.log10(system_dimension.estimated_path_distance)) + (20 * Math.log10(system_dimension.frequency)) + 32.45

				rssi_at_portable = antenna_erp - dl_margin - allowed_pl

				estimated_path_loss = allowed_pl
				antenna_coverage_radius = (10 ** ((estimated_path_loss - 32.5 - (20 * Math.log10(building_data_entry.frequency))) / (10 * building_data_entry.building_slope))) * 3.2308

				building_data_entry.build_link_budget(channel_power: channel_power, cable_length: cable_length, cable_loss: cable_loss, splitter_loss: splitter_loss, jumper_loss: jumper_loss, connector_loss: connector_loss, antenna_gain: antenna_gain, antenna_erp: antenna_erp, dl_margin: dl_margin, allowed_pl: allowed_pl, rssi_at_portable: rssi_at_portable).save
				## Save Link Budget End
			end
		end
	end

	def check_number_of_antennas(number_of_antennas, number_of_antennas_per_bda, number_of_bda, initial_number_of_bda, maximum_antennas_per_bda)
		bda_multiplier = number_of_antennas_per_bda / maximum_antennas_per_bda
		if bda_multiplier.round == bda_multiplier.ceil
			bda_multiplier = bda_multiplier.round
		else
			bda_multiplier = bda_multiplier.round + 0.5
		end
		number_of_bda = bda_multiplier * initial_number_of_bda
		number_of_antennas_per_bda = (number_of_antennas / number_of_bda).round
		return number_of_bda, number_of_antennas_per_bda
	end

	def send_admin_email
		if self.report?
			ProjectMailer.delay.email_admin(self)
		end
	end
end
