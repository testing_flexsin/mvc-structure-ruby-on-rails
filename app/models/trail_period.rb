class TrailPeriod < ActiveRecord::Base
	## Validations
	validates :period, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
