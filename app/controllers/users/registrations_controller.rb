class Users::RegistrationsController < Devise::RegistrationsController
	respond_to :html, :js
	skip_before_action :trial_expired, only: [:edit]
	
  protected
		def update_resource(resource, params)
			if params[:password].present?
				result = resource.update_with_password(params)
				sign_in resource, bypass: true
			else
				result = resource.update_without_password(params)
			end
		end

	  def after_inactive_sign_up_path_for(resource)
	    new_user_session_path
	  end

	  def after_update_path_for(resource)
      edit_user_registration_path
    end
end