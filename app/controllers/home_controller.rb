class HomeController < ApplicationController
  ## Filters
  before_action :authenticate_user!, only: [:pricing]
  skip_before_action :set_static_page, only: [:save_contact_us]

  def about_us
  end

  def pricing
  end

  def help
  end

  def terms_and_conditions
  end

  def contact_us
    @contact = Contact.new
  end

  def save_contact_us
    @message = ""
    @saved = false
    contact = Contact.new(contact_params)
    if contact.save
      UserMailer.delay.contact_us(contact)
      @saved = true
      @message = "We will get back to you soon."
    else
      @message = "Sorry please try again."
    end
  end

  def privacy_policy
  end

  private
    def contact_params
      params.require(:contact).permit(:first_name, :last_name, :email, :contact_number, :message)
    end
end
