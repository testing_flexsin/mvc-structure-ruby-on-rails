class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  skip_before_action :verify_authenticity_token, if: Proc.new { |c| c.request.format == 'application/json' }
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :store_location, :get_new_user, :set_static_page

  ## Helper Methods
  helper_method :remaining_days

  def store_location
    return unless request.get?
    if (request.path != "/" &&
        request.path != "/users/sign_in" &&
        request.path != "/users/sign_up" &&
        request.path != "/users/password/new" &&
        request.path != "/users/password/edit" &&
        request.path != "/users/confirmation/new" &&
        request.path != "/users/sign_out" &&
        request.path != "/users/retrieve_password/new" &&
        !(request.path.include? "/admin") &&
        !request.xhr?)
      session[:previous_url] = request.fullpath
    end
  end
  
  def get_new_user
    unless user_signed_in?
      @new_user = User.new
    end
  end

  def set_static_page
    @static_page = StaticPage.last
  end

  def after_sign_in_path_for(resource)
    if resource.class.name == "User"
      if current_user.sign_in_count == 1
        welcome_path
      else
        session[:previous_url] || root_path
      end
    else
      admin_dashboard_path
    end
  end

  def after_sign_up_path_for(resource)
    if resource.class.name == "User"
      new_user_session_path
    else
      admin_dashboard_path
    end
  end

  def after_sign_out_path_for(resource)
    session[:previous_url] = ""
    root_path
  end
  
  protected
    def record_not_found
      redirect_to root_path
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:first_name, :last_name, :user_name, :company_name, :phone_number, :email, :password, :password_confirmation) }
      devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :user_name, :email, :password, :remember_me) }
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:first_name, :last_name, :user_name, :company_name, :phone_number, :email, :password, :password_confirmation, :current_password) }
    end
end
