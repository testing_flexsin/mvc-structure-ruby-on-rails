$(document).ready(function(){
  // Signin Popup form validation
	//-----------------------------------------------		
	if($("#login_popup").length>0) {
		$("#login_popup").validate({
      errorPlacement: function(error, element) {
        error.insertAfter( element );
      },
      rules: {
        "user[login]": {
          required: true,
          noSpace: true,
          maxlength: 255
        },
        "user[password]": {
          required: true,
          noSpace: true,
          minlength: 8,
          maxlength: 255
        }
      },
      messages: {
        "user[login]": {
          required: "Please enter your User Name or Email Id.",
          noSpace: "Please enter valid User Name or Email Id."
        },
				"user[password]": {
          required: "Please enter your Password.",
          noSpace: "Please enter valid Password.",
          minlength: "Your Password must be of minimum 8 characters.",
          maxlength: "Your Password must be of maximum 255 characters."
				}
      },
      highlight: function (element) {
        $(element).parent().removeClass("has-success").addClass("has-error");
      },
      success: function (element) {
        $(element).parent().removeClass("has-error").addClass("has-success");
        $(element).removeClass("error");
        $(element).addClass("display-table margin-0");
      }
    });
	}
});