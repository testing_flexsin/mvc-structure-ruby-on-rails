require 'rails_helper'

describe Project do
  it 'is valid with a user id' do
    expect(build(:project)).to be_valid
  end

  it 'is invalid without a user id' do
    expect(build(:project, user_id: nil)).to_not be_valid
  end
end