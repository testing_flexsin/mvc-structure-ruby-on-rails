require 'rails_helper'

describe NumberOfBuilding do
  it 'is valid with a building' do
    expect(build(:number_of_building)).to be_valid
  end

  it 'is invalid without a building' do
    expect(build(:number_of_building, buildings: nil)).to_not be_valid
  end
end