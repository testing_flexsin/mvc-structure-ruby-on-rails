role :app, %w{ubuntu@32.34.12.34}
role :web, %w{ubuntu@32.34.12.34}
role :db,  %w{ubuntu@32.34.12.34}

set :stage, :production
set :rails_env, :production

set :deploy_user, "ubuntu"

server '32.34.12.34', user: 'ubuntu', roles: %w{web app db}, primary: true

# set :ssh_options, {
# 	ubuntu: 'ubuntu',
# 	forward_agent: false,
# 	auth_methods: %w(password),
# 	password: 'password'
# }

set :ssh_options, {
	keys: %w(/home/ubuntu/buildingapp.pem),
	forward_agent: false,
	auth_methods: %w(publickey)
}