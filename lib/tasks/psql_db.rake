namespace :psql_db do
	desc "Backup DB every 1 month"
	task backup: :environment do
		begin
			Dir.chdir(File.join(Rails.root.to_s, "public"))
			Dir.mkdir("psql_db_backup") unless File.directory?("psql_db_backup") 
			
			`PGPASSWORD="ubuntu" pg_dump -U ubuntu building_production > /home/ubuntu/buildings/shared/public/psql_db_backup/database.sql`
		rescue => e
			Rails.logger.info "Backup DB: #{e}"
		end
	end
end