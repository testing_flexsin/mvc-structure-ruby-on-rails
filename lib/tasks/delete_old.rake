namespace :delete_old do
	desc "Deleting Old Versions of Paper Trail before 1 month"
	task versions: :environment do
		begin
			PaperTrail::Version.delete_all ["created_at < ?", 1.month.ago]
	  rescue => e
	  	Rails.logger.info "Deleting Old Versions of Paper Trail before 1 month: #{e}"
		end
	end
end