puts "Create Admin User"
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
puts "Done"

puts "Creating Number of Buildings"
NumberOfBuilding.create!([
  {buildings: 1},
  {buildings: 2},
  {buildings: 3},
  {buildings: 4},
  {buildings: 5},
  {buildings: 6},
  {buildings: 7},
  {buildings: 8},
  {buildings: 9},
  {buildings: 10}
])
puts "Done"

puts "Creating Number of Floors"
NumberOfFloor.create!([
  {floors: 1},
  {floors: 2},
  {floors: 3},
  {floors: 4},
  {floors: 5},
  {floors: 6},
  {floors: 7},
  {floors: 8},
  {floors: 9},
  {floors: 10},
  {floors: 11},
  {floors: 12},
  {floors: 13},
  {floors: 14},
  {floors: 15},
  {floors: 16},
  {floors: 17},
  {floors: 18},
  {floors: 19},
  {floors: 20},
  {floors: 21},
  {floors: 22},
  {floors: 23},
  {floors: 24},
  {floors: 25},
  {floors: 26},
  {floors: 27},
  {floors: 28},
  {floors: 29},
  {floors: 30},
  {floors: 31},
  {floors: 32},
  {floors: 33},
  {floors: 34},
  {floors: 35},
  {floors: 36},
  {floors: 37},
  {floors: 38},
  {floors: 39},
  {floors: 40},
  {floors: 41},
  {floors: 42},
  {floors: 43},
  {floors: 44},
  {floors: 45},
  {floors: 46},
  {floors: 47},
  {floors: 48},
  {floors: 49},
  {floors: 50}
])
puts "Done"

puts "Creating Number of Services"
NumberOfService.create!([
  {services: 1},
  {services: 2},
  {services: 3},
  {services: 4},
  {services: 5},
  {services: 6},
  {services: 7},
  {services: 8},
  {services: 9},
  {services: 10}
])
puts "Done"

puts "Creating Number of Tunnels"
NumberOfTunnel.create!([
  {tunnels: 1},
  {tunnels: 2},
  {tunnels: 3},
  {tunnels: 4},
  {tunnels: 5},
  {tunnels: 6},
  {tunnels: 7},
  {tunnels: 8},
  {tunnels: 9},
  {tunnels: 10}
])
puts "Done"

puts "Creating Number of Bores"
NumberOfBore.create!([
  {bores: 1},
  {bores: 2},
  {bores: 3},
  {bores: 4},
  {bores: 5},
  {bores: 6},
  {bores: 7},
  {bores: 8},
  {bores: 9},
  {bores: 10}
])
puts "Done"

puts "Creating Number of Segments"
NumberOfSegment.create!([
  {segments: 1},
  {segments: 2},
  {segments: 3},
  {segments: 4},
  {segments: 5},
  {segments: 6},
  {segments: 7},
  {segments: 8},
  {segments: 9},
  {segments: 10}
])
puts "Done"

puts "Creating Number of Radio Rooms"
NumberOfRadioRoom.create!([
  {radio_rooms: 1},
  {radio_rooms: 2},
  {radio_rooms: 3},
  {radio_rooms: 4},
  {radio_rooms: 5},
  {radio_rooms: 6},
  {radio_rooms: 7},
  {radio_rooms: 8},
  {radio_rooms: 9},
  {radio_rooms: 10}
])
puts "Done"

puts "Creating Highest Frequency Bands"
HighestFrequencyBand.create!([
	{frequency: 5},
	{frequency: 10},
	{frequency: 20},
	{frequency: 30},
	{frequency: 50},
  {frequency: 88},
  {frequency: 100},
  {frequency: 108},
  {frequency: 150},
  {frequency: 174},
  {frequency: 200},
  {frequency: 300},
  {frequency: 400},
  {frequency: 450},
  {frequency: 500},
  {frequency: 512},
  {frequency: 600},
  {frequency: 700},
  {frequency: 800},
  {frequency: 824},
  {frequency: 894},
  {frequency: 960},
  {frequency: 1000},
  {frequency: 1250},
  {frequency: 1500},
  {frequency: 1700},
  {frequency: 1950},
  {frequency: 2000},
  {frequency: 2300},
  {frequency: 2400},
  {frequency: 3000},
  {frequency: 3300},
  {frequency: 3400},
  {frequency: 4000},
  {frequency: 4900},
  {frequency: 5000},
  {frequency: 5200},
  {frequency: 5300},
  {frequency: 5600},
  {frequency: 6000}
])
puts "Done"

puts "Creating BDA Product Categories"
BdaProductCategory.create!([
  {category: "Low Power"},
  {category: "Medium Power"},
  {category: "High Power"}
])
puts "Done"

puts "Creating Regions"
Region.create!([
  {name: "North America"},
  {name: "CALA"},
  {name: "Europe"},
  {name: "Russia & CIS"},
  {name: "Middle East"},
  {name: "Africa"},
  {name: "Asia/Pacific"},
  {name: "Canada"}
])
puts "Done"

puts "Creating Material Categories"
MaterialCategory.create!([
  {category: "IN-BUILDING DAS"},
  {category: "BI-DIRECTIONAL AMP"},
  {category: "DIRECT FEED"},
  {category: "FIBER OPTIC"},
  {category: "OFF-AIR DONOR"},
  {category: "TUNNEL DAS"}
])
puts "Done"

puts "Creating Passive Component Losses"
PassiveComponentLoss.create!([
  {jumper_loss: 0.5, connector_loss: 0.1, way2_splitter_loss: 3.5, way3_splitter_loss: 5.5, way4_splitter_loss: 6.5, way6_splitter_loss: 8.2, way8_splitter_loss: 9.5, directional_coupler_loss: 0.8, hybrid_coupler_loss: 3.5, quantity_of_splitters: 3, type_of_splitter: "way2_splitter_loss"}
])
puts "Done"

puts "Creating Timer Settings"
TimerSetting.create!([
  {plan_name: "Basic", report_download_timer: 20, report_generate_timer: 20},
  {plan_name: "Standard", report_download_timer: 10, report_generate_timer: 10},
  {plan_name: "Professional", report_download_timer: 5, report_generate_timer: 5}
])
puts "Done"

puts "Creating Splitter Matrices"
(1..1000).each do |number_of_bdas|
  (1..14).each do |antennas|
    if number_of_bdas % 2 == 0
      case antennas
      when 1
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 0.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 2
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 0.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 3
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 1.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 4
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 1.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 5
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 1.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 6
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 1.0, product_type_3w: 2.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 7
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 4.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 8
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 2.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 9
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 6.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 10
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 5.0, product_type_3w: 2.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 11
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 4.0, product_type_3w: 3.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 12
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 4.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 13
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 2.0, product_type_3w: 5.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 14
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 5.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      end
    else
      case antennas
      when 1
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 0.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 2
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 1.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 3
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 0.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 4
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 5
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 2.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 6
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 1.0, product_type_3w: 2.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 7
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 4.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 8
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 2.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 9
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 6.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 10
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 5.0, product_type_3w: 2.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 11
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 4.0, product_type_3w: 3.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 12
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 4.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 13
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 2.0, product_type_3w: 5.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 14
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 5.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      end
    end
  end
end
puts "Done"

puts "Creating Tunnel Splitter Matrices"
TunnelSplitterMatrix.create!([
  {number_of_bdas: 1, product_type_2w: 1.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 2, product_type_2w: 2.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 3, product_type_2w: 3.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 4, product_type_2w: 4.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 5, product_type_2w: 5.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 6, product_type_2w: 6.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 7, product_type_2w: 7.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 8, product_type_2w: 8.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 9, product_type_2w: 9.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 10, product_type_2w: 10.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 11, product_type_2w: 11.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 12, product_type_2w: 12.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 13, product_type_2w: 13.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 14, product_type_2w: 14.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 15, product_type_2w: 15.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 16, product_type_2w: 16.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 17, product_type_2w: 17.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 18, product_type_2w: 18.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 19, product_type_2w: 19.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 20, product_type_2w: 20.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 21, product_type_2w: 21.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 22, product_type_2w: 22.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 23, product_type_2w: 23.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 24, product_type_2w: 24.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 25, product_type_2w: 25.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 26, product_type_2w: 26.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 27, product_type_2w: 27.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 28, product_type_2w: 28.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 29, product_type_2w: 29.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 30, product_type_2w: 30.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 31, product_type_2w: 31.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 32, product_type_2w: 32.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 33, product_type_2w: 33.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 34, product_type_2w: 34.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 35, product_type_2w: 35.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 36, product_type_2w: 36.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 37, product_type_2w: 37.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 38, product_type_2w: 38.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 39, product_type_2w: 39.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 40, product_type_2w: 40.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 41, product_type_2w: 41.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 42, product_type_2w: 42.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 43, product_type_2w: 43.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 44, product_type_2w: 44.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 45, product_type_2w: 45.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 46, product_type_2w: 46.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 47, product_type_2w: 47.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 48, product_type_2w: 48.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 49, product_type_2w: 49.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 50, product_type_2w: 50.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
])
puts "Done"

puts "Creating Tunnel Length"
TunnelLength.create!([
  {length: 2640.0}
])
puts "Done"

puts "Creating Cable Losses"
CableLoss.create!([
  {highest_frequency_band_id: 1, cable_loss_1_by_2: 0.00159, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 2, cable_loss_1_by_2: 0.00225, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 3, cable_loss_1_by_2: 0.00319, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 4, cable_loss_1_by_2: 0.00391, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 5, cable_loss_1_by_2: 0.00507, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 6, cable_loss_1_by_2: 0.00676, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 7, cable_loss_1_by_2: 0.00722, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 8, cable_loss_1_by_2: 0.00751, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 9, cable_loss_1_by_2: 0.00889, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 10, cable_loss_1_by_2: 0.0096, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 11, cable_loss_1_by_2: 0.0103, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 12, cable_loss_1_by_2: 0.0127, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 13, cable_loss_1_by_2: 0.0148, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 14, cable_loss_1_by_2: 0.0157, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 15, cable_loss_1_by_2: 0.0166, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 16, cable_loss_1_by_2: 0.0168, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 17, cable_loss_1_by_2: 0.0183, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 18, cable_loss_1_by_2: 0.0199, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 19, cable_loss_1_by_2: 0.0213, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 20, cable_loss_1_by_2: 0.0219, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 21, cable_loss_1_by_2: 0.0226, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 22, cable_loss_1_by_2: 0.0235, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 23, cable_loss_1_by_2: 0.024, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 24, cable_loss_1_by_2: 0.0271, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 25, cable_loss_1_by_2: 0.0299, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 26, cable_loss_1_by_2: 0.032, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 27, cable_loss_1_by_2: 0.0345, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 28, cable_loss_1_by_2: 0.035, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 29, cable_loss_1_by_2: 0.0376, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 30, cable_loss_1_by_2: 0.0392, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 31, cable_loss_1_by_2: 0.0438, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 32, cable_loss_1_by_2: 0.0452, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 33, cable_loss_1_by_2: 0.0481, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 34, cable_loss_1_by_2: 0.0545, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 35, cable_loss_1_by_2: 0.0658, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 36, cable_loss_1_by_2: 0.0628, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 37, cable_loss_1_by_2: 0.0654, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 38, cable_loss_1_by_2: 0.061, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 39, cable_loss_1_by_2: 0.0609, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 40, cable_loss_1_by_2: 0.0638, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil}
])
puts "Done"

puts "Creating Technology Types"
TechnologyType.create!([
  {technology: "CDMA", papr: 10.0, default_selected: false},
  {technology: "GSM", papr: 5.0, default_selected: false},
  {technology: "P25", papr: 3.0, default_selected: false},
  {technology: "IDEN", papr: 3.0, default_selected: false},
  {technology: "TETRA", papr: 3.0, default_selected: false},
  {technology: "Analog FM", papr: 3.0, default_selected: true},
  {technology: "TDMA", papr: 3.0, default_selected: false},
  {technology: "UMTS", papr: 10.0, default_selected: false},
  {technology: "LTE", papr: 20.0, default_selected: false},
  {technology: "Wi-Fi", papr: 20.0, default_selected: false},
  {technology: "CW", papr: 0.0, default_selected: false}
])
puts "Done"

puts "Creating Tunnel Product Frequency Channels"
TunnelProductFrequencyChannel.create!([
  {bda_product_category_id: 1, number_of_channels: 1, composite_power: 30.0, channel_power: 27.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 2, composite_power: 30.0, channel_power: 24.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 3, composite_power: 30.0, channel_power: 22.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 4, composite_power: 30.0, channel_power: 21.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 5, composite_power: 30.0, channel_power: 20.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 6, composite_power: 30.0, channel_power: 19.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 7, composite_power: 30.0, channel_power: 18.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 8, composite_power: 30.0, channel_power: 18.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 9, composite_power: 30.0, channel_power: 17.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 10, composite_power: 30.0, channel_power: 17.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 11, composite_power: 30.0, channel_power: 16.6, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 12, composite_power: 30.0, channel_power: 16.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 13, composite_power: 30.0, channel_power: 15.9, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 14, composite_power: 30.0, channel_power: 15.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 15, composite_power: 30.0, channel_power: 15.2, papr: 3.0, default_selected: true},
  {bda_product_category_id: 1, number_of_channels: 16, composite_power: 30.0, channel_power: 15.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 17, composite_power: 30.0, channel_power: 14.7, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 18, composite_power: 30.0, channel_power: 14.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 19, composite_power: 30.0, channel_power: 14.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 20, composite_power: 30.0, channel_power: 14.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 21, composite_power: 30.0, channel_power: 13.8, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 22, composite_power: 30.0, channel_power: 13.6, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 23, composite_power: 30.0, channel_power: 13.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 24, composite_power: 30.0, channel_power: 13.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 25, composite_power: 30.0, channel_power: 13.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 26, composite_power: 30.0, channel_power: 12.9, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 27, composite_power: 30.0, channel_power: 12.7, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 28, composite_power: 30.0, channel_power: 12.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 29, composite_power: 30.0, channel_power: 12.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 30, composite_power: 30.0, channel_power: 12.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 31, composite_power: 30.0, channel_power: 12.1, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 32, composite_power: 34.0, channel_power: 15.9, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 33, composite_power: 34.0, channel_power: 15.8, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 34, composite_power: 34.0, channel_power: 15.7, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 35, composite_power: 34.0, channel_power: 15.6, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 36, composite_power: 34.0, channel_power: 15.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 37, composite_power: 34.0, channel_power: 15.3, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 38, composite_power: 34.0, channel_power: 15.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 39, composite_power: 34.0, channel_power: 15.1, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 40, composite_power: 34.0, channel_power: 15.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 41, composite_power: 34.0, channel_power: 14.9, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 42, composite_power: 34.0, channel_power: 14.8, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 43, composite_power: 34.0, channel_power: 14.7, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 44, composite_power: 34.0, channel_power: 14.6, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 45, composite_power: 34.0, channel_power: 14.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 46, composite_power: 34.0, channel_power: 14.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 47, composite_power: 34.0, channel_power: 14.3, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 48, composite_power: 34.0, channel_power: 14.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 49, composite_power: 34.0, channel_power: 14.1, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 50, composite_power: 34.0, channel_power: 14.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 51, composite_power: 34.0, channel_power: 13.9, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 52, composite_power: 34.0, channel_power: 13.8, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 53, composite_power: 34.0, channel_power: 13.8, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 54, composite_power: 34.0, channel_power: 13.7, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 55, composite_power: 34.0, channel_power: 13.6, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 56, composite_power: 34.0, channel_power: 13.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 57, composite_power: 34.0, channel_power: 13.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 58, composite_power: 34.0, channel_power: 13.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 59, composite_power: 34.0, channel_power: 13.3, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 60, composite_power: 34.0, channel_power: 13.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 61, composite_power: 34.0, channel_power: 13.1, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 62, composite_power: 34.0, channel_power: 13.1, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 63, composite_power: 34.0, channel_power: 13.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 64, composite_power: 34.0, channel_power: 12.9, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 65, composite_power: 34.0, channel_power: 12.9, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 66, composite_power: 34.0, channel_power: 12.8, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 67, composite_power: 34.0, channel_power: 12.7, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 68, composite_power: 34.0, channel_power: 12.7, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 69, composite_power: 34.0, channel_power: 12.6, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 70, composite_power: 34.0, channel_power: 12.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 71, composite_power: 34.0, channel_power: 12.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 72, composite_power: 34.0, channel_power: 12.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 73, composite_power: 34.0, channel_power: 12.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 74, composite_power: 34.0, channel_power: 12.3, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 75, composite_power: 34.0, channel_power: 12.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 76, composite_power: 34.0, channel_power: 12.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 77, composite_power: 34.0, channel_power: 12.1, papr: 3.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 78, composite_power: 34.0, channel_power: 12.1, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 79, composite_power: 37.0, channel_power: 15.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 80, composite_power: 37.0, channel_power: 15.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 81, composite_power: 37.0, channel_power: 14.9, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 82, composite_power: 37.0, channel_power: 14.9, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 83, composite_power: 37.0, channel_power: 14.8, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 84, composite_power: 37.0, channel_power: 14.8, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 85, composite_power: 37.0, channel_power: 14.7, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 86, composite_power: 37.0, channel_power: 14.7, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 87, composite_power: 37.0, channel_power: 14.6, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 88, composite_power: 37.0, channel_power: 14.6, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 89, composite_power: 37.0, channel_power: 14.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 90, composite_power: 37.0, channel_power: 14.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 91, composite_power: 37.0, channel_power: 14.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 92, composite_power: 37.0, channel_power: 14.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 93, composite_power: 37.0, channel_power: 14.3, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 94, composite_power: 37.0, channel_power: 14.3, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 95, composite_power: 37.0, channel_power: 14.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 96, composite_power: 37.0, channel_power: 14.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 97, composite_power: 37.0, channel_power: 14.1, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 98, composite_power: 37.0, channel_power: 14.1, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 99, composite_power: 37.0, channel_power: 14.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 100, composite_power: 37.0, channel_power: 14.0, papr: 3.0, default_selected: false}
])
puts "Done"

puts "Creating Cable Type Matrices"
CableTypeMatrix.create!([
  {radiating_cable_type: "cable_loss_1_by_2", cable_type_assigned: true},
  {radiating_cable_type: "cable_loss_7_by_8", cable_type_assigned: false},
  {radiating_cable_type: "cable_loss_11_by_4", cable_type_assigned: false},
  {radiating_cable_type: "cable_loss_15_by_8", cable_type_assigned: false}
])
puts "Done"

puts "Creating Tunnel Material Categories"
TunnelMaterialCategory.create!([
  {category: "BI-DIRECTIONAL AMP"},
  {category: "DIRECT FEED"},
  {category: "FIBER OPTIC"},
  {category: "OFF-AIR DONOR"},
  {category: "TUNNEL DAS"}
])
puts "Done"

puts "Creating Marketings"
Marketing.create!([
  {region_id: 1, vendor_name: "Vendor 1"},
  {region_id: 1, vendor_name: "Vendor 2"},
  {region_id: 1, vendor_name: "Vendor 3"},
  {region_id: 1, vendor_name: "Vendor 4"},
  {region_id: 1, vendor_name: "Vendor 5"},
  {region_id: 1, vendor_name: "Vendor 6"},
  {region_id: 2, vendor_name: "Vendor 1"},
  {region_id: 2, vendor_name: "Vendor 2"},
  {region_id: 3, vendor_name: "Vendor 1"},
  {region_id: 3, vendor_name: "Vendor 2"},
  {region_id: 3, vendor_name: "Vendor 3"},
  {region_id: 3, vendor_name: "Vendor 4"},
  {region_id: 3, vendor_name: "Vendor 5"},
  {region_id: 3, vendor_name: "Vendor 6"},
  {region_id: 3, vendor_name: "Vendor 7"},
  {region_id: 3, vendor_name: "Vendor 8"},
  {region_id: 4, vendor_name: "Vendor 1"},
  {region_id: 4, vendor_name: "Vendor 2"},
  {region_id: 5, vendor_name: "Vendor 1"},
  {region_id: 5, vendor_name: "Vendor 2"},
  {region_id: 5, vendor_name: "Vendor 3"},
  {region_id: 5, vendor_name: "Vendor 4"},
  {region_id: 5, vendor_name: "Vendor 5"},
  {region_id: 6, vendor_name: "Vendor 1"},
  {region_id: 6, vendor_name: "Vendor 2"},
  {region_id: 6, vendor_name: "Vendor 3"},
  {region_id: 6, vendor_name: "Vendor 4"},
  {region_id: 6, vendor_name: "Vendor 5"},
  {region_id: 7, vendor_name: "Vendor 1"},
  {region_id: 7, vendor_name: "Vendor 2"},
  {region_id: 7, vendor_name: "Vendor 3"},
  {region_id: 7, vendor_name: "Vendor 4"},
  {region_id: 7, vendor_name: "Vendor 5"},
  {region_id: 7, vendor_name: "Vendor 6"},
  {region_id: 7, vendor_name: "Vendor 7"},
  {region_id: 8, vendor_name: "Vendor 1"},
  {region_id: 8, vendor_name: "Vendor 2"},
  {region_id: 8, vendor_name: "Vendor 3"}
])
puts "Done"

puts "Creating Report Material Lists"
ReportMaterialList.create!([
  {material_category_id: 1, description: "1/2 inch plenum rated coaxial cable (ft)"},
  {material_category_id: 1, description: "Crossband Coupler, N-type connectors"},
  {material_category_id: 1, description: "Directional Coupler (broadband), N-type connectors"},
  {material_category_id: 1, description: "4 way splitters (broadband), N-type connectors"},
  {material_category_id: 1, description: "3 way splitters (broadband), N-type connectors"},
  {material_category_id: 1, description: "2 way splitters (broadband), N-type connectors"},
  {material_category_id: 1, description: "Hybrid Combiner, N-type connectors"},
  {material_category_id: 1, description: "Custom Built Filtering, N-type connectors"},
  {material_category_id: 1, description: "Coaxial RF jumpers, plenum rated, N-Male Connectors (3ft)"},
  {material_category_id: 1, description: "N-Male Connectors (for 1/2 inch plenum rated coaxial cable)"},
  {material_category_id: 1, description: "InBuilding Antenna (broadband), N-type connectors"},
  {material_category_id: 2, description: "Low Power (1 Watt Composite Power) BDA Unit Connected to DAS"},
  {material_category_id: 2, description: "Medium Power (2 Watts Composite Power) BDA Unit Connected to DAS"},
  {material_category_id: 2, description: "High Power (5 Watts Composite Power) BDA Unit Connected to DAS"},
  {material_category_id: 2, description: "Medium Power (2 Watts Composite Power) BDA Unit Connected to OFF-AIR DONOR"},
  {material_category_id: 3, description: "Base Station Interface (Point to Interconnect)"},
  {material_category_id: 3, description: "Hybrid Combiner, N-type connectors"},
  {material_category_id: 3, description: "Custom Built Filtering, N-type connectors"},
  {material_category_id: 4, description: "Master Fiber Optic Transceiver (Single Port)"},
  {material_category_id: 4, description: "Master Fiber Optic Transceiver (8 Fiber Optic Ports)"},
  {material_category_id: 4, description: "Slave Fiber Optic Transceiver (Single Port)"},
  {material_category_id: 4, description: "Fiber Optic Jumpers (10ft)"},
  {material_category_id: 4, description: "Fiber Optic Backbone Cable (ft)"},
  {material_category_id: 5, description: "Off-air donor antenna, Yagi, minimum 10dBi Gain"},
  {material_category_id: 5, description: "DC Block Donor, Coaxial Line Protector (N-Female connectors)"},
  {material_category_id: 5, description: "1/2 inch Off-air donor antenna cable (ft)"},
  {material_category_id: 5, description: "N-Male connectors for off-air donor antenna cables"},
  {material_category_id: 5, description: "Ground Kit for 1/2 inch Coax (for off-air donor antenna cable outer conductor)"},
  {material_category_id: 5, description: "Universal Weatherproofing Kit (for off-air donor antenna cable connectors)"},
  {material_category_id: 5, description: "Prep Tool for 1/2 inch donor cable (all-in-one strip tool)"},
  {material_category_id: 6, description: "1-5/8 inch Diameter Radiating Cable (ft)"},
  {material_category_id: 6, description: "1-1/4 inch Diameter Radiating Cable (ft)"},
  {material_category_id: 6, description: "7/8 inch Diameter Radiating Cable (ft)"},
  {material_category_id: 6, description: "1/2 inch Diameter Radiating Cable (ft)"},
  {material_category_id: 6, description: "1/2 inch plenum rated coaxial cable (ft)"},
  {material_category_id: 6, description: "2 way splitters (broadband),N-type connectors"},
  {material_category_id: 6, description: "3 way splitters (broadband),N-type connectors"},
  {material_category_id: 6, description: "4 way splitters (broadband),N-type connectors"},
  {material_category_id: 6, description: "Directional Coupler (broadband),N-type connectors"},
  {material_category_id: 6, description: "Crossband Coupler ,N-type connectors"},
  {material_category_id: 6, description: "Hybrid Combiner,N-type connectors"},
  {material_category_id: 6, description: "Custom Built Filtering,N-type connectors"},
  {material_category_id: 6, description: "Coaxial RF jumpers, plenum rated, N-Male Connectors (3ft)"},
  {material_category_id: 6, description: "N-Male Connectors (for 1/2 inch plenum rated coaxial cable)"},
  {material_category_id: 6, description: "Antenna, Yagi, minimum 10dBi Gain, N-type connectors"},
  {material_category_id: 6, description: "Corresponding size cable clamps (kit or individual)"},
  {material_category_id: 6, description: "Corresponding Size Radiating Cable Connector (N-Type)"},
  {material_category_id: 6, description: "End of Cable 50 Ohm Load (N-Type Connector)"}
])
puts "Done"

puts "Creating Square Footage Size"
SquareFootageSize.create!([
  {square_foot: 500000.0}
])
puts "Done"

puts "Creating Tunnel Cable I Losses"
TunnelCableILoss.create!([
  {highest_frequency_band_id: 1, cable_loss_1_by_2: 0.00159, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 2, cable_loss_1_by_2: 0.00225, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 3, cable_loss_1_by_2: 0.00319, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 4, cable_loss_1_by_2: 0.00391, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 5, cable_loss_1_by_2: 0.00507, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 6, cable_loss_1_by_2: 0.00676, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 7, cable_loss_1_by_2: 0.00722, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 8, cable_loss_1_by_2: 0.00751, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 9, cable_loss_1_by_2: 0.00889, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 10, cable_loss_1_by_2: 0.0096, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 11, cable_loss_1_by_2: 0.0103, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 12, cable_loss_1_by_2: 0.0127, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 13, cable_loss_1_by_2: 0.0148, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 14, cable_loss_1_by_2: 0.0157, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 15, cable_loss_1_by_2: 0.0166, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 16, cable_loss_1_by_2: 0.0168, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 17, cable_loss_1_by_2: 0.0183, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 18, cable_loss_1_by_2: 0.0199, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 19, cable_loss_1_by_2: 0.0213, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 20, cable_loss_1_by_2: 0.0219, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 21, cable_loss_1_by_2: 0.0226, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 22, cable_loss_1_by_2: 0.0235, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 23, cable_loss_1_by_2: 0.024, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 24, cable_loss_1_by_2: 0.0271, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 25, cable_loss_1_by_2: 0.0299, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 26, cable_loss_1_by_2: 0.032, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 27, cable_loss_1_by_2: 0.0345, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 28, cable_loss_1_by_2: 0.035, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 29, cable_loss_1_by_2: 0.0376, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 30, cable_loss_1_by_2: 0.0392, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 31, cable_loss_1_by_2: 0.0438, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 32, cable_loss_1_by_2: 0.0452, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 33, cable_loss_1_by_2: 0.0481, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 34, cable_loss_1_by_2: 0.0545, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 35, cable_loss_1_by_2: 0.0658, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 36, cable_loss_1_by_2: 0.0628, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 37, cable_loss_1_by_2: 0.0654, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 38, cable_loss_1_by_2: 0.061, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 39, cable_loss_1_by_2: 0.0609, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 40, cable_loss_1_by_2: 0.0638, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil}
])
puts "Done"

puts "Creating RSSI Threshold Level Benchmarks"
RssiThresholdLevelBenchmark.create!([
  {threshold_level: -50, default_selected: false},
  {threshold_level: -55, default_selected: false},
  {threshold_level: -60, default_selected: false},
  {threshold_level: -65, default_selected: false},
  {threshold_level: -70, default_selected: false},
  {threshold_level: -75, default_selected: false},
  {threshold_level: -80, default_selected: false},
  {threshold_level: -85, default_selected: true},
  {threshold_level: -90, default_selected: false},
  {threshold_level: -95, default_selected: false},
  {threshold_level: -100, default_selected: false}
])
puts "Done"

puts "Creating Tunnel Report Material Lists"
TunnelReportMaterialList.create!([
  {tunnel_material_category_id: 1, description: "Low Power (1 Watt Composite Power) BDA Unit Connected to DAS"},
  {tunnel_material_category_id: 1, description: "Medium Power (2 Watts Composite Power) BDA Unit Connected to DAS"},
  {tunnel_material_category_id: 1, description: "High Power (5 Watts Composite Power) BDA Unit Connected to DAS"},
  {tunnel_material_category_id: 1, description: "Medium Power (2 Watts Composite Power) BDA Unit Connected to OFF-AIR DONOR"},
  {tunnel_material_category_id: 2, description: "Base Station Interface (Point to Interconnect)"},
  {tunnel_material_category_id: 2, description: "Hybrid Combiner, N-type connectors"},
  {tunnel_material_category_id: 2, description: "Custom Built Filtering, N-type connectors"},
  {tunnel_material_category_id: 3, description: "Master Fiber Optic Transceiver (Single Port)"},
  {tunnel_material_category_id: 3, description: "Master Fiber Optic Transceiver (8 Fiber Optic Ports)"},
  {tunnel_material_category_id: 3, description: "Slave Fiber Optic Transceiver (Single Port)"},
  {tunnel_material_category_id: 3, description: "Fiber Optic Jumpers (3ft)"},
  {tunnel_material_category_id: 3, description: "Fiber Optic Backbone Cable (ft)"},
  {tunnel_material_category_id: 4, description: "Off-air donor antenna, Yagi, minimum 10dBi Gain"},
  {tunnel_material_category_id: 4, description: "DC Block Donor, Coaxial Line Protector (N-Female connectors)"},
  {tunnel_material_category_id: 4, description: "1/2 inch Off-air donor antenna cable (ft)"},
  {tunnel_material_category_id: 4, description: "N-Male connectors for off-air donor antenna cables"},
  {tunnel_material_category_id: 4, description: "Ground Kit for 1/2 inch Coax (for off-air donor antenna cable outer conductor)"},
  {tunnel_material_category_id: 4, description: "Universal Weatherproofing Kit (for off-air donor antenna cable connectors)"},
  {tunnel_material_category_id: 4, description: "Prep Tool for 1/2 inch donor cable (all-in-one strip tool)"},
  {tunnel_material_category_id: 5, description: "1-5/8 inch Diameter Radiating Cable (ft)"},
  {tunnel_material_category_id: 5, description: "1-1/4 inch Diameter Radiating Cable (ft)"},
  {tunnel_material_category_id: 5, description: "7/8 inch Diameter Radiating Cable (ft)"},
  {tunnel_material_category_id: 5, description: "1/2 inch Diameter Radiating Cable (ft)"},
  {tunnel_material_category_id: 5, description: "1/2 inch plenum rated coaxial cable (ft)"},
  {tunnel_material_category_id: 5, description: "2 way splitters (broadband),N-type connectors"},
  {tunnel_material_category_id: 5, description: "3 way splitters (broadband),N-type connectors"},
  {tunnel_material_category_id: 5, description: "4 way splitters (broadband),N-type connectors"},
  {tunnel_material_category_id: 5, description: "Directional Coupler (broadband),N-type connectors"},
  {tunnel_material_category_id: 5, description: "Crossband Coupler ,N-type connectors"},
  {tunnel_material_category_id: 5, description: "Hybrid Combiner,N-type connectors"},
  {tunnel_material_category_id: 5, description: "Custom Built Filtering,N-type connectors"},
  {tunnel_material_category_id: 5, description: "Coaxial RF jumpers, plenum rated, N-Male Connectors (3ft)"},
  {tunnel_material_category_id: 5, description: "N-Male Connectors (for 1/2 inch plenum rated coaxial cable)"},
  {tunnel_material_category_id: 5, description: "Antenna, Yagi, minimum 10dBi Gain, N-type connectors"},
  {tunnel_material_category_id: 5, description: "Corresponding size cable clamps (kit or individual)"},
  {tunnel_material_category_id: 5, description: "Corresponding Size Radiating Cable Connector (N-Type)"},
  {tunnel_material_category_id: 5, description: "End of Cable 50 Ohm Load (N-Type Connector)"}
])
puts "Done"

puts "Creating Link Budget Admin"
LinkBudgetAdmin.create!([
  {cable_length: 120.0, antenna_gain: 0.0, dl_margin: 3.0}
])
puts "Done"

puts "Creating Tunnel Link Budget Admin"
TunnelLinkBudgetAdmin.create!([
  {indoor_margin: 15.0}
])
puts "Done"

puts "Creating System Architectures"
SystemArchitecture.create!([
  {architecture: "Fiber and Coax", architecture_assigned: false, default_selected: true},
  {architecture: "Coax", architecture_assigned: false, default_selected: false},
  {architecture: "Auto Select", architecture_assigned: true, default_selected: false}
])
puts "Done"

puts "Creating Have Floor Plans"
HaveFloorPlan.create!([
  {operator: "No", operand: "Enter Estimated Total Building Area on checkmark selection or Enter Estimated Area of Each Floor, in \"Area Data Entry\" below. Estimated Area may possibly be verified online using the Planimeter Tool under \"Search Options\""},
  {operator: "Yes", operand: "Enter True Total Building Area on checkmark selection or Enter True Area of Each Floor in \"Area Data Entry\" below.True Area can be verified from actual floorplans accessible on your hard drive via \"Search Options\""}
])
puts "Done"

puts "Creating Tunnel Have Floor Plans"
TunnelHaveFloorPlan.create!([
  {operator: "No", operand: "Enter Estimated Data for each Tunnel Segment"},
  {operator: "Yes", operand: "Enter True Data for each Tunnel Segment"}
])
puts "Done"

puts "Creating Tunnel Cable C Losses"
TunnelCableCLoss.create!([
  {highest_frequency_band_id: 1, cable_loss_1_by_2: 0.00159, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 2, cable_loss_1_by_2: 0.00225, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 3, cable_loss_1_by_2: 0.00319, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 4, cable_loss_1_by_2: 0.00391, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 5, cable_loss_1_by_2: 0.00507, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 6, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 7, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 8, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 9, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 10, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 11, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 12, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 13, cable_loss_1_by_2: 66.0, cable_loss_7_by_8: 66.0, cable_loss_11_by_4: 66.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 14, cable_loss_1_by_2: 66.0, cable_loss_7_by_8: 66.0, cable_loss_11_by_4: 66.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 15, cable_loss_1_by_2: 66.0, cable_loss_7_by_8: 66.0, cable_loss_11_by_4: 66.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 16, cable_loss_1_by_2: 66.0, cable_loss_7_by_8: 66.0, cable_loss_11_by_4: 66.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 17, cable_loss_1_by_2: 66.0, cable_loss_7_by_8: 66.0, cable_loss_11_by_4: 66.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 18, cable_loss_1_by_2: 67.0, cable_loss_7_by_8: 67.0, cable_loss_11_by_4: 67.0, cable_loss_15_by_8: 70.0, default_selected: nil},
  {highest_frequency_band_id: 19, cable_loss_1_by_2: 67.0, cable_loss_7_by_8: 67.0, cable_loss_11_by_4: 67.0, cable_loss_15_by_8: 70.0, default_selected: nil},
  {highest_frequency_band_id: 20, cable_loss_1_by_2: 67.0, cable_loss_7_by_8: 67.0, cable_loss_11_by_4: 67.0, cable_loss_15_by_8: 70.0, default_selected: nil},
  {highest_frequency_band_id: 21, cable_loss_1_by_2: 67.0, cable_loss_7_by_8: 67.0, cable_loss_11_by_4: 67.0, cable_loss_15_by_8: 70.0, default_selected: nil},
  {highest_frequency_band_id: 22, cable_loss_1_by_2: 67.0, cable_loss_7_by_8: 67.0, cable_loss_11_by_4: 67.0, cable_loss_15_by_8: 70.0, default_selected: nil},
  {highest_frequency_band_id: 23, cable_loss_1_by_2: 67.0, cable_loss_7_by_8: 67.0, cable_loss_11_by_4: 67.0, cable_loss_15_by_8: 70.0, default_selected: nil},
  {highest_frequency_band_id: 24, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 25, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 26, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 27, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 28, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 66.0, default_selected: nil},
  {highest_frequency_band_id: 29, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 66.0, default_selected: nil},
  {highest_frequency_band_id: 30, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 71.0, default_selected: nil},
  {highest_frequency_band_id: 31, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 71.0, default_selected: nil},
  {highest_frequency_band_id: 32, cable_loss_1_by_2: 0.0452, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 33, cable_loss_1_by_2: 0.0481, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 34, cable_loss_1_by_2: 0.0545, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 35, cable_loss_1_by_2: 0.0658, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 36, cable_loss_1_by_2: 0.0628, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 37, cable_loss_1_by_2: 0.0654, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 38, cable_loss_1_by_2: 0.061, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 39, cable_loss_1_by_2: 0.0609, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil},
  {highest_frequency_band_id: 40, cable_loss_1_by_2: 0.0638, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil, default_selected: nil}
])
puts "Done"

puts "Creating Product Frequency Channels"
ProductFrequencyChannel.create!([
  {number_of_channels: 1, composite_power: 30.0, channel_power: 27.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 2, composite_power: 30.0, channel_power: 24.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 3, composite_power: 30.0, channel_power: 22.2, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 4, composite_power: 30.0, channel_power: 21.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 5, composite_power: 30.0, channel_power: 20.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 6, composite_power: 30.0, channel_power: 19.2, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 7, composite_power: 30.0, channel_power: 18.5, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 8, composite_power: 30.0, channel_power: 18.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 9, composite_power: 30.0, channel_power: 17.5, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 10, composite_power: 30.0, channel_power: 17.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 11, composite_power: 30.0, channel_power: 16.6, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 12, composite_power: 30.0, channel_power: 16.2, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 13, composite_power: 30.0, channel_power: 15.9, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 14, composite_power: 30.0, channel_power: 15.5, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 15, composite_power: 30.0, channel_power: 15.2, papr: 3.0, bda_product_category_id: 1, default_selected: true},
  {number_of_channels: 16, composite_power: 30.0, channel_power: 15.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 17, composite_power: 30.0, channel_power: 14.7, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 18, composite_power: 30.0, channel_power: 14.4, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 19, composite_power: 30.0, channel_power: 14.2, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 20, composite_power: 30.0, channel_power: 14.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 21, composite_power: 30.0, channel_power: 13.8, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 22, composite_power: 30.0, channel_power: 13.6, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 23, composite_power: 30.0, channel_power: 13.4, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 24, composite_power: 30.0, channel_power: 13.2, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 25, composite_power: 30.0, channel_power: 13.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 26, composite_power: 30.0, channel_power: 12.9, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 27, composite_power: 30.0, channel_power: 12.7, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 28, composite_power: 30.0, channel_power: 12.5, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 29, composite_power: 30.0, channel_power: 12.4, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 30, composite_power: 30.0, channel_power: 12.2, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 31, composite_power: 30.0, channel_power: 12.1, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 32, composite_power: 34.0, channel_power: 15.9, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 33, composite_power: 34.0, channel_power: 15.8, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 34, composite_power: 34.0, channel_power: 15.7, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 35, composite_power: 34.0, channel_power: 15.6, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 36, composite_power: 34.0, channel_power: 15.4, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 37, composite_power: 34.0, channel_power: 15.3, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 38, composite_power: 34.0, channel_power: 15.2, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 39, composite_power: 34.0, channel_power: 15.1, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 40, composite_power: 34.0, channel_power: 15.0, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 41, composite_power: 34.0, channel_power: 14.9, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 42, composite_power: 34.0, channel_power: 14.8, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 43, composite_power: 34.0, channel_power: 14.7, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 44, composite_power: 34.0, channel_power: 14.6, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 45, composite_power: 34.0, channel_power: 14.5, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 46, composite_power: 34.0, channel_power: 14.4, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 47, composite_power: 34.0, channel_power: 14.3, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 48, composite_power: 34.0, channel_power: 14.2, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 49, composite_power: 34.0, channel_power: 14.1, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 50, composite_power: 34.0, channel_power: 14.0, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 51, composite_power: 34.0, channel_power: 13.9, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 52, composite_power: 34.0, channel_power: 13.8, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 53, composite_power: 34.0, channel_power: 13.8, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 54, composite_power: 34.0, channel_power: 13.7, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 55, composite_power: 34.0, channel_power: 13.6, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 56, composite_power: 34.0, channel_power: 13.5, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 57, composite_power: 34.0, channel_power: 13.4, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 58, composite_power: 34.0, channel_power: 13.4, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 59, composite_power: 34.0, channel_power: 13.3, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 60, composite_power: 34.0, channel_power: 13.2, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 61, composite_power: 34.0, channel_power: 13.1, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 62, composite_power: 34.0, channel_power: 13.1, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 63, composite_power: 34.0, channel_power: 13.0, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 64, composite_power: 34.0, channel_power: 12.9, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 65, composite_power: 34.0, channel_power: 12.9, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 66, composite_power: 34.0, channel_power: 12.8, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 67, composite_power: 34.0, channel_power: 12.7, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 68, composite_power: 34.0, channel_power: 12.7, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 69, composite_power: 34.0, channel_power: 12.6, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 70, composite_power: 34.0, channel_power: 12.5, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 71, composite_power: 34.0, channel_power: 12.5, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 72, composite_power: 34.0, channel_power: 12.4, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 73, composite_power: 34.0, channel_power: 12.4, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 74, composite_power: 34.0, channel_power: 12.3, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 75, composite_power: 34.0, channel_power: 12.2, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 76, composite_power: 34.0, channel_power: 12.2, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 77, composite_power: 34.0, channel_power: 12.1, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 78, composite_power: 34.0, channel_power: 12.1, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 79, composite_power: 37.0, channel_power: 15.0, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 80, composite_power: 37.0, channel_power: 15.0, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 81, composite_power: 37.0, channel_power: 14.9, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 82, composite_power: 37.0, channel_power: 14.9, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 83, composite_power: 37.0, channel_power: 14.8, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 84, composite_power: 37.0, channel_power: 14.8, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 85, composite_power: 37.0, channel_power: 14.7, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 86, composite_power: 37.0, channel_power: 14.7, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 87, composite_power: 37.0, channel_power: 14.6, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 88, composite_power: 37.0, channel_power: 14.6, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 89, composite_power: 37.0, channel_power: 14.5, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 90, composite_power: 37.0, channel_power: 14.5, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 91, composite_power: 37.0, channel_power: 14.4, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 92, composite_power: 37.0, channel_power: 14.4, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 93, composite_power: 37.0, channel_power: 14.3, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 94, composite_power: 37.0, channel_power: 14.3, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 95, composite_power: 37.0, channel_power: 14.2, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 96, composite_power: 37.0, channel_power: 14.2, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 97, composite_power: 37.0, channel_power: 14.1, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 98, composite_power: 37.0, channel_power: 14.1, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 99, composite_power: 37.0, channel_power: 14.0, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 100, composite_power: 37.0, channel_power: 14.0, papr: 3.0, bda_product_category_id: 3, default_selected: false}
])
puts "Done"

puts "Creating Tunnel Product Price Lists"
TunnelProductPriceList.create!([
  {tunnel_report_material_list_id: 1, vendor: "vendor1", manufacturer: "manufacturer1", mfg_pn: "mfg_pn1", our_pn: "our_pn1", description: nil, mfg_list_price: 124.0, our_cost: 125.0, discount_off_list: 1.0, our_sell_price: 126.0},
  {tunnel_report_material_list_id: 2, vendor: "vendor13", manufacturer: "manufacturer13", mfg_pn: "mfg_pn13", our_pn: "our_pn13", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 3.0, our_sell_price: 128.0},
  {tunnel_report_material_list_id: 3, vendor: "vendor14", manufacturer: "manufacturer14", mfg_pn: "mfg_pn14", our_pn: "our_pn14", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 2.0, our_sell_price: 129.0},
  {tunnel_report_material_list_id: 4, vendor: "vendor15", manufacturer: "manufacturer15", mfg_pn: "mfg_pn15", our_pn: "our_pn15", description: nil, mfg_list_price: 122.0, our_cost: 123.0, discount_off_list: 1.0, our_sell_price: 124.0},
  {tunnel_report_material_list_id: 5, vendor: "vendor16", manufacturer: "manufacturer16", mfg_pn: "mfg_pn16", our_pn: "our_pn16", description: nil, mfg_list_price: 123.0, our_cost: 124.0, discount_off_list: 2.0, our_sell_price: 125.0},
  {tunnel_report_material_list_id: 6, vendor: "vendor17", manufacturer: "manufacturer17", mfg_pn: "mfg_pn17", our_pn: "our_pn17", description: nil, mfg_list_price: 124.0, our_cost: 125.0, discount_off_list: 2.0, our_sell_price: 126.0},
  {tunnel_report_material_list_id: 7, vendor: "vendor18", manufacturer: "manufacturer18", mfg_pn: "mfg_pn18", our_pn: "our_pn18", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 3.0, our_sell_price: 128.0},
  {tunnel_report_material_list_id: 8, vendor: "vendor19", manufacturer: "manufacturer19", mfg_pn: "mfg_pn19", our_pn: "our_pn19", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 2.0, our_sell_price: 128.0},
  {tunnel_report_material_list_id: 9, vendor: "vendor20", manufacturer: "manufacturer20", mfg_pn: "mfg_pn20", our_pn: "our_pn20", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 2.0, our_sell_price: 129.0},
  {tunnel_report_material_list_id: 10, vendor: "vendor21", manufacturer: "manufacturer21", mfg_pn: "mfg_pn21", our_pn: "our_pn21", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 2.0, our_sell_price: 127.0},
  {tunnel_report_material_list_id: 11, vendor: "vendor22", manufacturer: "manufacturer22", mfg_pn: "mfg_pn22", our_pn: "our_pn22", description: nil, mfg_list_price: 128.0, our_cost: 129.0, discount_off_list: 2.0, our_sell_price: 130.0},
  {tunnel_report_material_list_id: 12, vendor: "vendor23", manufacturer: "manufacturer23", mfg_pn: "mfg_pn23", our_pn: "our_pn23", description: nil, mfg_list_price: 124.0, our_cost: 125.0, discount_off_list: 2.0, our_sell_price: 126.0},
  {tunnel_report_material_list_id: 13, vendor: "vendor24", manufacturer: "manufacturer24", mfg_pn: "mfg_pn24", our_pn: "our_pn24", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 3.0, our_sell_price: 127.0},
  {tunnel_report_material_list_id: 14, vendor: "vendor25", manufacturer: "manufacturer25", mfg_pn: "mfg_pn25", our_pn: "our_pn25", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 1.0, our_sell_price: 128.0},
  {tunnel_report_material_list_id: 15, vendor: "vendor26", manufacturer: "manufacturer26", mfg_pn: "mfg_pn26", our_pn: "our_pn26", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 2.0, our_sell_price: 129.0},
  {tunnel_report_material_list_id: 16, vendor: "vendor27", manufacturer: "manufacturer27", mfg_pn: "mfg_pn27", our_pn: "our_pn27", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 1.0, our_sell_price: 127.0},
  {tunnel_report_material_list_id: 17, vendor: "vendor28", manufacturer: "manufacturer28", mfg_pn: "mfg_pn28", our_pn: "our_pn28", description: nil, mfg_list_price: 122.0, our_cost: 123.0, discount_off_list: 5.0, our_sell_price: 124.0},
  {tunnel_report_material_list_id: 18, vendor: "vendor29", manufacturer: "manufacturer29", mfg_pn: "mfg_pn29", our_pn: "our_pn29", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 1.0, our_sell_price: 128.0},
  {tunnel_report_material_list_id: 19, vendor: "vendor30", manufacturer: "manufacturer30", mfg_pn: "mfg_pn30", our_pn: "our_pn30", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 2.0, our_sell_price: 129.0},
  {tunnel_report_material_list_id: 20, vendor: "vendor31", manufacturer: "manufacturer31", mfg_pn: "mfg_pn31", our_pn: "our_pn31", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 1.0, our_sell_price: 127.0},
  {tunnel_report_material_list_id: 21, vendor: "vendor32", manufacturer: "manufacturer32", mfg_pn: "mfg_pn32", our_pn: "our_pn32", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 1.0, our_sell_price: 129.0},
  {tunnel_report_material_list_id: 22, vendor: "vendor33", manufacturer: "manufacturer33", mfg_pn: "mfg_pn33", our_pn: "our_pn33", description: nil, mfg_list_price: 121.0, our_cost: 122.0, discount_off_list: 1.0, our_sell_price: 123.0},
  {tunnel_report_material_list_id: 23, vendor: "vendor34", manufacturer: "manufacturer34", mfg_pn: "mfg_pn34", our_pn: "our_pn34", description: nil, mfg_list_price: 120.0, our_cost: 121.0, discount_off_list: 1.0, our_sell_price: 122.0},
  {tunnel_report_material_list_id: 24, vendor: "vendor35", manufacturer: "manufacturer35", mfg_pn: "mfg_pn35", our_pn: "our_pn35", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 2.0, our_sell_price: 127.0},
  {tunnel_report_material_list_id: 25, vendor: "vendor36", manufacturer: "manufacturer36", mfg_pn: "mfg_pn36", our_pn: "our_pn36", description: nil, mfg_list_price: 123.0, our_cost: 124.0, discount_off_list: 1.0, our_sell_price: 125.0},
  {tunnel_report_material_list_id: 26, vendor: "vendor37", manufacturer: "manufacturer37", mfg_pn: "mfg_pn37", our_pn: "our_pn37", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 2.0, our_sell_price: 129.0},
  {tunnel_report_material_list_id: 27, vendor: "vendor38", manufacturer: "manufacturer38", mfg_pn: "mfg_pn38", our_pn: "our_pn38", description: nil, mfg_list_price: 123.0, our_cost: 124.0, discount_off_list: 1.0, our_sell_price: 125.0},
  {tunnel_report_material_list_id: 28, vendor: "vendor39", manufacturer: "manufacturer39", mfg_pn: "mfg_pn39", our_pn: "our_pn39", description: nil, mfg_list_price: 122.0, our_cost: 123.0, discount_off_list: 1.0, our_sell_price: 124.0},
  {tunnel_report_material_list_id: 29, vendor: "vendor40", manufacturer: "manufacturer40", mfg_pn: "mfg_pn40", our_pn: "our_pn40", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 1.0, our_sell_price: 128.0},
  {tunnel_report_material_list_id: 30, vendor: "vendor41", manufacturer: "manufacturer41", mfg_pn: "mfg_pn41", our_pn: "our_pn41", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 1.0, our_sell_price: 129.0},
  {tunnel_report_material_list_id: 31, vendor: "vendor42", manufacturer: "manufacturer42", mfg_pn: "mfg_pn42", our_pn: "our_pn42", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 1.0, our_sell_price: 127.0},
  {tunnel_report_material_list_id: 32, vendor: "vendor43", manufacturer: "manufacturer43", mfg_pn: "mfg_pn43", our_pn: "our_pn43", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 1.0, our_sell_price: 127.0},
  {tunnel_report_material_list_id: 33, vendor: "vendor44", manufacturer: "manufacturer44", mfg_pn: "mfg_pn44", our_pn: "our_pn44", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 1.0, our_sell_price: 128.0},
  {tunnel_report_material_list_id: 34, vendor: "vendor45", manufacturer: "manufacturer45", mfg_pn: "mfg_pn45", our_pn: "our_pn45", description: nil, mfg_list_price: 123.0, our_cost: 124.0, discount_off_list: 1.0, our_sell_price: 125.0},
  {tunnel_report_material_list_id: 35, vendor: "vendor46", manufacturer: "manufacturer46", mfg_pn: "mfg_pn46", our_pn: "our_pn46", description: nil, mfg_list_price: 123.0, our_cost: 124.0, discount_off_list: 1.0, our_sell_price: 125.0},
  {tunnel_report_material_list_id: 36, vendor: "vendor47", manufacturer: "manufacturer47", mfg_pn: "mfg_pn47", our_pn: "our_pn47", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 2.0, our_sell_price: 128.0},
  {tunnel_report_material_list_id: 37, vendor: "vendor48", manufacturer: "manufacturer48", mfg_pn: "mfg_pn48", our_pn: "our_pn48", description: nil, mfg_list_price: 128.0, our_cost: 129.0, discount_off_list: 3.0, our_sell_price: 130.0}
])
puts "Done"

puts "Creating Tunnel Coverage Distance Per BDAs"
TunnelCoverageDistancePerBda.create!([
  {bda_product_category_id: 1, coverage_distance: 2640.0},
  {bda_product_category_id: 2, coverage_distance: 5280.0},
  {bda_product_category_id: 3, coverage_distance: 10560.0}
])
puts "Done"

puts "Creating System Feed Methods"
SystemFeedMethod.create!([
  {feed_method: "Direct Feed", feed_assigned: false, default_selected: false},
  {feed_method: "Off-Air", feed_assigned: false, default_selected: true},
  {feed_method: "Auto Select", feed_assigned: true, default_selected: false}
])
puts "Done"

puts "Creating Tunnel Fiber Material Quantities"
TunnelFiberMaterialQuantity.create!([
  {tunnel_report_material_list_id: 8, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 9, description: nil, quantity: 0.0},
  {tunnel_report_material_list_id: 10, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 11, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 12, description: nil, quantity: 1.0}
])
puts "Done"

puts "Creating Tunnel Donor Direct Feed Quantities"
TunnelDonorDirectFeedQuantity.create!([
  {tunnel_report_material_list_id: 5, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 6, description: nil, quantity: 0.0},
  {tunnel_report_material_list_id: 7, description: nil, quantity: 0.0},
  {tunnel_report_material_list_id: 13, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 14, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 15, description: nil, quantity: 250.0},
  {tunnel_report_material_list_id: 16, description: nil, quantity: 2.0},
  {tunnel_report_material_list_id: 17, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 18, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 19, description: nil, quantity: 1.0}
])
puts "Done"

puts "Creating Building Environments"
BuildingEnvironment.create!([
  {environment: "Wide Open Facility (Airport, Stadium etc.)", building_indoor_factor: 2.2},
  {environment: "Medium Cluttered Facility", building_indoor_factor: 2.5},
  {environment: "Densely Cluttered Facility", building_indoor_factor: 3.5},
  {environment: "Free Space - No Clutter (ideal)", building_indoor_factor: 2.0},
  {environment: "Tunnel", building_indoor_factor: 2.0},
  {environment: "Mail / Food Court / Covention Center", building_indoor_factor: 2.2},
  {environment: "Retail Outlet / Big Box Store", building_indoor_factor: 2.2},
  {environment: "Warehouse (no machines or shelving)", building_indoor_factor: 2.3},
  {environment: "Office with low partitions", building_indoor_factor: 2.5},
  {environment: "Office with high partitions", building_indoor_factor: 2.6},
  {environment: "Office with mixed material partitions", building_indoor_factor: 2.8},
  {environment: "Office with more rooms, less partitions", building_indoor_factor: 3.0},
  {environment: "Warehouse with metal shelving", building_indoor_factor: 3.0},
  {environment: "Hospital", building_indoor_factor: 3.0},
  {environment: "University Campus", building_indoor_factor: 3.0},
  {environment: "Factory with large metal machinery", building_indoor_factor: 3.5},
  {environment: "Correctional Facility (prison)", building_indoor_factor: 4.0},
  {environment: "Other Building (stone or concrete)", building_indoor_factor: 4.0}
])
puts "Done"

puts "Creating Coverage Area Per BDAs"
CoverageAreaPerBda.create!([
  {bda_product_category_id: 1, coverage_area: 100000.0},
  {bda_product_category_id: 2, coverage_area: 180000.0},
  {bda_product_category_id: 3, coverage_area: 250000.0}
])
puts "Done"

puts "Creating Static Page"
StaticPage.create!([
  {contact_us: "<div class=\"contactMap\">\r\n  <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d55613461.81318646!2d-146.61479896957405!3d31.70561343684153!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x52b30b71698e729d%3A0x131328839761a382!2sNorth+America!5e0!3m2!1sen!2sin!4v1447843417347\" width=\"100%\" height=\"350\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\r\n</div>\r\n<div class=\"addressCon\">\r\n  <h1> We are based in North America</h1>\r\n  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec lectus gravida, fringilla nunc id, cursus orci. Nulla efficitur ut tortor ac dapibus. Phasellus dignissim elementum diam eget tempor.</p>\r\n  <ul>\r\n    <li>\r\n      <img src=\"/assets/address.png\">\r\n      <p>Consulting Lorem ipsum dolor sit amet.</p>\r\n    </li>\r\n    <li>\r\n      <img src=\"/assets/phone.png\" alt=\"\">\r\n      <p>(909) 949-9083</p>\r\n    </li>\r\n    <li>\r\n      <img src=\"/assets/email.png\" alt=\"\">\r\n      <p>info@example.com</p>\r\n    </li>\r\n  </ul>\r\n</div>\r\n<div class=\"formbox\">\r\n  <div class=\"formOuter\">\r\n    <div class=\"blackHD\">reach out to us today</div>\r\n    <div class=\"row\">\r\n      <form method=\"post\" data-remote=\"true\" accept-charset=\"UTF-8\" action=\"/contact_us\" id=\"new_contact\" class=\"new_contact\" novalidate=\"novalidate\">\r\n      <div class=\"col-sm-12\" id=\"message\">\r\n        <div class=\"alert alert-info fade in margin-bottom-10\">\r\n          <a data-dismiss=\"alert\" class=\"close position-inherit\">x</a>\r\n          <div id=\"return_message\"></div>\r\n        </div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"col-xs-6 prz\">\r\n        <input type=\"text\" id=\"contact_first_name\" name=\"contact[first_name]\" placeholder=\"First name\" class=\"form-control inputText\" required=\"required\" autofocus=\"autofocus\" aria-required=\"true\">\r\n      </div>\r\n      <div class=\"col-xs-6 plz\">\r\n        <input type=\"text\" id=\"contact_last_name\" name=\"contact[last_name]\" placeholder=\"Last name\" class=\"form-control inputText\" required=\"required\" aria-required=\"true\">\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"col-xs-6 prz\">\r\n        <input type=\"email\" id=\"contact_email\" name=\"contact[email]\" placeholder=\"Email Address\" class=\"form-control inputText\" required=\"required\" aria-required=\"true\">\r\n      </div>\r\n      <div class=\"col-xs-6 plz\">\r\n        <input type=\"text\" id=\"contact_contact_number\" name=\"contact[contact_number]\" placeholder=\"Contact Number\" class=\"form-control inputText\" required=\"required\" aria-required=\"true\">\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"col-xs-12\">\r\n        <textarea id=\"contact_message\" name=\"contact[message]\" placeholder=\"Enter Message\" class=\"form-control inputText\" required=\"required\" aria-required=\"true\"></textarea>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"col-xs-12\">\r\n        <input type=\"submit\" class=\"form-control inputText inputSub\" value=\"Submit\" name=\"commit\">\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>", faq: "<!--Start Frequently Asked Questions container-->\r\n<div class=\"innermainCon2 innermainCon3\">\r\n  <div class=\"banrHd\">\r\n    <h1>Frequently Asked Questions</h1>\r\n  </div>\r\n  <div class=\"faqCon\">\r\n    <div class=\"container\">\r\n      <div class=\"heading\">General Questions</div>\r\n      <ul>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry ?</strong>\r\n          <p id=\"1\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem. \r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem et Ipsum is simply dummy text of the printing and typesetting industry text of the printing industry ?</strong>\r\n          <p id=\"2\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem. \r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text ?</strong>\r\n          <p id=\"3\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem. \r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry ?</strong>\r\n          <p id=\"4\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem. \r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text ?</strong>\r\n          <p id=\"5\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem. \r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry ?</strong>\r\n          <p id=\"6\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem. \r\n          </p>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n  <div class=\"faqCon faqCon2\">\r\n    <div class=\"container\">\r\n      <div class=\"heading\">General Questions</div>\r\n      <ul>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry ?</strong>\r\n          <p id=\"7\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.\r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry text of the printing industry ?</strong>\r\n          <p id=\"8\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.\r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text ?</strong>\r\n          <p id=\"9\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.\r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry ?</strong>\r\n          <p id=\"10\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.\r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text ?</strong>\r\n          <p id=\"11\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.\r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry ?</strong>\r\n          <p id=\"12\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.\r\n          </p>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!--End Frequently Asked Questions container-->", privacy_policy: "<div class=\"innermainCon2\">\r\n  <div class=\"banrHd\">\r\n    <h1 align=\"left\">Privacy Policy</h1>\r\n  </div>\r\n  <div class=\"privacyCon\">\r\n    <p align=\"left\">This Privacy Policy has been compiled to better serve those who are concerned with how their 'Personally identifiable information' (PII) is being used online. PII, as used in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our Privacy Policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>\r\n\r\n<h2 align=\"left\">1. What personal information do we collect from visitors to our website?</h2>\r\n    <p align=\"left\">When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address, phone number, Company Name or other details to help you with your experience.</p>\r\n   \r\n<h2 align=\"left\">2. Do we use 'cookies'?</h2>\r\n    <p align=\"left\">Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.\r\n<p align=\"left\">We use cookies to:<br>\r\n■  Help remember and process the items in the shopping cart.<br>\r\n■  Understand and save user's preferences for future visits.<br>\r\n■  Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third party services that track this information on our behalf.\r\n</p>\r\n\r\n    <p align=\"left\">You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser (like Internet Explorer) settings. Each browser is a little different, so look at your browser's Help menu to learn the correct way to modify your cookies.\r\n    <p align=\"left\">If you disable cookies off, some features will be disabled. It won't affect the users experience that make your site experience more efficient and some of our services will not function properly.\r\nHowever, you can still place orders.</p>\r\n\r\n<h2 align=\"left\">3. How we use the Information we gather</h2>\r\n    <p align=\"left\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec lectus gravida, fringilla nunc id, cursus orci. Nulla efficitur ut tortor ac dapibus. Phasellus dignissim elementum diam eget tempor. Donec massa eros, tristique sit amet varius sit amet, placerat vel mi. Curabitur tincidunt, tortor eget feugiat malesuada, elit nibh venenatis lacus, et tristique dolor lacus ut sapien. Morbi pharetra sodales lectus ac scelerisque. Nam sit amet quam in turpis efficitur ullamcorper. Sed tristique lorem lacus, rhoncus cursus nisl tempus vel.</p>\r\n    <p align=\"left\">Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum. </p>\r\n    <p align=\"left\">Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum. </p>\r\n\r\n<h2 align=\"left\">4. Accessing, Reviewing and Changing your Personal Information</h2>\r\n    <p align=\"left\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec lectus gravida, fringilla nunc id, cursus orci. Nulla efficitur ut tortor ac dapibus. Phasellus dignissim elementum diam eget tempor. Donec massa eros, tristique sit amet varius sit amet, placerat vel mi. Curabitur tincidunt, tortor eget feugiat malesuada, elit nibh venenatis lacus, et tristique dolor lacus ut sapien. Morbi pharetra sodales lectus ac scelerisque. Nam sit amet quam in turpis efficitur ullamcorper. Sed tristique lorem lacus, rhoncus cursus nisl tempus vel.</p>\r\n    <p align=\"left\">Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum. </p>\r\n    <p align=\"left\">Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum. </p>\r\n  </div>\r\n</div>", terms_and_conditions: "<div class=\"innermainCon2\">\r\n  <div class=\"banrHd\">\r\n    <h1 align=\"left\">Terms and Conditions</h1>\r\n    <strong>The following is a listing of the terms and conditions that govern the use of this service and this website.</strong>\r\n  </div>\r\n  <div class=\"privacyCon\">\r\n\r\n<h2 align=\"left\">1. Terms of Use</h2>\r\n<p align=\"left\">By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trade mark law.</p>\r\n\r\n<h3 align=\"left\">2. Intellectual Property Rights<h3>\r\n<p align=\"left\">All copyrights, trademarks, patents and other intellectual property rights in and on our website and all content and software located on the site shall remain the sole property of our company or its licensors. The use of our trademarks, content and intellectual property is forbidden without the express written consent from the owner of this website.</p>\r\n<p style=\"text-align:left\">You must not:<br>\r\n■ Republish material from our website without prior written consent<br>\r\n■ Sell or rent material from our website<br>\r\n■ Reproduce, duplicate, create derivative, copy or otherwise exploit material on our website for any purpose<br>\r\n■ Redistribute any content from our website, including onto another website\r\n</p>\r\n\r\n<h3 align=\"left\">3. Acceptable Use<h3>\r\n<p align=\"left\">You agree to use our website only for lawful purposes, and in a way that does not infringe the rights of, restrict or inhibit anyone else’s use and enjoyment of the website. Prohibited behavior includes harassing or causing distress or inconvenience to any other user, transmitting obscene or offensive content or disrupting the normal flow of dialogue within our website.You must not use our website to send unsolicited commercial communications.You must not use the content on our website for any marketing related purpose without our express written consent.</p>\r\n\r\n<h3 align=\"left\">4. Restricted Access<h3>\r\n<p align=\"left\">We may in the future need to restrict access to parts (or all) of our website and reserve full rights to do so. If, at any point, we provide you with a username and password for you to access restricted areas of our website, you must ensure that both your username and password are kept confidential.</p>\r\n\r\n<h3 align=\"left\">5. Revisions<h3>\r\n<p align=\"left\">The owner of this website may change these terms from time to time and so you should check these terms regularly. Your continued use of our website will be deemed acceptance of the updated or amended terms. If you do not agree to the changes, you should cease using our website immediately. If any of these terms are determined to be illegal, invalid or otherwise unenforceable, it shall be severed and deleted from these terms and the remaining terms shall survive and continue to be binding and enforceable.</p>\r\n\r\n<h3 align=\"left\">6. Limitation of Liability<h3>\r\n<p align=\"left\">THE MATERIALS AT THIS SITE ARE PROVIDED “AS IS” WITHOUT ANY EXPRESS OR IMPLIED WARRANTY OF ANY KIND INCLUDING WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT OF INTELLECTUAL PROPERTY, OR FITNESS FOR ANY PARTICULAR PURPOSE. IN NO EVENT SHALL THE OWNER OF THIS WEBSITE OR ITS AGENTS OR OFFICERS BE LIABLE FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS, BUSINESS INTERRUPTION, LOSS OF INFORMATION, INJURY OR DEATH) ARISING OUT OF THE USE OF OR INABILITY TO USE THE MATERIALS, EVEN IF THE OWNER OF THIS WEBSITE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS OR DAMAGES.</p>\r\n \r\n<h2 align=\"left\">7. Pricing and Marketing Terms and Conditions</h2>\r\n     <p align=\"left\">The pricing provided for quoted projects by XXXXX Inc in the output report from this service represents a best average market price as offered by preferred vendors, OEMs and equipment manufacturers. XXXXX Inc. reserves the right to further offer, promote and/or make suggestions to the subscriber to opt-into participating in external granular, accurate and competitive pricing offers from preferred vendors, OEMs and equipment manufacturers for the subscriber’s quoted projects, at the subscriber’s discretion. The subscriber acknowledges and agrees to these terms and conditions of pricing by registering an account with XXXXX Inc.</p>\r\n\r\n    <h2 align=\"left\">8. Registering with You</h2>\r\n    <p align=\"left\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec lectus gravida, fringilla nunc id, cursus orci. Nulla efficitur ut tortor ac dapibus. Phasellus dignissim elementum diam eget tempor. Donec massa eros, tristique sit amet varius sit amet, placerat vel mi. Curabitur tincidunt, tortor eget feugiat malesuada, elit nibh venenatis lacus, et tristique dolor lacus ut sapien. Morbi pharetra sodales lectus ac scelerisque. Nam sit amet quam in turpis efficitur ullamcorper. Sed tristique lorem lacus, rhoncus cursus nisl tempus vel.</p>\r\n\r\n\r\n    <h2 align=\"left\">9. Services Not Intended For Children</h2>\r\n    <p align=\"left\">This website does not provide services or sell products to children under the age of 18.If we discover we have received any information from a child under the age of 18 in violation of this policy, we will delete that information immediately. If you believe we have received any information from or about anyone under the age of 18, please contact us at the address listed below.</p>\r\n\r\n  </div>\r\n</div>", about_us: "<!--Start About us-->\r\n<div class=\"aboutCon\">\r\n  <div class=\"aboutUsCon\">\r\n    <div class=\"innerContainer\">\r\n      <h1>About Us</h1>\r\n      <p>In line with our philosophy of seeking to dramatically improve the efficiency of fundamental processes involved in the typical DAS-based business, we have developed a unique, time-tested, Rapid System Design Assessment and Quote (RSDAQ™) method, practically realized as a scaleable software platform known as our RSDAQ™ Engine. This web-based software enables rapid delivery of a rough order-of-magnitude (ROM) estimate of the Bill of Materials (BOM) for a Distributed Antenna System (DAS) used for wireless signal coverage extension within Buildings or Tunnels. </p>\r\n      <p>Any of our subscribers can now gain the incredible benefits of lightning-fast DAS Equipment Cost Estimates and BOMs generated simultaneously for up to 10 Buildings (and more) in less than 15 minutes - in 3 easy steps! </p>\r\n      <br>\r\n      <p><strong>RSDAQ™ Engine Version 1.0.0.5      Patent Pending </strong></p>\r\n      <br>\r\n      <br>\r\n      <p>This software is protected by copyright laws and international treaties </p>\r\n      <p>RSDAQ is a trademark of XXXXX Inc. </p>\r\n      <p>Copyright © 2015. All Rights Reserved. </p>\r\n      <p>This software is made available subject to the terms of the licence information set forth in the Terms and Conditions of use. This Product was created by XXXX Inc. All internal contributors and developers are acknowledged. </p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!--End About us-->", help: nil, quick_start: "<div class=\"grayOuter\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <h1 class=\"clearfix tac boldHD\"> Quick-Start Guide</h1>\r\n      <h2 class=\"grayMainGD\">On this page you will find all the necessary information to enable you to get your projects started <br>\r\n        and completed in the shortest time possible. \r\n      </h2>\r\n      <p>So, ignore the temptation to skip this section- you’ll be glad you did- and you’ll benefit immensely in the long run</p>\r\n      <iframe  src=\"https://www.youtube.com/embed/6v2L2UGZJAM\" allowfullscreen class=\"videoFrame\" ></iframe>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"Disclaimer whiteBkg\">\r\n  <div class=\"container\">\r\n    <div class=\"max960width\">\r\n      <div class=\"clearfix tac boldHD\"> How it Works</div>\r\n      <p>The software produces and delivers a rough order of magnitude (ROM) estimate of the Bill of Materials (BOM) for wireless signal coverage extension systems within Buildings or Tunnels in 3 easy steps.</p>\r\n      <div class=\"row mt40\">\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"box box1\">\r\n            <span>\r\n              <div class=\"imageIcon\"> <img src=\"/assets/1a.png\" alt=\"\"> <img src=\"/assets/1.png\" class=\"numbers\" alt=\"\"/> </div>\r\n            </span>\r\n            <div class=\"bold\">Step 1</div>\r\n            <div>Input building or tunnel data.</div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"box box2\">\r\n            <span>\r\n              <div class=\"imageIcon\"> <img src=\"/assets/2b.png\" alt=\"\"> <img src=\"/assets/2.png\" class=\"numbers\" alt=\"\"> </div>\r\n            </span>\r\n            <div class=\"bold\">Step 2</div>\r\n            <div>Input system and target performance data as required.</div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"box box3\">\r\n            <span>\r\n              <div class=\"imageIcon\"> <img src=\"/assets/3c.png\" alt=\"\"> <img src=\"/assets/3.png\" class=\"numbers\" alt=\"\"></div>\r\n            </span>\r\n            <div class=\"bold\">Step 3</div>\r\n            <div>Acknowledge the disclaimer and hit the \"Next\" button to get your report!</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>", quick_start_step1: "<div class=\"grayOuter\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <div class=\"stepOuter\">\r\n        <span>\r\n        <img src=\"/assets/s1g.png\" alt=\"Step 1\">\r\n        <img src=\"/assets/s2Gray.png\" class=\"middleImg\" alt=\"Step 2\">\r\n        <img src=\"/assets/s3Gray.png\" alt=\"Step 3\">\r\n        </span>\r\n      </div>\r\n      <h1 class=\"grayMainGD\">This is Step 1 - the first of three steps toward completion of your project. <br>Let's get started!\r\n      </h1>\r\n      <p>In this step we enter the Project Details and Building Data (including Area covered), Frequency Band of Operation, Number of Services and Channels. Make sure you have the information at hand before proceeding to the next page.</p> \r\n      <p>Once you are ready, click the <strong>NEXT</strong> button to continue.</p>\r\n    </div>\r\n  </div>\r\n</div>", quick_start_step2: "<div class=\"grayOuter\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <div class=\"stepOuter\">\r\n        <span>\r\n        <img src=\"/assets/s1g.png\" alt=\"Step 1\">\r\n        <img src=\"/assets/s2GrnL.png\" class=\"middleImg\" alt=\"Step 2\">\r\n        <img src=\"/assets/s3Gray.png\" alt=\"Step 3\">\r\n        </span>\r\n      </div>\r\n      <h1 class=\"grayMainGD\">This is Step 2 - the second of three steps toward completion of your project. <br>\r\n      </h1>\r\n      <p>In this step we enter the System Feed Method (Off-Air, or Direct-Feed) and Architecture (Fiber or Coaxial Cable). <br>After your selections are made, click the <strong>NEXT</strong> button to continue.</p>\r\n    </div>\r\n  </div>\r\n</div>", quick_start_step3: "<div class=\"grayOuter\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <div class=\"stepOuter\">\r\n        <span>\r\n        <img alt=\"Step 1\" src=\"/assets/s1g.png\">\r\n        <img alt=\"Step 2\" class=\"middleImg\" src=\"/assets/s2GrnL.png\">\r\n        <img alt=\"Step 3\" src=\"/assets/s3orange.png\">\r\n        </span>\r\n      </div>\r\n      <h1 class=\"grayMainGD\">This is Step 3 - the third and final step toward completion of your project. Please acknowledge the Disclaimer below and click the FINISH button to proceed with generating your report.\r\n      </h1>\r\n      <p>This is a Material-only ROM/Budgetary Price Quotation. This quote represents our best assessment of the ROM material categories and quantities required to develop the DAS solution per minimum viable inputs (MVI) stated by the user. This quotes but may be subject to variations due to factors beyond our control. The accuracy of this ROM quote is limited to the confines of the accuracy and industry best practices relating to standards for Budget Estimates of -10% to +25%.</p>\r\n    </div>\r\n  </div>\r\n</div>", quick_start_download: "<div class=\"grayOuter\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <div class=\"clearfix tac boldHD\"> Here is your report !</div>\r\n      <h1 class=\"grayMainGD\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is  <br>\r\n        simply dummy text of the printing and typesetting industry.\r\n      </h1>\r\n      <p>This service is provided as-is, and is to be used at the subscriber’s discretion. The subscriber assumes all risk for using this service. XXXXX Inc. provides no guarantees or warranty of any kind, expressed or implied, with respect to any goods, parts  and service provided by XXXXX Inc. Including, but not limited to, the implied guarantees, warranties or merchantability and fitness for a particular purpose. XXXXX Inc. shall not in any event be liable for any damages including, but not limited to, any indirect, special or consequential damages arising out of or in connection with furnishing of goods, parts or service, or the performance, use of, or inability to use any goods, parts or service, or otherwise, whether based on contract, tort or any other legal theory.  \r\nIn the course of using the output report from this service to respond to or satisfy the conditions of any RFP or other situations relating to the output report from this service, the subscriber agrees to indemnify and hold harmless XXXXX Inc., its officers, agents and employees from and against any liabilities, damages, costs, expenses, and attorneys' fees for any claim of loss, damage, cost, or injury  of whatsoever kind or nature, including bodily injury of death, relating to the use of this service.</p>\r\n\r\n <p>The pricing provided for quoted projects by XXXXX Inc in the output report from this service represents a best average market price as offered by preferred vendors, OEMs and equipment manufacturers. XXXXX Inc. reserves the right to further offer, promote and/or make suggestions to the subscriber to opt-into participating in external granular, accurate and competitive pricing offers from preferred vendors, OEMs and equipment manufacturers for the subscriber’s quoted projects, at the subscriber’s discretion. The subscriber acknowledges and agrees to these terms and conditions of pricing by registering an account with XXXXX Inc.</p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"Disclaimer whiteBkg\">\r\n  <div class=\"container\">\r\n    <div class=\"max960width\">\r\n      <div class=\"clearfix tac boldHD\"> Notice</div>\r\n      <p>The downloadable file below is providing for your use only, as a subscriber to this site and the service offered item herein. Sharing of this file is strictly prohibited according to the terms of your subscription agreement.</p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"stepOuterNext stepOuterLast\">\r\n  <div class=\"container\">\r\n    Your download should start automatically after countdown. If not,click on the download button.\r\n    <div class=\"nextBtn mt20\">\r\n      <div class=\"countdown styled\"></div>\r\n    </div>\r\n  </div>\r\n</div>", header_logged_in: "<div class=\"col-sm-3 logoOuter\">\r\n  <a href=\"/\" data-remote=\"true\" class=\"logo\">\r\n  <img src=\"/assets/logo.png\" alt=\"Logo\">\r\n  </a>\r\n</div>\r\n<div class=\"col-sm-9\">\r\n  <div class=\"text-right alreadyMEm logoutOuter\"> <a href=\"/users/sign_out\" data-method=\"delete\" rel=\"nofollow\" class=\"signIn logout\">Logout</a></div>\r\n  <div class=\"nav\">\r\n    <ul>\r\n      <li class=\"i1\"> <a href=\"/\" data-remote=\"true\"> </a> </li>\r\n      <li class=\"i2\"> <a href=\"/projects\" data-remote=\"true\"> </a> </li>\r\n      <li class=\"i3\"> <a href=\"/tunnel_projects\" data-remote=\"true\"> </a> </li>\r\n      <li class=\"i4\"> <a href=\"/pricing\" data-remote=\"true\"> </a> </li>\r\n    </ul>\r\n  </div>\r\n</div>", header_not_logged_in: "<div class=\"col-sm-3 logoOuter\">\r\n  <a href=\"/\" data-remote=\"true\" class=\"logo\">\r\n  <img src=\"/assets/logo.png\" alt=\"Logo\">\r\n  </a>\r\n</div>\r\n<div class=\"col-sm-9\">\r\n  <div class=\"text-right alreadyMEm\">Already a Member ? <a href=\"javascript:;\" data-target=\"#login\" data-toggle=\"modal\" class=\"signIn\">Login</a></div>\r\n</div>", footer: "<div class=\"clearfix\"></div>\r\n<footer class=\"clearfix\">\r\n  <div class=\"container\">\r\n    <span>\r\n    <a href=\"/\" data-remote=\"true\">Home</a>\r\n    <a href=\"/about_us\" data-remote=\"true\">About Us</a>\r\n    <a href=\"/terms_and_conditions\" data-remote=\"true\">Terms of Use</a>\r\n    <a href=\"/privacy_policy\" data-remote=\"true\">Privacy Policy</a>\r\n    <a href=\"/faq\" data-remote=\"true\">FAQ</a>\r\n    <a href=\"/contact_us\" data-remote=\"true\">Contact Us</a>\r\n    </span>\r\n    <div class=\"copy\"> Copyright &copy; 2016 </div>\r\n  </div>\r\n</footer>\r\n<div class=\"ajax-loading\">\r\n  <span>\r\n    <p id=\"please_wait\"><strong>Please wait..</strong></p>\r\n    <p><i class=\"glyphicon glyphicon-refresh gly-spin\" aria-hidden=\"true\"></i></p>\r\n  </span>\r\n</div>", home_page: "<div class=\"grayOuter padding_50\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt dashboard\">\r\n      <div class=\"clearfix tac boldHD\"> Welcome to your Dashboard !</div><br>\r\n      <p class=\"dash_para\">On this page you may access your Account to review details, change password, review and upgrade your subscription, payment details, and more. You also have access to all completed project report files available for download (subject to limitations, if any, of your subscription level). You also have access to the Quick-Start Guide, available for review whenever you need it. <br> <span>Click to access any of the areas below, or Click the Start Button to begin a new project </span> </p>\r\n      <div class=\"row fastStart\">\r\n        <div class=\"col-sm-6\">\r\n          <div class=\"dash_start new_start\">\r\n            <p>Click <a href=\"/quick_start/step1?first=1\" data-remote=\"true\">Start</a> Button to begin a <br> new project with help screens</p>\r\n            <a href=\"/quick_start/step1?first=1\" data-remote=\"true\" class=\"btn_start\">Start</a> \r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-6\">\r\n          <div class=\"dash_start fast_start\">\r\n            <p>Click <a href=\"/projects/new\" data-remote=\"true\">fast-start</a> Button to Begin A <br>new project without help screens</p>\r\n            <a href=\"/projects/new\" data-remote=\"true\" class=\"btn_fast_start\">fast-Start</a> \r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-sm-3 col-xs-6\">\r\n        <div class=\"project_box padd_rgt0\">\r\n          <div class=\"dash_bg1\"> <a href=\"/projects\" data-remote=\"true\" class=\"projct\"> <span></span>projects </a> <a href=\"/projects_pending\" data-remote=\"true\" class=\"p_pending\"> <span></span>projects pending </a> </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-sm-3 padd_rgt0 col-xs-6\">\r\n        <div class=\"project_box\">\r\n          <div class=\"dash_bg2 center_text\">\r\n            <a href=\"/users/edit\" data-remote=\"true\" class=\"icn_account\"> <span></span>My Account </a> \r\n            <p></p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-sm-3 padd_rgt0 col-xs-6\">\r\n        <div class=\"project_box\">\r\n          <div class=\"dash_bg3\"> <a href=\"/quick_start\" data-remote=\"true\" class=\"q_start\"> <span></span>Quick-Start Guide </a> <a href=\"/help\" data-remote=\"true\" class=\"h_support\"> <span></span>Help-support </a> </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-sm-3 col-xs-6\">\r\n        <div class=\"project_box\">\r\n          <div class=\"dash_bg4 center_text\"> <span class=\"icn_report\"></span> <a href=\"javascript:;\" data-tooltip=\"AVAILABLE IN PRO ACCOUNT ONLY\"> Report<br>customization </a> </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>", disclaimer_acknowledgement: " <div class=\"Disclaimer whiteBkg\">\r\n  <div class=\"container\">\r\n    <div class=\"max960width\">\r\n      <div class=\"clearfix tac boldHD\"> Disclaimer</div>\r\n\r\n      <p align=\"left\">The output file is a Material-only ROM/Budgetary Price Quotation. This quote represents our best assessment of the ROM material categories and quantities required to develop the DAS solution per minimum viable inputs (MVI) stated by the user. This quotes but may be subject to variations due to factors beyond our control. The accuracy of this ROM quote is limited to the confines of the accuracy and industry best practices relating to standards for Budget Estimates of -10% to +25%.</p>\r\n      <p align=\"left\">This service is provided as is, and is to be used at the subscriber’s discretion. The subscriber assumes all risk for using this service.<strong> XXXXX Inc</strong>. provides no guarantees or warranty of any kind, expressed or implied, with respect to any goods, parts  and service provided by <strong>XXXXX Inc</strong>. Including, but not limited to, the implied guarantees, warranties or merchantability and fitness for a particular purpose. <strong>XXXXX Inc</strong>. shall not in any event be liable for any damages including, but not limited to, any indirect, special or consequential damages arising out of or in connection with furnishing of goods, parts or service, or the performance, use of, or inability to use any goods, parts or service, or otherwise, whether based on contract, tort or any other legal theory.  </p>\r\n<p align=\"left\">In the course of using the output report from this service to respond to or satisfy the conditions of any RFP or other situations relating to the output report from this service, the subscriber agrees to indemnify and hold harmless <strong>XXXXX Inc</strong>., its officers, agents and employees from and against any liabilities, damages, costs, expenses, and attorneys' fees for any claim of loss, damage, cost, or injury  of whatsoever kind or nature, including bodily injury of death, relating to the use of this service.\r\n</p>\r\n\r\n <p align=\"left\">The pricing provided for quoted projects by XXXXX Inc in the output report from this service represents a best average market price as offered by preferred vendors, OEMs and equipment manufacturers. XXXXX Inc. reserves the right to further offer, promote and/or make suggestions to the subscriber to opt-into participating in external granular, accurate and competitive pricing offers from preferred vendors, OEMs and equipment manufacturers for the subscriber’s quoted projects, at the subscriber’s discretion. The subscriber acknowledges and agrees to these terms and conditions of pricing by registering an account with XXXXX Inc.</p>\r\n\r\n      <label><input type=\"checkbox\" name=\"acknowledge\" id=\"disclaimer_acknowledge\"> I have read and acknowledge the above disclaimer.</label>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Disclaimer Acknowledge Modal -->\r\n<div class=\"modal fade\" id=\"acknowledge_show\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"DisclaimerShowLabel\">\r\n  <div class=\"modal-dialog modal-sm small-modal-margin\">\r\n    <div class=\"modal-content text-center\">\r\n      <div class=\"modal-header\">\r\n        <button aria-label=\"Close\" data-dismiss=\"modal\" class=\"close\" type=\"button\"><span aria-hidden=\"true\">×</span></button>\r\n        <h4 id=\"DisclaimerShowLabel\" class=\"modal-title\">You must acknowledge the above disclaimer.</h4>\r\n      </div>\r\n      <div class=\"modal-footer text-align-center btn_center margin-top-0\">\r\n        <a href=\"javascript:;\" data-dismiss=\"modal\" class=\"btn_start padding-10 margin-right-0\">OK</a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>", download_warning: "<div class=\"Disclaimer\">\r\n  <div class=\"clearfix tac boldHD\"> Notice</div>\r\n  <p>The downloadable report below is provided for your use, as a subscriber to this service. Subletting of this account or resale of this report or service is strictly prohibited according to the terms of your subscription agreement.</p>\r\n  <p>Violation of this notice will be subject to account termination.</p>\r\n</div>", report_disclaimer: " <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-top:30px;\">\r\n  <tr>\r\n    <td style=\"font-weight:bold;padding-bottom:15px;font-size:17px;color:#4f4f54;\">QUOTE NOTES</td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      <p>This is a Material-only ROM/Budgetary Price Quotation. This quote represents our best assessment of the ROM material categories and quantities required to develop the DAS solution per minimum viable inputs (MVI) stated by the user. This quote may be subject to variations due to factors beyond our control. The accuracy of this ROM quote is limited to the confines of the accuracy and industry best practices relating to standards for Budget Estimates of -10% to +25%.</p>\r\n      <p>1. This Quote does not Include Backup Battery or UPS Power Systems.AC Power Assumed for all active BDA equipment. </p> \r\n      <p>2. System Engineering Services for RF Site Survey, System Design, System Activation, Optimization, and Coverage confirmation for all extended services are not quoted. </p>\r\n      <p>3. This Quote does not include pricing for travel and expenses, System Closeout Package consisting of: System Description, As-built Documentation, System Settings, Coverage Benchmark Results, Equipment Manuals and Training. </p> \r\n      <p>4. Additional Services such as Alarms reporting via NMS are not quoted. </p>   \r\n      <p>5. This Quote does not include system Installation services. </p>\r\n      <p>6. This Quote assumes that all services lie within the bandwidth of the BDAs based on the basic frequency band entered. </p>\r\n      <p>7. This Quote assumes an all wall-mounted active/passive equipment installation. No floor-standing racks or cabinet assemblies are quoted. </p>\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum.\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"font-weight:bold;padding-bottom:35px;font-size:17px;color:#4f4f54;padding-top:25px;\">PRICING TERMS</td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      <p>1. The Pricing shown on this quote is valid for 30 Days from the quote preparation date shown. </p> \r\n      <p>2. All prices are in US Dollars</p>\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"font-weight:bold;padding-bottom:35px;font-size:17px;color:#4f4f54;padding-top:25px;\">QUOTE DISCLAIMERS AND ASSUMPTIONS</td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      <p>1. Provision of more details such as actual frequencies, floorplans or floor by floor dimensions and propagation profile and donor site details when produced may warrant a re-estimation and requote for greater accuracy. </p>\r\n      <p>2. The DAS shall deliver coverage throughout 95% of all occupied building spaces. </p>\r\n      <p>3. It is assumed that any undesired channels occurring within the passband of the headend is a minimum of 10dB lower in amplitude than any of the desired channels. </p>\r\n      <p>4. The proposed estimate assumes that portable radios are capable of transmitting at a minimum of +30 dBm and mobile phones at +26dBm. </p>\r\n      <p>5. The system is estimated to provide an average ‐ 95 dBm signal level throughout the buildings for Public Safety and other two-way radio Services, and ‐ 85 dBm for commercial wireless carrier services. </p> \r\n      <p>6. The Signal Boosters/BDAs are assumed to be located a Telco Closet with access to a stacked riser. </p>\r\n      <p>7. When Off-Air donor systems are selected, the Radio site BTS is assumed to be located off‐ site and an off‐ air feed solution is assumed. </p>\r\n      <p>8. All facility walls are assumed to be free of metal/mesh shielding. </p>\r\n      <p>9. Areas outside customer specific indicated coverage zones are not necessarily estimated to have coverage enhancement, however RF signal may radiate into these areas at a level below the average signal level estimated. </p>\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum.\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum.\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"font-weight:bold;padding-bottom:35px;font-size:17px;color:#4f4f54;padding-top:25px;\">QUOTE TERMS AND CONDITIONS</td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n       <p>This service is provided as-is, and is to be used at the subscriber’s discretion. The subscriber assumes all risk for using this service. XXXXX Inc. provides no guarantees or warranty of any kind, expressed or implied, with respect to any goods, parts  and service provided by XXXXX Inc. Including, but not limited to, the implied guarantees, warranties or merchantability and fitness for a particular purpose. XXXXX Inc. shall not in any event be liable for any damages including, but not limited to, any indirect, special or consequential damages arising out of or in connection with furnishing of goods, parts or service, or the performance, use of, or inability to use any goods, parts or service, or otherwise, whether based on contract, tort or any other legal theory. </p>  \r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      <p>In the course of using the output report from this service to respond to or satisfy the conditions of any RFP or other situations relating to the output report from this service, the subscriber agrees to indemnify and hold harmless XXXXX Inc., its officers, agents and employees from and against any liabilities, damages, costs, expenses, and attorneys' fees for any claim of loss, damage, cost, or injury  of whatsoever kind or nature, including bodily injury of death, relating to the use of this service. </p>\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\"> \r\n       <p>The pricing provided for quoted projects by XXXXX Inc in the output report from this service represents a best average market price as offered by preferred vendors, OEMs and equipment manufacturers. XXXXX Inc. reserves the right to further offer, promote and/or make suggestions to the subscriber to opt-into participating in external granular, accurate and competitive pricing offers from preferred vendors, OEMs and equipment manufacturers for the subscriber’s quoted projects, at the subscriber’s discretion. The subscriber acknowledges and agrees to these terms and conditions of pricing by registering an account with XXXXX Inc.</p>\r\n</td>\r\n  </tr>\r\n</table>", home_slider: " <div class=\"col-sm-7\">\r\n  <div class=\"sliderInner\">\r\n    <h1 class=\"mainHD\">NEVER WAIT</h1>  \r\n    <div class=\"subHD\">for another DAS Quote Estimate - Ever Again !</div>\r\n    <img src=\"/assets/arrow.png\" alt=\"Arrow\">\r\n    <div data-ride=\"carousel\" class=\"carousel slide\" id=\"carousel-example-generic\">\r\n      <div role=\"listbox\" class=\"carousel-inner\">\r\n        <!-- Slide 1 Start -->\r\n        <div class=\"item active\">\r\n          <div class=\"italic\">Welcome to the world's first implementation of our time-tested Rapid System Design Assessment and Quote (RSDAQ™) method. </div>\r\n        </div>\r\n        <!-- Slide 1 End -->\r\n        <!-- Slide 2 Start -->\r\n        <div class=\"item\">\r\n          <div class=\"italic\">Use our RSDAQ™ Engine and get the benefits of lightning-fast DAS Equipment Cost Estimates and BOMs generated simultaneously for up to 10 Buildings (and more) in less than 15 minutes - in 3 Easy Steps!</div>\r\n        </div>\r\n        <!-- Slide 2 End -->\r\n        <!-- Slide 3 Start -->\r\n        <div class=\"item\">\r\n          <div class=\"italic\">12,185 Fast DAS ROM Quotes generated by the RSDAQ™ Engine to date, and counting!</div>\r\n        </div>\r\n        <!-- Slide 3 End -->\r\n        <!-- Slide 4 Start -->\r\n        <div class=\"item\">\r\n          <div class=\"italic\">Extreme DAS ROM Quote ROI is now a reality !  The RSDAQ™ engine is unlike anything else available today for generating DAS ROM Quotes. Prove it for yourself !  Get your subscription and watch it drive your project ROI through the roof ! </div>\r\n        </div>\r\n        <!-- Slide 4 End -->\r\n      </div>\r\n      <!-- Controls -->\r\n      <a href=\"#carousel-example-generic\" data-slide=\"prev\" role=\"button\" class=\"left carousel-control\">\r\n        <span aria-hidden=\"true\" class=\"glyphicon glyphicon-menu-left\"></span>\r\n      </a>\r\n      <a href=\"#carousel-example-generic\" data-slide=\"next\" role=\"button\" class=\"right carousel-control\">\r\n        <span aria-hidden=\"true\" class=\"glyphicon glyphicon-menu-right\"></span>\r\n      </a>\r\n    </div>\r\n  </div>\r\n</div>", building_projects: "<div class=\"clearfix tac boldHD\"> Building Projects </div><br>\r\n<p class=\"dash_para\">Below is a list of completed Building-based projects. The files are available for download or may be kept here for short-term storage. <br>Be sure to verify your storage time allowance as corresponds to your subscription level.</p>", building_pending_projects: "<div class=\"clearfix tac boldHD\"> Building Projects Pending </div><br>\r\n<p class=\"dash_para\">The following is a list of Building-based projects that are 'works-in-process'. <br>Any item in the list may be opened for further editing and eventual completion.</p>", tunnel_projects: "<div class=\"clearfix tac boldHD\"> Tunnel Projects </div><br>\r\n<p class=\"dash_para\">Below is a list of completed Tunnel-based projects. The files are available for download or may be kept here for short-term storage. <br>Be sure to verify your storage time allowance as corresponds to your subscription level.</p>", tunnel_pending_projects: "<div class=\"clearfix tac boldHD\"> Tunnel Projects Pending </div><br>\r\n<p class=\"dash_para\">The following is a list of Tunnel-based projects that are 'works-in-process'. <br>Any item in the list may be opened for further editing and eventual completion.</p>"}
])
puts "Done"

puts "Creating Design Information Admin"
DesignInformationAdmin.create!([
  {distance_between_each_floor: 25.0}
])
puts "Done"

puts "Creating Communication Types"
CommunicationType.create!([
  {communication: "Simplex", communication_assigned: false, default_selected: false},
  {communication: "Duplex", communication_assigned: false, default_selected: true},
  {communication: "Auto Select", communication_assigned: true, default_selected: false}
])
puts "Done"

puts "Creating Product Price Lists"
ProductPriceList.create!([
  {report_material_list_id: 1, vendor: "vendor1", manufacturer: "manufacturer1", mfg_pn: "mfg_pn1", our_pn: "our_pn1", description: nil, mfg_list_price: 123.0, our_cost: 124.0, discount_off_list: 3.0, our_sell_price: 125.0},
  {report_material_list_id: 2, vendor: "vendor2", manufacturer: "manufacturer2", mfg_pn: "mfg_pn2", our_pn: "our_pn2", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 2.0, our_sell_price: 128.0},
  {report_material_list_id: 3, vendor: "vendor3", manufacturer: "manufacturer3", mfg_pn: "mfg_pn3", our_pn: "our_pn3", description: nil, mfg_list_price: 123.0, our_cost: 124.0, discount_off_list: 3.0, our_sell_price: 125.0},
  {report_material_list_id: 4, vendor: "vendor4", manufacturer: "manufacturer4", mfg_pn: "mfg_pn4", our_pn: "our_pn4", description: nil, mfg_list_price: 124.0, our_cost: 125.0, discount_off_list: 1.0, our_sell_price: 126.0},
  {report_material_list_id: 5, vendor: "vendor5", manufacturer: "manufacturer5", mfg_pn: "mfg_pn5", our_pn: "our_pn5", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 4.0, our_sell_price: 127.0},
  {report_material_list_id: 6, vendor: "vendor6", manufacturer: "manufacturer6", mfg_pn: "mfg_pn6", our_pn: "our_pn6", description: nil, mfg_list_price: 122.0, our_cost: 123.0, discount_off_list: 2.0, our_sell_price: 124.0},
  {report_material_list_id: 7, vendor: "vendor7", manufacturer: "manufacturer7", mfg_pn: "mfg_pn7", our_pn: "our_pn7", description: nil, mfg_list_price: 124.0, our_cost: 125.0, discount_off_list: 3.0, our_sell_price: 126.0},
  {report_material_list_id: 8, vendor: "vendor8", manufacturer: "manufacturer8", mfg_pn: "mfg_pn8", our_pn: "our_pn8", description: nil, mfg_list_price: 124.0, our_cost: 125.0, discount_off_list: 4.0, our_sell_price: 126.0},
  {report_material_list_id: 9, vendor: "vendor9", manufacturer: "manufacturer9", mfg_pn: "mfg_pn9", our_pn: "our_pn9", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 2.0, our_sell_price: 127.0},
  {report_material_list_id: 10, vendor: "vendor10", manufacturer: "manufacturer10", mfg_pn: "mfg_pn10", our_pn: "our_pn10", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 2.0, our_sell_price: 128.0},
  {report_material_list_id: 11, vendor: "vendor11", manufacturer: "manufacturer11", mfg_pn: "mfg_pn11", our_pn: "our_pn11", description: nil, mfg_list_price: 121.0, our_cost: 122.0, discount_off_list: 5.0, our_sell_price: 123.0},
  {report_material_list_id: 12, vendor: "vendor12", manufacturer: "manufacturer12", mfg_pn: "mfg_pn12", our_pn: "our_pn12", description: nil, mfg_list_price: 124.0, our_cost: 125.0, discount_off_list: 1.0, our_sell_price: 126.0},
  {report_material_list_id: 13, vendor: "vendor13", manufacturer: "manufacturer13", mfg_pn: "mfg_pn13", our_pn: "our_pn13", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 3.0, our_sell_price: 128.0},
  {report_material_list_id: 14, vendor: "vendor14", manufacturer: "manufacturer14", mfg_pn: "mfg_pn14", our_pn: "our_pn14", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 2.0, our_sell_price: 129.0},
  {report_material_list_id: 15, vendor: "vendor15", manufacturer: "manufacturer15", mfg_pn: "mfg_pn15", our_pn: "our_pn15", description: nil, mfg_list_price: 122.0, our_cost: 123.0, discount_off_list: 1.0, our_sell_price: 124.0},
  {report_material_list_id: 16, vendor: "vendor16", manufacturer: "manufacturer16", mfg_pn: "mfg_pn16", our_pn: "our_pn16", description: nil, mfg_list_price: 123.0, our_cost: 124.0, discount_off_list: 2.0, our_sell_price: 125.0},
  {report_material_list_id: 17, vendor: "vendor17", manufacturer: "manufacturer17", mfg_pn: "mfg_pn17", our_pn: "our_pn17", description: nil, mfg_list_price: 124.0, our_cost: 125.0, discount_off_list: 2.0, our_sell_price: 126.0},
  {report_material_list_id: 18, vendor: "vendor18", manufacturer: "manufacturer18", mfg_pn: "mfg_pn18", our_pn: "our_pn18", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 3.0, our_sell_price: 128.0},
  {report_material_list_id: 19, vendor: "vendor19", manufacturer: "manufacturer19", mfg_pn: "mfg_pn19", our_pn: "our_pn19", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 2.0, our_sell_price: 128.0},
  {report_material_list_id: 20, vendor: "vendor20", manufacturer: "manufacturer20", mfg_pn: "mfg_pn20", our_pn: "our_pn20", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 2.0, our_sell_price: 129.0},
  {report_material_list_id: 21, vendor: "vendor21", manufacturer: "manufacturer21", mfg_pn: "mfg_pn21", our_pn: "our_pn21", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 2.0, our_sell_price: 127.0},
  {report_material_list_id: 22, vendor: "vendor22", manufacturer: "manufacturer22", mfg_pn: "mfg_pn22", our_pn: "our_pn22", description: nil, mfg_list_price: 128.0, our_cost: 129.0, discount_off_list: 2.0, our_sell_price: 130.0},
  {report_material_list_id: 23, vendor: "vendor23", manufacturer: "manufacturer23", mfg_pn: "mfg_pn23", our_pn: "our_pn23", description: nil, mfg_list_price: 124.0, our_cost: 125.0, discount_off_list: 2.0, our_sell_price: 126.0},
  {report_material_list_id: 24, vendor: "vendor24", manufacturer: "manufacturer24", mfg_pn: "mfg_pn24", our_pn: "our_pn24", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 3.0, our_sell_price: 127.0},
  {report_material_list_id: 25, vendor: "vendor25", manufacturer: "manufacturer25", mfg_pn: "mfg_pn25", our_pn: "our_pn25", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 1.0, our_sell_price: 128.0},
  {report_material_list_id: 26, vendor: "vendor26", manufacturer: "manufacturer26", mfg_pn: "mfg_pn26", our_pn: "our_pn26", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 2.0, our_sell_price: 129.0},
  {report_material_list_id: 27, vendor: "vendor27", manufacturer: "manufacturer27", mfg_pn: "mfg_pn27", our_pn: "our_pn27", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 1.0, our_sell_price: 127.0},
  {report_material_list_id: 28, vendor: "vendor28", manufacturer: "manufacturer28", mfg_pn: "mfg_pn28", our_pn: "our_pn28", description: nil, mfg_list_price: 122.0, our_cost: 123.0, discount_off_list: 5.0, our_sell_price: 124.0},
  {report_material_list_id: 29, vendor: "vendor29", manufacturer: "manufacturer29", mfg_pn: "mfg_pn29", our_pn: "our_pn29", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 1.0, our_sell_price: 128.0},
  {report_material_list_id: 30, vendor: "vendor30", manufacturer: "manufacturer30", mfg_pn: "mfg_pn30", our_pn: "our_pn30", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 2.0, our_sell_price: 129.0},
  {report_material_list_id: 31, vendor: "vendor31", manufacturer: "manufacturer31", mfg_pn: "mfg_pn31", our_pn: "our_pn31", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 1.0, our_sell_price: 127.0},
  {report_material_list_id: 32, vendor: "vendor32", manufacturer: "manufacturer32", mfg_pn: "mfg_pn32", our_pn: "our_pn32", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 1.0, our_sell_price: 129.0},
  {report_material_list_id: 33, vendor: "vendor33", manufacturer: "manufacturer33", mfg_pn: "mfg_pn33", our_pn: "our_pn33", description: nil, mfg_list_price: 121.0, our_cost: 122.0, discount_off_list: 1.0, our_sell_price: 123.0},
  {report_material_list_id: 34, vendor: "vendor34", manufacturer: "manufacturer34", mfg_pn: "mfg_pn34", our_pn: "our_pn34", description: nil, mfg_list_price: 120.0, our_cost: 121.0, discount_off_list: 1.0, our_sell_price: 122.0},
  {report_material_list_id: 35, vendor: "vendor35", manufacturer: "manufacturer35", mfg_pn: "mfg_pn35", our_pn: "our_pn35", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 2.0, our_sell_price: 127.0},
  {report_material_list_id: 36, vendor: "vendor36", manufacturer: "manufacturer36", mfg_pn: "mfg_pn36", our_pn: "our_pn36", description: nil, mfg_list_price: 123.0, our_cost: 124.0, discount_off_list: 1.0, our_sell_price: 125.0},
  {report_material_list_id: 37, vendor: "vendor37", manufacturer: "manufacturer37", mfg_pn: "mfg_pn37", our_pn: "our_pn37", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 2.0, our_sell_price: 129.0},
  {report_material_list_id: 38, vendor: "vendor38", manufacturer: "manufacturer38", mfg_pn: "mfg_pn38", our_pn: "our_pn38", description: nil, mfg_list_price: 123.0, our_cost: 124.0, discount_off_list: 1.0, our_sell_price: 125.0},
  {report_material_list_id: 39, vendor: "vendor39", manufacturer: "manufacturer39", mfg_pn: "mfg_pn39", our_pn: "our_pn39", description: nil, mfg_list_price: 122.0, our_cost: 123.0, discount_off_list: 1.0, our_sell_price: 124.0},
  {report_material_list_id: 40, vendor: "vendor40", manufacturer: "manufacturer40", mfg_pn: "mfg_pn40", our_pn: "our_pn40", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 1.0, our_sell_price: 128.0},
  {report_material_list_id: 41, vendor: "vendor41", manufacturer: "manufacturer41", mfg_pn: "mfg_pn41", our_pn: "our_pn41", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 1.0, our_sell_price: 129.0},
  {report_material_list_id: 42, vendor: "vendor42", manufacturer: "manufacturer42", mfg_pn: "mfg_pn42", our_pn: "our_pn42", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 1.0, our_sell_price: 127.0},
  {report_material_list_id: 43, vendor: "vendor43", manufacturer: "manufacturer43", mfg_pn: "mfg_pn43", our_pn: "our_pn43", description: nil, mfg_list_price: 125.0, our_cost: 126.0, discount_off_list: 1.0, our_sell_price: 127.0},
  {report_material_list_id: 44, vendor: "vendor44", manufacturer: "manufacturer44", mfg_pn: "mfg_pn44", our_pn: "our_pn44", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 1.0, our_sell_price: 128.0},
  {report_material_list_id: 45, vendor: "vendor45", manufacturer: "manufacturer45", mfg_pn: "mfg_pn45", our_pn: "our_pn45", description: nil, mfg_list_price: 123.0, our_cost: 124.0, discount_off_list: 1.0, our_sell_price: 125.0},
  {report_material_list_id: 46, vendor: "vendor46", manufacturer: "manufacturer46", mfg_pn: "mfg_pn46", our_pn: "our_pn46", description: nil, mfg_list_price: 123.0, our_cost: 124.0, discount_off_list: 1.0, our_sell_price: 125.0},
  {report_material_list_id: 47, vendor: "vendor47", manufacturer: "manufacturer47", mfg_pn: "mfg_pn47", our_pn: "our_pn47", description: nil, mfg_list_price: 126.0, our_cost: 127.0, discount_off_list: 2.0, our_sell_price: 128.0},
  {report_material_list_id: 48, vendor: "vendor48", manufacturer: "manufacturer48", mfg_pn: "mfg_pn48", our_pn: "our_pn48", description: nil, mfg_list_price: 128.0, our_cost: 129.0, discount_off_list: 3.0, our_sell_price: 130.0}
])
puts "Done"

puts "Creating Tunnel Passive Component Losses"
TunnelPassiveComponentLoss.create!([
  {jumper_loss: 0.5, connector_loss: 0.1, way2_splitter_loss: 2.5, way3_splitter_loss: 5.5, way4_splitter_loss: 6.5, way6_splitter_loss: 8.2, way8_splitter_loss: 9.5, directional_coupler_loss: 0.8, hybrid_coupler_loss: 3.5, quantity_of_splitters: 2, type_of_splitter: "way2_splitter_loss"}
])
puts "Done"

puts "Creating Fiber Material Quantities"
FiberMaterialQuantity.create!([
  {report_material_list_id: 19, description: nil, quantity: 1.0},
  {report_material_list_id: 20, description: nil, quantity: 0.0},
  {report_material_list_id: 21, description: nil, quantity: 1.0},
  {report_material_list_id: 22, description: nil, quantity: 1.0},
  {report_material_list_id: 23, description: nil, quantity: 1.0}
])
puts "Done"

puts "Creating Donor Direct Feed Quantities"
DonorDirectFeedQuantity.create!([
  {report_material_list_id: 16, description: nil, quantity: 1.0},
  {report_material_list_id: 17, description: nil, quantity: 0.0},
  {report_material_list_id: 18, description: nil, quantity: 0.0},
  {report_material_list_id: 24, description: nil, quantity: 1.0},
  {report_material_list_id: 25, description: nil, quantity: 1.0},
  {report_material_list_id: 26, description: nil, quantity: 250.0},
  {report_material_list_id: 27, description: nil, quantity: 2.0},
  {report_material_list_id: 28, description: nil, quantity: 1.0},
  {report_material_list_id: 29, description: nil, quantity: 1.0},
  {report_material_list_id: 30, description: nil, quantity: 1.0}
])
puts "Done"

puts "Creating Tunnel Environments"
TunnelEnvironment.create!([
  {environment: "Wide Open Service Access Tunnel", tunnel_indoor_factor: 2.2},
  {environment: "Vehicle Tunnel", tunnel_indoor_factor: 2.5},
  {environment: "Train Tunnel (subway or otherwise)", tunnel_indoor_factor: 3.5},
  {environment: "Free Space - No Tunnel Clutter (ideal)", tunnel_indoor_factor: 2.0},
  {environment: "Crawl Space - Small Tunnel", tunnel_indoor_factor: 1.7},
  {environment: "Coal Mine Shaft", tunnel_indoor_factor: 3.5},
  {environment: "Other Mine Shaft", tunnel_indoor_factor: 2.2}
])
puts "Done"
