class AddDataEntriesToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :building_data_entry, :text, default: " "
    add_column :static_pages, :tunnel_data_entry, :text, default: " "
  end
end
