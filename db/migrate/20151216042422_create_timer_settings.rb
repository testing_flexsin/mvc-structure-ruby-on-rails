class CreateTimerSettings < ActiveRecord::Migration
  def change
    create_table :timer_settings do |t|
      t.string :plan_name
      t.integer :timer

      t.timestamps null: false
    end
  end
end
