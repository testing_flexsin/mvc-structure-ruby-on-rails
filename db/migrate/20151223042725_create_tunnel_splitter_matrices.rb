class CreateTunnelSplitterMatrices < ActiveRecord::Migration
  def change
    create_table :tunnel_splitter_matrices do |t|
      t.integer :number_of_bdas
      t.float :product_type_2w
      t.float :product_type_3w
      t.float :product_type_4w
      t.float :dc
      t.integer :config_per_bda

      t.timestamps null: false
    end
  end
end
