class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :name
      t.float :amount
      t.integer :credits
      t.boolean :usage_credits_one_time, default: true

      t.timestamps null: false
    end
  end
end
