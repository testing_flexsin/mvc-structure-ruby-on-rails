class AddPricingToStaticPages < ActiveRecord::Migration
  def change
  	add_column :static_pages, :building_pricing, :text, default: " "
  	add_column :static_pages, :tunnel_pricing, :text, default: " "
  	add_column :static_pages, :pricing, :text, default: " "
  end
end