class CreateTunnelLengths < ActiveRecord::Migration
  def change
    create_table :tunnel_lengths do |t|
      t.float :length

      t.timestamps null: false
    end
  end
end
