class AddDeletedAtToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :deleted_at, :datetime
    add_index :static_pages, :deleted_at
  end
end
