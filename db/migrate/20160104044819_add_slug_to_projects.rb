class AddSlugToProjects < ActiveRecord::Migration
  def change
  	add_column :projects, :slug, :string
  	add_column :tunnel_projects, :slug, :string

	  add_index :projects, :slug, unique: true
	  add_index :tunnel_projects, :slug, unique: true
  end
end
