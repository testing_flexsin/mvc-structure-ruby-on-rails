class AddHeaderFooterToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :header_logged_in, :text, default: " "
    add_column :static_pages, :header_not_logged_in, :text, default: " "
    add_column :static_pages, :footer, :text, default: " "
  end
end
