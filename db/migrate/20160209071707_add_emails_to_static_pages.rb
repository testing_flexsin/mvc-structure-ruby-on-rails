class AddEmailsToStaticPages < ActiveRecord::Migration
  def change
  	add_column :static_pages, :verification_email, :text, default: " "
  	add_column :static_pages, :confirmation_email, :text, default: " "
  	add_column :static_pages, :reset_password_email, :text, default: " "
  	add_column :static_pages, :unlock_email, :text, default: " "
  end
end
