class CreateHighestFrequencyBands < ActiveRecord::Migration
  def change
    create_table :highest_frequency_bands do |t|
      t.integer :frequency

      t.timestamps null: false
    end
  end
end
