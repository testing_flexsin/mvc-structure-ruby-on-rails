class CreateTunnelCoverageDistancePerBdas < ActiveRecord::Migration
  def change
    create_table :tunnel_coverage_distance_per_bdas do |t|
      t.integer :bda_product_category_id
      t.float :coverage_distance

      t.timestamps null: false
    end
  end
end
