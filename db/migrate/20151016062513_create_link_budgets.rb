class CreateLinkBudgets < ActiveRecord::Migration
  def change
    create_table :link_budgets do |t|
      t.integer :building_data_entry_id
      t.integer :antenna_number
      t.integer :bda_number
      t.float :channel_power
      t.float :cable_length
      t.float :cable_loss
      t.float :splitter_loss
      t.float :jumper_loss
      t.float :connector_loss
      t.float :antenna_gain
      t.float :antenna_erp
      t.float :dl_margin
      t.float :allowed_pl
      t.float :rssi_at_portable

      t.timestamps null: false
    end
  end
end
