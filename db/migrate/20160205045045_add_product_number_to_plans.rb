class AddProductNumberToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :product_number, :string
  end
end
