class ChangeTypeOfIdOfTables < ActiveRecord::Migration
  def change
  	change_table :building_data_entries do |t|
  		t.change :id, :integer, limit: 8
  		t.change :building_id, :integer, limit: 8
  	end
  	change_table :buildings do |t|
  		t.change :id, :integer, limit: 8
  		t.change :project_id, :integer, limit: 8
  	end
  	change_table :contacts do |t|
  		t.change :id, :integer, limit: 8
  	end
  	change_table :design_informations do |t|
  		t.change :id, :integer, limit: 8
  		t.change :building_data_entry_id, :integer, limit: 8
  	end
  	change_table :link_budgets do |t|
  		t.change :id, :integer, limit: 8
  		t.change :building_data_entry_id, :integer, limit: 8
  	end
  	change_table :part_quantity_informations do |t|
  		t.change :id, :integer, limit: 8
  		t.change :building_data_entry_id, :integer, limit: 8
  	end
  	change_table :projects do |t|
  		t.change :id, :integer, limit: 8
  		t.change :user_id, :integer, limit: 8
  	end
  	change_table :splitter_matrices do |t|
  		t.change :id, :integer, limit: 8
  	end
  	change_table :subscriptions do |t|
  		t.change :id, :integer, limit: 8
  		t.change :user_id, :integer, limit: 8
  	end
  	change_table :system_dimensions do |t|
  		t.change :id, :integer, limit: 8
  		t.change :building_data_entry_id, :integer, limit: 8
  	end
  	change_table :tunnel_data_entries do |t|
  		t.change :id, :integer, limit: 8
  		t.change :tunnel_id, :integer, limit: 8
  	end
  	change_table :tunnel_design_informations do |t|
  		t.change :id, :integer, limit: 8
  		t.change :tunnel_data_entry_id, :integer, limit: 8
  	end
  	change_table :tunnel_link_budgets do |t|
  		t.change :id, :integer, limit: 8
  		t.change :tunnel_data_entry_id, :integer, limit: 8
  	end
  	change_table :tunnel_part_quantity_informations do |t|
  		t.change :id, :integer, limit: 8
  		t.change :tunnel_data_entry_id, :integer, limit: 8
  	end
  	change_table :tunnel_projects do |t|
  		t.change :id, :integer, limit: 8
  		t.change :user_id, :integer, limit: 8
  	end
  	change_table :tunnel_signal_level_benchmarks do |t|
  		t.change :id, :integer, limit: 8
  		t.change :tunnel_data_entry_id, :integer, limit: 8
  	end
  	change_table :tunnel_splitter_matrices do |t|
  		t.change :id, :integer, limit: 8
  	end
  	change_table :tunnels do |t|
  		t.change :id, :integer, limit: 8
  		t.change :tunnel_project_id, :integer, limit: 8
  	end
  	change_table :users do |t|
  		t.change :id, :integer, limit: 8
  	end

  	execute "ALTER TABLE building_data_entries ALTER id TYPE BIGINT"
  	execute "ALTER TABLE building_data_entries ALTER building_id TYPE BIGINT"
  	execute "ALTER TABLE buildings ALTER id TYPE BIGINT"
  	execute "ALTER TABLE buildings ALTER project_id TYPE BIGINT"
  	execute "ALTER TABLE contacts ALTER id TYPE BIGINT"
  	execute "ALTER TABLE design_informations ALTER id TYPE BIGINT"
  	execute "ALTER TABLE design_informations ALTER building_data_entry_id TYPE BIGINT"
  	execute "ALTER TABLE link_budgets ALTER id TYPE BIGINT"
  	execute "ALTER TABLE link_budgets ALTER building_data_entry_id TYPE BIGINT"
  	execute "ALTER TABLE part_quantity_informations ALTER id TYPE BIGINT"
  	execute "ALTER TABLE part_quantity_informations ALTER building_data_entry_id TYPE BIGINT"
  	execute "ALTER TABLE projects ALTER id TYPE BIGINT"
  	execute "ALTER TABLE projects ALTER user_id TYPE BIGINT"
  	execute "ALTER TABLE splitter_matrices ALTER id TYPE BIGINT"
  	execute "ALTER TABLE subscriptions ALTER id TYPE BIGINT"
  	execute "ALTER TABLE subscriptions ALTER user_id TYPE BIGINT"
  	execute "ALTER TABLE system_dimensions ALTER id TYPE BIGINT"
  	execute "ALTER TABLE system_dimensions ALTER building_data_entry_id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_data_entries ALTER id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_data_entries ALTER tunnel_id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_design_informations ALTER id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_design_informations ALTER tunnel_data_entry_id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_link_budgets ALTER id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_link_budgets ALTER tunnel_data_entry_id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_part_quantity_informations ALTER id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_part_quantity_informations ALTER tunnel_data_entry_id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_projects ALTER id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_projects ALTER user_id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_signal_level_benchmarks ALTER id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_signal_level_benchmarks ALTER tunnel_data_entry_id TYPE BIGINT"
  	execute "ALTER TABLE tunnel_splitter_matrices ALTER id TYPE BIGINT"
  	execute "ALTER TABLE tunnels ALTER id TYPE BIGINT"
  	execute "ALTER TABLE tunnels ALTER tunnel_project_id TYPE BIGINT"
  	execute "ALTER TABLE users ALTER id TYPE BIGINT"
  end
end
