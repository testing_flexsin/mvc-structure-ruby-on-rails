class AddFieldsToPassiveComponentLosses < ActiveRecord::Migration
  def change
    add_column :passive_component_losses, :quantity_of_splitters, :integer
    add_column :passive_component_losses, :type_of_splitter, :string
  end
end
