class CreateTunnelReportMaterialLists < ActiveRecord::Migration
  def change
    create_table :tunnel_report_material_lists do |t|
      t.integer :tunnel_material_category_id
      t.text :description

      t.timestamps null: false
    end
  end
end
