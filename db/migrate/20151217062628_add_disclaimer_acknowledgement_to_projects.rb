class AddDisclaimerAcknowledgementToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :disclaimer_acknowledgement, :boolean, default: false
  end
end
