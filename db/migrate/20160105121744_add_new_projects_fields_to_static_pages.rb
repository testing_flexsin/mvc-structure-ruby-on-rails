class AddNewProjectsFieldsToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :left_image, :string
    add_column :static_pages, :right_image, :string
  end
end
