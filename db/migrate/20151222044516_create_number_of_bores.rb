class CreateNumberOfBores < ActiveRecord::Migration
  def change
    create_table :number_of_bores do |t|
      t.integer :bores

      t.timestamps null: false
    end
  end
end
