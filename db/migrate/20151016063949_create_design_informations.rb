class CreateDesignInformations < ActiveRecord::Migration
  def change
    create_table :design_informations do |t|
      t.integer :building_data_entry_id
      t.float :design_frequency
      t.float :coverage_radius
      t.float :coverage_area_per_antenna
      t.float :building_total_coverage_area
      t.float :number_of_antennas
      t.float :horizontal_cable
      t.float :number_of_bda
      t.float :total_number_of_antennas
      t.integer :number_of_floors
      t.float :distance_each_floor
      t.float :vertical_cable
      t.float :coverage_area_per_bda

      t.timestamps null: false
    end
  end
end
