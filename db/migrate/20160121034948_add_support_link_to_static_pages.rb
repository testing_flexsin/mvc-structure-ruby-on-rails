class AddSupportLinkToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :support_link, :string, default: " "
  end
end
