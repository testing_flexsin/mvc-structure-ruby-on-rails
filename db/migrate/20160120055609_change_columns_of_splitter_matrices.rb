class ChangeColumnsOfSplitterMatrices < ActiveRecord::Migration
  def change
    add_column :splitter_matrices, :matrix_type, :string
    add_column :splitter_matrices, :product_type_6w, :float
    add_column :splitter_matrices, :product_type_8w, :float
    add_column :splitter_matrices, :hc, :float
  end
end
