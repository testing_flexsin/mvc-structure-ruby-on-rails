class AddMarketingIdToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :marketing_id, :integer
  end
end
