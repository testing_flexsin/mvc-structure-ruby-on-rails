class AddReportRelatedFieldsToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :disclaimer_acknowledgement, :text, default: " "
    add_column :static_pages, :download_warning, :text, default: " "
  end
end
