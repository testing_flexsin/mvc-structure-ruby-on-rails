class AddConfirmableInUsers < ActiveRecord::Migration
  def change  	
    add_column :users, :full_name, :string
    add_column :users, :user_name, :string
    add_column :users, :company_name, :string
    add_column :users, :phone_number, :string
    add_column :users, :confirmation_token, :string
    add_column :users, :confirmed_at, :datetime
    add_column :users, :confirmation_sent_at, :datetime
    add_column :users, :unconfirmed_email, :string

    add_index :users, :confirmation_token,   unique: true
  end
end
