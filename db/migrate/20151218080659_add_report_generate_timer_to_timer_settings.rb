class AddReportGenerateTimerToTimerSettings < ActiveRecord::Migration
  def change
    add_column :timer_settings, :report_generate_timer, :integer
    rename_column :timer_settings, :timer, :report_download_timer
  end
end
