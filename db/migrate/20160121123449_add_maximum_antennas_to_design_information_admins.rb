class AddMaximumAntennasToDesignInformationAdmins < ActiveRecord::Migration
  def change
    add_column :design_information_admins, :maximum_antennas_per_bda, :integer
  end
end
