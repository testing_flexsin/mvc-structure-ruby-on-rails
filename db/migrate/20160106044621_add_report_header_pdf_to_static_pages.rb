class AddReportHeaderPdfToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :report_header_pdf, :text, default: " "
  end
end
