class CreateTunnelDataEntries < ActiveRecord::Migration
  def change
    create_table :tunnel_data_entries do |t|
      t.integer :tunnel_id
      t.string :tunnel_name
      t.integer :tunnel_number
      t.integer :number_of_bores
      t.integer :number_of_segments
      t.integer :bore_number
      t.integer :segment_number
      t.float :tunnel_length
      t.float :tunnel_width
      t.float :tunnel_height
      t.float :environment_designator
      t.integer :number_of_radio_rooms

      t.timestamps null: false
    end
  end
end
