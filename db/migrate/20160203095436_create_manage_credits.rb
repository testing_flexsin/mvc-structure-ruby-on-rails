class CreateManageCredits < ActiveRecord::Migration
  def change
    create_table :manage_credits do |t|
      t.integer :default_credits
      t.integer :credits_required

      t.timestamps null: false
    end
  end
end
