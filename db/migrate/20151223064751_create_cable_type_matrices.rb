class CreateCableTypeMatrices < ActiveRecord::Migration
  def change
    create_table :cable_type_matrices do |t|
      t.string :radiating_cable_type
      t.boolean :cable_type_assigned

      t.timestamps null: false
    end
  end
end
