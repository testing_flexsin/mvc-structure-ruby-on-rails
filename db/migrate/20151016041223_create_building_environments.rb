class CreateBuildingEnvironments < ActiveRecord::Migration
  def change
    create_table :building_environments do |t|
      t.string :environment
      t.float :building_indoor_factor

      t.timestamps null: false
    end
  end
end
