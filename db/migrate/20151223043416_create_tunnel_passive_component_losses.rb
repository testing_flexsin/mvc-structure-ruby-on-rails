class CreateTunnelPassiveComponentLosses < ActiveRecord::Migration
  def change
    create_table :tunnel_passive_component_losses do |t|
      t.float :jumper_loss
      t.float :connector_loss
      t.float :way2_splitter_loss
      t.float :way3_splitter_loss
      t.float :way4_splitter_loss
      t.float :way6_splitter_loss
      t.float :way8_splitter_loss
      t.float :directional_coupler_loss
      t.float :hybrid_coupler_loss
      t.integer :quantity_of_splitters
      t.string :type_of_splitter

      t.timestamps null: false
    end
  end
end
