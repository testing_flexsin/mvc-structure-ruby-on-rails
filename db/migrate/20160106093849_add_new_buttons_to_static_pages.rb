class AddNewButtonsToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :b2b_portal_access, :string, default: " "
    add_column :static_pages, :diagramming_tool, :string, default: " "
  end
end
