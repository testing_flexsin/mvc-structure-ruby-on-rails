class CreateCoverageAreaPerBdas < ActiveRecord::Migration
  def change
    create_table :coverage_area_per_bdas do |t|
      t.integer :bda_product_category_id
      t.float :coverage_area

      t.timestamps null: false
    end
  end
end
