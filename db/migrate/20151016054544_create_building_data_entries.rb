class CreateBuildingDataEntries < ActiveRecord::Migration
  def change
    create_table :building_data_entries do |t|
      t.integer :building_id
      t.string :building_name
      t.integer :building_number
      t.integer :floor_number
      t.integer :number_of_floors
      t.float :area_this_floor
      t.float :total_area_building
      t.float :antenna_coverage_radius
      t.float :estimated_path_loss
      t.float :frequency
      t.float :building_slope

      t.timestamps null: false
    end
  end
end
