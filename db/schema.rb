# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160209081213) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "bda_product_categories", force: :cascade do |t|
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "building_data_entries", id: :bigserial, force: :cascade do |t|
    t.integer  "building_id",             limit: 8
    t.string   "building_name"
    t.integer  "building_number"
    t.integer  "floor_number"
    t.integer  "number_of_floors"
    t.float    "area_this_floor"
    t.float    "total_area_building"
    t.float    "antenna_coverage_radius"
    t.float    "estimated_path_loss"
    t.float    "frequency"
    t.float    "building_slope"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "building_environments", force: :cascade do |t|
    t.string   "environment"
    t.float    "building_indoor_factor"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "buildings", id: :bigserial, force: :cascade do |t|
    t.integer  "project_id",          limit: 8
    t.integer  "number_of_buildings"
    t.string   "have_floorplan"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "cable_losses", force: :cascade do |t|
    t.integer  "highest_frequency_band_id"
    t.float    "cable_loss_1_by_2"
    t.float    "cable_loss_7_by_8"
    t.float    "cable_loss_11_by_4"
    t.float    "cable_loss_15_by_8"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "cable_type_matrices", force: :cascade do |t|
    t.string   "radiating_cable_type"
    t.boolean  "cable_type_assigned"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "communication_types", force: :cascade do |t|
    t.string   "communication"
    t.boolean  "communication_assigned", default: false
    t.boolean  "default_selected",       default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "contacts", id: :bigserial, force: :cascade do |t|
    t.string   "first_name"
    t.string   "email"
    t.string   "contact_number"
    t.string   "last_name"
    t.text     "message"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "coverage_area_per_bdas", force: :cascade do |t|
    t.integer  "bda_product_category_id"
    t.float    "coverage_area"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "credits", force: :cascade do |t|
    t.integer  "credits"
    t.float    "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "design_information_admins", force: :cascade do |t|
    t.float    "distance_between_each_floor"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "maximum_antennas_per_bda"
  end

  create_table "design_informations", id: :bigserial, force: :cascade do |t|
    t.integer  "building_data_entry_id",       limit: 8
    t.float    "design_frequency"
    t.float    "coverage_radius"
    t.float    "coverage_area_per_antenna"
    t.float    "building_total_coverage_area"
    t.float    "number_of_antennas"
    t.float    "horizontal_cable"
    t.float    "number_of_bda"
    t.float    "total_number_of_antennas"
    t.integer  "number_of_floors"
    t.float    "distance_each_floor"
    t.float    "vertical_cable"
    t.float    "coverage_area_per_bda"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "donor_direct_feed_quantities", force: :cascade do |t|
    t.integer  "report_material_list_id"
    t.string   "description"
    t.float    "quantity"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "equalization_multipliers", force: :cascade do |t|
    t.float    "multiplied_factor"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "fiber_material_quantities", force: :cascade do |t|
    t.integer  "report_material_list_id"
    t.string   "description"
    t.float    "quantity"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "have_floor_plans", force: :cascade do |t|
    t.string   "operator"
    t.string   "operand"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "highest_frequency_bands", force: :cascade do |t|
    t.integer  "frequency"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "link_budget_admins", force: :cascade do |t|
    t.float    "cable_length"
    t.float    "antenna_gain"
    t.float    "dl_margin"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "link_budgets", id: :bigserial, force: :cascade do |t|
    t.integer  "building_data_entry_id", limit: 8
    t.integer  "antenna_number"
    t.integer  "bda_number"
    t.float    "channel_power"
    t.float    "cable_length"
    t.float    "cable_loss"
    t.float    "splitter_loss"
    t.float    "jumper_loss"
    t.float    "connector_loss"
    t.float    "antenna_gain"
    t.float    "antenna_erp"
    t.float    "dl_margin"
    t.float    "allowed_pl"
    t.float    "rssi_at_portable"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "manage_credits", force: :cascade do |t|
    t.integer  "default_credits"
    t.integer  "credits_required"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "marketings", force: :cascade do |t|
    t.integer  "region_id"
    t.string   "vendor_name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "material_categories", force: :cascade do |t|
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "number_of_bores", force: :cascade do |t|
    t.integer  "bores"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "number_of_buildings", force: :cascade do |t|
    t.integer  "buildings"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "number_of_floors", force: :cascade do |t|
    t.integer  "floors"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "number_of_radio_rooms", force: :cascade do |t|
    t.integer  "radio_rooms"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "number_of_segments", force: :cascade do |t|
    t.integer  "segments"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "number_of_services", force: :cascade do |t|
    t.integer  "services"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "number_of_tunnels", force: :cascade do |t|
    t.integer  "tunnels"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "part_quantity_informations", id: :bigserial, force: :cascade do |t|
    t.integer  "building_data_entry_id",  limit: 8
    t.integer  "report_material_list_id"
    t.float    "quantity"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "passive_component_losses", force: :cascade do |t|
    t.float    "jumper_loss"
    t.float    "connector_loss"
    t.float    "way2_splitter_loss"
    t.float    "way3_splitter_loss"
    t.float    "way4_splitter_loss"
    t.float    "way6_splitter_loss"
    t.float    "way8_splitter_loss"
    t.float    "directional_coupler_loss"
    t.float    "hybrid_coupler_loss"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "quantity_of_splitters"
    t.string   "type_of_splitter"
  end

  create_table "plans", force: :cascade do |t|
    t.string   "name"
    t.float    "amount"
    t.integer  "credits"
    t.boolean  "usage_credits_one_time", default: true
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "product_number"
  end

  create_table "product_frequency_channels", force: :cascade do |t|
    t.integer  "number_of_channels"
    t.float    "composite_power"
    t.float    "channel_power"
    t.float    "papr"
    t.integer  "bda_product_category_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "default_selected",        default: false
  end

  create_table "product_price_lists", force: :cascade do |t|
    t.integer  "report_material_list_id"
    t.string   "vendor"
    t.string   "manufacturer"
    t.string   "mfg_pn"
    t.string   "our_pn"
    t.text     "description"
    t.float    "mfg_list_price"
    t.float    "our_cost"
    t.float    "discount_off_list"
    t.float    "our_sell_price"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "projects", id: :bigserial, force: :cascade do |t|
    t.integer  "user_id",                      limit: 8
    t.string   "user_name"
    t.string   "project_name"
    t.string   "company"
    t.string   "name"
    t.string   "facility_option"
    t.integer  "number_of_services"
    t.integer  "highest_frequency_band"
    t.integer  "product_frequency_channel_id"
    t.string   "system_feed_method"
    t.string   "system_architecture"
    t.string   "marketing_vendor_name"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.boolean  "report",                                 default: false
    t.integer  "marketing_id"
    t.integer  "step_completed",                         default: 0
    t.boolean  "disclaimer_acknowledgement",             default: false
    t.boolean  "visited_report",                         default: false
    t.string   "slug"
    t.integer  "expected_rssi_at_mobile"
    t.string   "technology_type"
    t.float    "papr"
    t.string   "communication_type"
    t.datetime "deleted_at"
  end

  add_index "projects", ["deleted_at"], name: "index_projects_on_deleted_at", using: :btree
  add_index "projects", ["slug"], name: "index_projects_on_slug", unique: true, using: :btree

  create_table "regions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "report_material_lists", force: :cascade do |t|
    t.integer  "material_category_id"
    t.text     "description"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "rssi_threshold_level_benchmarks", force: :cascade do |t|
    t.integer  "threshold_level"
    t.boolean  "default_selected", default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "services_breakouts", force: :cascade do |t|
    t.integer  "number_of_services"
    t.integer  "number_of_bdas_donor"
    t.integer  "number_of_bdas_das"
    t.string   "services_breakout_in_each_bda_box"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "splitter_matrices", id: :bigserial, force: :cascade do |t|
    t.integer  "antennas"
    t.float    "product_type_2w"
    t.float    "product_type_3w"
    t.float    "product_type_4w"
    t.float    "dc"
    t.integer  "number_of_bdas"
    t.integer  "config_per_bda"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "matrix_type"
    t.float    "product_type_6w"
    t.float    "product_type_8w"
    t.float    "hc"
  end

  create_table "square_footage_sizes", force: :cascade do |t|
    t.float    "square_foot"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "static_pages", force: :cascade do |t|
    t.text     "contact_us",                               default: " "
    t.text     "faq",                                      default: " "
    t.text     "privacy_policy",                           default: " "
    t.text     "terms_and_conditions",                     default: " "
    t.text     "about_us",                                 default: " "
    t.text     "quick_start",                              default: " "
    t.text     "quick_start_step1",                        default: " "
    t.text     "quick_start_step2",                        default: " "
    t.text     "quick_start_step3",                        default: " "
    t.text     "quick_start_download",                     default: " "
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.text     "header_logged_in",                         default: " "
    t.text     "header_not_logged_in",                     default: " "
    t.text     "footer",                                   default: " "
    t.text     "home_page",                                default: " "
    t.text     "disclaimer_acknowledgement",               default: " "
    t.text     "download_warning",                         default: " "
    t.text     "report_disclaimer",                        default: " "
    t.text     "home_slider",                              default: " "
    t.text     "building_projects",                        default: " "
    t.text     "building_pending_projects",                default: " "
    t.text     "tunnel_projects",                          default: " "
    t.text     "tunnel_pending_projects",                  default: " "
    t.string   "left_image"
    t.string   "right_image"
    t.text     "report_header_pdf",                        default: " "
    t.string   "help_user_name",                           default: " "
    t.string   "help_project_name",                        default: " "
    t.string   "help_company",                             default: " "
    t.string   "help_name",                                default: " "
    t.string   "help_number_of_buildings",                 default: " "
    t.string   "help_number_of_tunnels",                   default: " "
    t.string   "help_have_floorplan",                      default: " "
    t.string   "help_number_of_services",                  default: " "
    t.string   "help_highest_frequency_band",              default: " "
    t.string   "help_frequency_channels",                  default: " "
    t.string   "help_excepted_rssi_at_mobile",             default: " "
    t.string   "help_materials_sourcing",                  default: " "
    t.text     "credits",                                  default: " "
    t.text     "transaction_history",                      default: " "
    t.text     "buy_credits",                              default: " "
    t.text     "edit_your_account",                        default: " "
    t.text     "personal_information",                     default: " "
    t.text     "change_password",                          default: " "
    t.text     "my_plan",                                  default: " "
    t.text     "building_project_details",                 default: " "
    t.text     "building_facility_option",                 default: " "
    t.text     "building_data",                            default: " "
    t.text     "building_area_data_entry",                 default: " "
    t.text     "building_search_option",                   default: " "
    t.text     "building_band_and_services",               default: " "
    t.text     "building_system_feed_method",              default: " "
    t.text     "building_system_architecture",             default: " "
    t.text     "building_preferred_materials_sourcing",    default: " "
    t.text     "tunnel_project_details",                   default: " "
    t.text     "tunnel_facility_option",                   default: " "
    t.text     "tunnel_data",                              default: " "
    t.text     "tunnel_area_data_entry",                   default: " "
    t.text     "tunnel_search_option",                     default: " "
    t.text     "tunnel_band_and_services",                 default: " "
    t.text     "tunnel_system_feed_method",                default: " "
    t.text     "tunnel_system_architecture",               default: " "
    t.text     "expected_rssi_at_mobile",                  default: " "
    t.text     "tunnel_preferred_materials_sourcing",      default: " "
    t.text     "technology_type",                          default: " "
    t.text     "communication_type",                       default: " "
    t.string   "admin_email",                              default: " "
    t.text     "internal_server_error",                    default: " "
    t.text     "not_found",                                default: " "
    t.text     "show_report_heading",                      default: " "
    t.text     "buttons_report_page",                      default: " "
    t.text     "building_data_entry",                      default: " "
    t.text     "tunnel_data_entry",                        default: " "
    t.string   "help_building_name",                       default: " "
    t.string   "help_building_number",                     default: " "
    t.string   "help_number_of_floors",                    default: " "
    t.string   "help_quick_area_data_entry",               default: " "
    t.string   "help_building_environment",                default: " "
    t.string   "help_tunnel_name",                         default: " "
    t.string   "help_tunnel_number",                       default: " "
    t.string   "help_number_of_bores",                     default: " "
    t.string   "help_number_of_segments",                  default: " "
    t.string   "help_this_tunnel_bore",                    default: " "
    t.string   "help_this_tunnel_segment",                 default: " "
    t.string   "help_tunnel_environment",                  default: " "
    t.string   "help_number_of_stations",                  default: " "
    t.string   "support_link",                             default: " "
    t.string   "heading_change_your_password",             default: " "
    t.string   "heading_forgot_your_password",             default: " "
    t.string   "heading_register_account",                 default: " "
    t.string   "heading_login_details",                    default: " "
    t.string   "heading_retrieve_forgotten_password",      default: " "
    t.string   "heading_resend_confirmation_instructions", default: " "
    t.datetime "deleted_at"
    t.text     "building_pricing",                         default: " "
    t.text     "tunnel_pricing",                           default: " "
    t.text     "pricing",                                  default: " "
    t.string   "building_basic_plan_link",                 default: " "
    t.string   "building_standard_plan_link",              default: " "
    t.string   "building_professional_plan_link",          default: " "
    t.string   "tunnel_basic_plan_link",                   default: " "
    t.string   "tunnel_standard_plan_link",                default: " "
    t.string   "tunnel_professional_plan_link",            default: " "
    t.text     "welcome_page"
    t.text     "verification_email",                       default: " "
    t.text     "confirmation_email",                       default: " "
    t.text     "reset_password_email",                     default: " "
    t.text     "unlock_email",                             default: " "
  end

  add_index "static_pages", ["deleted_at"], name: "index_static_pages_on_deleted_at", using: :btree

  create_table "subscriptions", id: :bigserial, force: :cascade do |t|
    t.integer  "user_id",               limit: 8
    t.string   "transaction_id"
    t.string   "transaction_gateway"
    t.string   "transaction_receipt"
    t.string   "transaction_type"
    t.datetime "transaction_date"
    t.float    "transaction_amount"
    t.string   "transaction_currency"
    t.string   "seller_paypal_email"
    t.string   "customer_paypal_email"
    t.string   "paypal_payer_id"
    t.string   "product_number"
    t.string   "product_name"
    t.string   "product_type"
    t.float    "product_amount"
    t.string   "payment_type"
    t.string   "recurring_id"
    t.datetime "next_rebill"
    t.integer  "recurring_times"
    t.string   "recurring_status"
    t.integer  "payment_number"
    t.string   "seller_id"
    t.string   "seller_email"
    t.string   "customer_firstname"
    t.string   "customer_lastname"
    t.string   "customer_address"
    t.string   "customer_state"
    t.string   "customer_city"
    t.string   "customer_country"
    t.string   "customer_email"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "credits",                         default: 0
  end

  create_table "system_architectures", force: :cascade do |t|
    t.string   "architecture"
    t.boolean  "architecture_assigned", default: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.boolean  "default_selected",      default: false
  end

  create_table "system_dimensions", id: :bigserial, force: :cascade do |t|
    t.integer  "building_data_entry_id",  limit: 8
    t.float    "estimated_path_distance"
    t.float    "frequency"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "system_feed_methods", force: :cascade do |t|
    t.string   "feed_method"
    t.boolean  "feed_assigned",    default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "default_selected", default: false
  end

  create_table "technology_types", force: :cascade do |t|
    t.string   "technology"
    t.float    "papr"
    t.boolean  "default_selected", default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "timer_settings", force: :cascade do |t|
    t.string   "plan_name"
    t.integer  "report_download_timer"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "report_generate_timer"
  end

  create_table "trail_periods", force: :cascade do |t|
    t.integer  "period"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tunnel_cable_c_losses", force: :cascade do |t|
    t.integer  "highest_frequency_band_id"
    t.float    "cable_loss_1_by_2"
    t.float    "cable_loss_7_by_8"
    t.float    "cable_loss_11_by_4"
    t.float    "cable_loss_15_by_8"
    t.string   "default_selected"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "tunnel_cable_i_losses", force: :cascade do |t|
    t.integer  "highest_frequency_band_id"
    t.float    "cable_loss_1_by_2"
    t.float    "cable_loss_7_by_8"
    t.float    "cable_loss_11_by_4"
    t.float    "cable_loss_15_by_8"
    t.string   "default_selected"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "tunnel_coverage_distance_per_bdas", force: :cascade do |t|
    t.integer  "bda_product_category_id"
    t.float    "coverage_distance"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "tunnel_data_entries", id: :bigserial, force: :cascade do |t|
    t.integer  "tunnel_id",              limit: 8
    t.string   "tunnel_name"
    t.integer  "tunnel_number"
    t.integer  "number_of_bores"
    t.integer  "number_of_segments"
    t.integer  "bore_number"
    t.integer  "segment_number"
    t.float    "tunnel_length"
    t.float    "tunnel_width"
    t.float    "tunnel_height"
    t.float    "environment_designator"
    t.integer  "number_of_radio_rooms"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "tunnel_design_informations", id: :bigserial, force: :cascade do |t|
    t.integer  "tunnel_data_entry_id",         limit: 8
    t.float    "tunnel_length"
    t.integer  "number_of_radio_rooms"
    t.float    "average_distance_radio_rooms"
    t.float    "number_of_cable_runs"
    t.string   "cable_type"
    t.float    "total_cable_length"
    t.string   "bda_product_category"
    t.float    "number_of_bdas"
    t.string   "communication_type"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "tunnel_donor_direct_feed_quantities", force: :cascade do |t|
    t.integer  "tunnel_report_material_list_id"
    t.text     "description"
    t.float    "quantity"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "tunnel_environments", force: :cascade do |t|
    t.string   "environment"
    t.float    "tunnel_indoor_factor"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "tunnel_fiber_material_quantities", force: :cascade do |t|
    t.integer  "tunnel_report_material_list_id"
    t.text     "description"
    t.float    "quantity"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "tunnel_have_floor_plans", force: :cascade do |t|
    t.string   "operator"
    t.string   "operand"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tunnel_lengths", force: :cascade do |t|
    t.float    "length"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tunnel_link_budget_admins", force: :cascade do |t|
    t.float    "indoor_margin"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "tunnel_link_budgets", id: :bigserial, force: :cascade do |t|
    t.integer  "tunnel_data_entry_id",           limit: 8
    t.float    "channel_power"
    t.float    "frequency"
    t.float    "distribution_loss"
    t.float    "radiax_length"
    t.float    "radiating_cable_insertion_loss"
    t.float    "radiating_cable_coupling_loss"
    t.float    "distance_from_cable"
    t.float    "indoor_margin"
    t.float    "indoor_factor"
    t.float    "path_loss_from_cable"
    t.float    "signal_level_at_mobile"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "cable_type"
  end

  create_table "tunnel_material_categories", force: :cascade do |t|
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tunnel_part_quantity_informations", id: :bigserial, force: :cascade do |t|
    t.integer  "tunnel_data_entry_id",           limit: 8
    t.integer  "tunnel_report_material_list_id"
    t.float    "quantity"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "tunnel_passive_component_losses", force: :cascade do |t|
    t.float    "jumper_loss"
    t.float    "connector_loss"
    t.float    "way2_splitter_loss"
    t.float    "way3_splitter_loss"
    t.float    "way4_splitter_loss"
    t.float    "way6_splitter_loss"
    t.float    "way8_splitter_loss"
    t.float    "directional_coupler_loss"
    t.float    "hybrid_coupler_loss"
    t.integer  "quantity_of_splitters"
    t.string   "type_of_splitter"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "tunnel_product_frequency_channels", force: :cascade do |t|
    t.integer  "bda_product_category_id"
    t.integer  "number_of_channels"
    t.float    "composite_power"
    t.float    "channel_power"
    t.float    "papr"
    t.boolean  "default_selected",        default: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "tunnel_product_price_lists", force: :cascade do |t|
    t.integer  "tunnel_report_material_list_id"
    t.string   "vendor"
    t.string   "manufacturer"
    t.string   "mfg_pn"
    t.string   "our_pn"
    t.text     "description"
    t.float    "mfg_list_price"
    t.float    "our_cost"
    t.float    "discount_off_list"
    t.float    "our_sell_price"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "tunnel_projects", id: :bigserial, force: :cascade do |t|
    t.integer  "user_id",                             limit: 8
    t.string   "user_name"
    t.string   "project_name"
    t.string   "company"
    t.string   "name"
    t.string   "facility_option"
    t.integer  "number_of_services"
    t.integer  "highest_frequency_band"
    t.integer  "tunnel_product_frequency_channel_id"
    t.string   "system_feed_method"
    t.string   "system_architecture"
    t.integer  "marketing_id"
    t.integer  "expected_rssi_at_mobile"
    t.string   "technology_type"
    t.string   "communication_type"
    t.integer  "step_completed"
    t.boolean  "report",                                        default: false
    t.boolean  "disclaimer_acknowledgement",                    default: false
    t.boolean  "visited_report",                                default: false
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.float    "papr"
    t.string   "slug"
    t.datetime "deleted_at"
  end

  add_index "tunnel_projects", ["deleted_at"], name: "index_tunnel_projects_on_deleted_at", using: :btree
  add_index "tunnel_projects", ["slug"], name: "index_tunnel_projects_on_slug", unique: true, using: :btree

  create_table "tunnel_report_material_lists", force: :cascade do |t|
    t.integer  "tunnel_material_category_id"
    t.text     "description"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "tunnel_signal_level_benchmarks", id: :bigserial, force: :cascade do |t|
    t.integer  "tunnel_data_entry_id",       limit: 8
    t.float    "rssi_at_portable_in_tunnel"
    t.float    "threshold_level_benchmark"
    t.float    "threshold_overlimit_factor"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "tunnel_splitter_matrices", id: :bigserial, force: :cascade do |t|
    t.integer  "number_of_bdas"
    t.float    "product_type_2w"
    t.float    "product_type_3w"
    t.float    "product_type_4w"
    t.float    "dc"
    t.integer  "config_per_bda"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "tunnels", id: :bigserial, force: :cascade do |t|
    t.integer  "tunnel_project_id", limit: 8
    t.integer  "number_of_tunnels"
    t.string   "have_floorplan"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "unit_multipliers", force: :cascade do |t|
    t.float    "multiplied_factor"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "users", id: :bigserial, force: :cascade do |t|
    t.string   "email",                          default: "",    null: false
    t.string   "encrypted_password",             default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                  default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "full_name"
    t.string   "user_name"
    t.string   "company_name"
    t.string   "phone_number"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "first_name",                     default: ""
    t.string   "last_name",                      default: ""
    t.integer  "credits",                        default: 0
    t.integer  "building_plan_id"
    t.datetime "building_last_transaction_date"
    t.integer  "tunnel_plan_id"
    t.datetime "tunnel_last_transaction_date"
    t.boolean  "active",                         default: false
    t.datetime "building_last_credits_date"
    t.datetime "tunnel_last_credits_date"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id"
    t.string  "foreign_key_name", null: false
    t.integer "foreign_key_id"
  end

  add_index "version_associations", ["foreign_key_name", "foreign_key_id"], name: "index_version_associations_on_foreign_key", using: :btree
  add_index "version_associations", ["version_id"], name: "index_version_associations_on_version_id", using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.integer  "transaction_id"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  add_index "versions", ["transaction_id"], name: "index_versions_on_transaction_id", using: :btree

end
